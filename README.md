<!--
SPDX-FileCopyrightText: 2024 The Nirvati Developers

SPDX-License-Identifier: AGPL-3.0-or-later
-->

# Nirvati dashboard

This is the web frontend for Nirvati. It provides an easy way to manage and view various aspects of the Nirvati server.

It uses [Nuxt](https://nuxt.com/), and interacts with [Nirvati's backend](https://gitlab.com/nirvati/system/backend) which exposes a GraphQL API.
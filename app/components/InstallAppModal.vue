<!--
SPDX-FileCopyrightText: 2024 The Nirvati Developers

SPDX-License-Identifier: AGPL-3.0-or-later
-->

<template>
  <div>
    <UModal
      v-model:open="show"
      class="text-center"
      :title="
        page == 'domain'
          ? t('apps.install.set-domain.title')
          : page == 'settings'
            ? t('apps.install.settings.title')
            : t('apps.install.confirm.title')
      "
    >
      <template #body>
        <form @submit.prevent="formSubmit">
          <div
            v-if="page === 'domain'"
            class="flex flex-col items-center justify-center w-full"
          >
            <USelect
              v-model="newDomainOption"
              class="m-1 min-w-[50%] mb-2"
              :placeholder="t('utils.please-select')"
              :items="domainOptions"
            />
            <UInput
              v-if="newDomainOption === 'custom'"
              v-model="customAppDomain"
              :placeholder="`${app}.example.com`"
              class="mb-2"
              required
            />
            <UButton type="submit">
              {{ t("apps.install.next") }}
            </UButton>
            <UAlert
              v-if="newDomainOption === 'custom'"
              color="warning"
              variant="subtle"
              class="max-w-[80%] my-4"
              :title="t('https.port-forwarding.title')"
              :description="t('https.port-forwarding.content')"
            />
          </div>
          <div
            v-else-if="page === 'settings'"
            class="flex flex-col items-center justify-center w-full"
          >
            <AppSettingsDialog
              v-model="settings"
              :settings="settingsDefinition"
            />
            <UButton type="submit">
              {{ t("apps.install.next") }}
            </UButton>
          </div>
          <div
            v-else-if="page === 'confirm'"
            class="flex flex-col items-center justify-center w-full"
          >
            <p v-if="appPerms?.length > 0" class="mb-2">
              {{ t("apps.install.confirm.permissions") }}
            </p>
            <div v-if="appPerms?.length > 0" class="flex flex-col gap-4">
              <AppPermission
                v-for="perm in appPerms"
                :key="JSON.stringify(perm)"
                :perm
              />
            </div>

            <p class="mb-4 mt-4">
              {{ t("apps.install.confirm.content") }}
            </p>
            <UButton type="submit" :loading>
              {{ t("apps.install.install") }}
            </UButton>
          </div>
        </form>
      </template>
    </UModal>
  </div>
</template>

<script setup lang="ts">
import type { AcmeProvider } from "~/utils/gql/graphql";
import { generateAppFilesMutation } from "~/utils/gql_utils";
import { getDangerLevel } from "~/utils/permissions";

const props = defineProps({
  app: {
    type: String,
    required: true,
  },
  settingsDefinition: {
    type: Object as PropType<Record<string, Setting>>,
    required: true,
  },
  hasIngress: {
    type: Boolean,
    required: true,
  },
});
const emit = defineEmits(["startInstall"]);
const show = defineModel<boolean>();
const settings = defineModel("settings", {
  type: Object as PropType<Record<string, string>>,
  required: true,
});
const page = ref<"settings" | "domain" | "confirm">(
  Object.keys(props.settingsDefinition).length > 0
    ? "settings"
    : props.hasIngress
      ? "domain"
      : "confirm",
);

const { t } = useI18n();
const { getTranslated } = useTranslate();
const newDomainOption = ref<string>("");
const customAppDomain = ref("");
const loading = ref(false);

const selectedDomain = computed(() => {
  if (newDomainOption.value === "custom") {
    return customAppDomain.value;
  } else {
    return newDomainOption.value;
  }
});

const domainsQuery = graphql(`
  query getLinkedDomainNames {
    users {
      me {
        domains {
          domain
        }
        acmeProviders
      }
    }
  }
`);

const appWithSettingsQuery = graphql(`
  query getAppPermsWithSettings(
    $id: String!
    $settings: JSON!
    $initialDomain: String
  ) {
    users {
      me {
        app(appId: $id, settings: $settings, initialDomain: $initialDomain) {
          permissions {
            __typename
            ... on BuiltinPermission {
              permission
            }
            ... on AppPermission {
              appName
              permName
              description
            }
          }
        }
      }
    }
  }
`);

const { data: linkedDomains } = await useAsyncQuery(domainsQuery);
const appPermissionsQuery = useAsyncQuery(
  appWithSettingsQuery,
  computed(() => {
    console.log(props.app.toString());
    console.log({
      id: props.app,
      settings: settings.value,
      initialDomain:
        selectedDomain.value === "" ? undefined : selectedDomain.value,
    });
    return {
      id: props.app.toString(),
      settings: settings.value,
      initialDomain:
        selectedDomain.value === "" ? undefined : selectedDomain.value,
    };
  }),
);

function randomProvider(acmeProviders: AcmeProvider[]) {
  return acmeProviders[Math.floor(Math.random() * acmeProviders.length)];
}

const appInstallMutation = graphql(`
  mutation installApp(
    $id: String!
    $domain: String
    $acmeProvider: AcmeProvider
    $settings: JSON!
  ) {
    installApp(
      appId: $id
      settings: $settings
      initialDomain: $domain
      acmeProvider: $acmeProvider
    )
  }
`);

const { mutate: generateAppFiles } = useMutation(generateAppFilesMutation);
const { mutate: installApp } = useMutation(appInstallMutation);

watch([settings, selectedDomain], async () => {
  if (!props.app) {
    return;
  }
  console.log("Refetching!");
  await appPermissionsQuery.refresh();
});

const appPerms = computed(() => {
  const perms = appPermissionsQuery.data.value?.users.me.app.permissions;
  const sorted = perms?.toSorted((perm1, perm2) => {
    // If the permission is an "AppPermission", it should be at the end, otherwise, compare the dangerlevel, higher dangerlevel first
    if (
      perm1.__typename === "AppPermission" &&
      perm2.__typename !== "AppPermission"
    ) {
      return 1;
    } else if (
      perm1.__typename !== "AppPermission" &&
      perm2.__typename === "AppPermission"
    ) {
      return -1;
    } else if (
      perm1.__typename === "AppPermission" &&
      perm2.__typename === "AppPermission"
    ) {
      return getTranslated(perm1.appName).localeCompare(getTranslated(perm2.appName));
    } else {
      return (
        getDangerLevel(perm2.permission) - getDangerLevel(perm1.permission)
      );
    }
  });
  console.log(sorted);
  return sorted;
});

async function performInstall() {
  loading.value = true;
  emit("startInstall");
  await generateAppFiles();
  await installApp({
    id: props.app,
    domain: selectedDomain.value === "" ? undefined : selectedDomain.value,
    acmeProvider:
      selectedDomain.value === ""
        ? undefined
        : randomProvider(linkedDomains.value!.users.me.acmeProviders),
    settings: settings.value,
  });
  newDomainOption.value = "custom";
  customAppDomain.value = "";
  show.value = false;
  loading.value = false;
}

async function formSubmit() {
  if (page.value === "confirm") {
    await performInstall();
  } else if (page.value === "domain") {
    if (newDomainOption.value === "select") {
      return;
    }
    if (newDomainOption.value === "custom" && !customAppDomain.value) {
      return;
    }
    page.value = "confirm";
  } else if (page.value === "settings") {
    if (props.hasIngress) {
      page.value = "domain";
    } else {
      page.value = "confirm";
    }
  }
}

const domainOptions = computed(() => {
  const domains = linkedDomains.value?.users.me.domains.map(
    (d: { domain: string }) => ({
      label: `${props.app}.${d.domain}`,
      value: `${props.app}.${d.domain}`,
    }),
  );
  return [
    ...(domains || []),
    {
      value: "custom",
      label: t("https.custom"),
    },
  ];
});
</script>

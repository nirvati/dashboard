// SPDX-FileCopyrightText: 2024 The Nirvati Developers
//
// SPDX-License-Identifier: AGPL-3.0-or-later

export function useTranslate(): {
  getTranslated: (record: Record<string, string> | undefined) => string | undefined;
} {
  let i18n: ReturnType<typeof useI18n> | undefined;
  try {
    i18n = useI18n();
  } catch {
    console.warn("Failed to get i18n");
  }
  return {
    getTranslated(category: Record<string, string> | undefined): string | undefined {
      return category ? (
        category[i18n?.locale?.value || "en"] ||
        category[i18n?.fallbackLocale?.value?.toString() || "en"] ||
        category.en
      ) : undefined;
    },
  };
}

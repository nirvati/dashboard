/* eslint-disable */
import * as types from "./graphql";
import type { TypedDocumentNode as DocumentNode } from "@graphql-typed-document-node/core";

/**
 * Map of all GraphQL operations in the project.
 *
 * This map has several performance disadvantages:
 * 1. It is not tree-shakeable, so it will include all operations in the project.
 * 2. It is not minifiable, so the string of a GraphQL query will be multiple times inside the bundle.
 * 3. It does not support dead code elimination, so it will add unused operations.
 *
 * Therefore it is highly recommended to use the babel or swc plugin for production.
 * Learn more about it here: https://the-guild.dev/graphql/codegen/plugins/presets/preset-client#reducing-bundle-size
 */
const documents = {
  "\n  query getAppOverview {\n    users {\n      me {\n        username\n        apps(installedOnly: true) {\n          id\n          scope\n          name\n          icon\n          defaultUsername\n          defaultPassword\n          path\n          supportsIngress\n          protected\n          paused\n          domains {\n            name\n          }\n        }\n      }\n    }\n  }\n":
    types.GetAppOverviewDocument,
  "\n  mutation uninstallApp($id: String!) {\n    uninstallApp(appId: $id)\n  }\n":
    types.UninstallAppDocument,
  "\n  mutation setAppProtectionState($id: String!, $protect: Boolean!) {\n    setAppProtected(appId: $id, protected: $protect)\n  }\n":
    types.SetAppProtectionStateDocument,
  "\n  mutation pauseApp($id: String!) {\n    pauseApp(appId: $id)\n  }\n":
    types.PauseAppDocument,
  "\n  mutation resumeApp($id: String!) {\n    unpauseApp(appId: $id)\n  }\n":
    types.ResumeAppDocument,
  '\n  mutation upgradeToCitadel {\n    installApp(appId: "citadel-system-wide", settings: {})\n  }\n':
    types.UpgradeToCitadelDocument,
  '\n  mutation updateNirvati {\n    updateApp(appId: "nirvati", skipDownload: true)\n  }\n':
    types.UpdateNirvatiDocument,
  "\nmutation createUser($username: String!, $name: String!, $permissions: [UserPermission!]!, $email: String, $showOnLogin: Boolean!, $password: String!) {\n    createUser(username: $username, name: $name, email: $email, permissions: $permissions, showOnLoginScreen: $showOnLogin, password: $password)\n}":
    types.CreateUserDocument,
  "\n  query getOtherUserDetails($username: String!) {\n    users {\n      user(username: $username) {\n        name\n        email\n        permissions\n        isPublic\n      }\n      me {\n        username\n      }\n    }\n  }\n":
    types.GetOtherUserDetailsDocument,
  "\n  mutation editUser(\n    $username: String!\n    $name: String!\n    $permissions: [UserPermission!]!\n    $email: String\n    $showOnLogin: Boolean!\n    $password: String\n  ) {\n    updateUser(\n      username: $username\n      name: $name\n      email: $email\n      permissions: $permissions\n      showOnLoginScreen: $showOnLogin\n      password: $password\n    )\n  }\n":
    types.EditUserDocument,
  "\n  query getLinkedDomainNames {\n    users {\n      me {\n        domains {\n          domain\n        }\n        acmeProviders\n      }\n    }\n  }\n":
    types.GetLinkedDomainNamesDocument,
  "\n  query getAppPermsWithSettings(\n    $id: String!\n    $settings: JSON!\n    $initialDomain: String\n  ) {\n    users {\n      me {\n        app(appId: $id, settings: $settings, initialDomain: $initialDomain) {\n          permissions {\n            __typename\n            ... on BuiltinPermission {\n              permission\n            }\n            ... on AppPermission {\n              appName\n              permName\n              description\n            }\n          }\n        }\n      }\n    }\n  }\n":
    types.GetAppPermsWithSettingsDocument,
  "\n  mutation installApp(\n    $id: String!\n    $domain: String\n    $acmeProvider: AcmeProvider\n    $settings: JSON!\n  ) {\n    installApp(\n      appId: $id\n      settings: $settings\n      initialDomain: $domain\n      acmeProvider: $acmeProvider\n    )\n  }\n":
    types.InstallAppDocument,
  "\n  query BasicDetails {\n    users {\n      me {\n        name\n        username\n      }\n    }\n  }\n":
    types.BasicDetailsDocument,
  "\n  mutation AddPubkey($keyId: String!, $pubkey: String!, $displayName: String!, $rpId: String!) {\n    addPubkey(keyId: $keyId, pubkeyDer: $pubkey, displayName: $displayName, rpId: $rpId)\n  }\n":
    types.AddPubkeyDocument,
  '\n  query userDetails {\n    users {\n      me {\n        name\n        permissions\n        apps(installedOnly: true) {\n          id\n          uiModule {\n            menuEntries {\n              name\n              icon\n              path\n            }\n          }\n        }\n        app(appId: "nirvati") {\n          version\n        }\n      }\n    }\n  }\n':
    types.UserDetailsDocument,
  "\n  query getAppById($id: String!) {\n    users {\n      me {\n        permissions\n        app(appId: $id) {\n          name\n          tagline\n          icon\n          description\n          repos\n          gallery\n          defaultUsername\n          defaultPassword\n          license\n          installed\n          path\n          supportsIngress\n          permissions {\n            __typename\n            ... on AppPermission {\n              appName\n              permName\n              description\n            }\n            ... on BuiltinPermission {\n              permission\n            }\n          }\n          domains {\n            name\n          }\n          settings\n          dependencies {\n            __typename\n            ... on AppMetadata {\n              id\n              name\n              installed\n              icon\n            }\n            ... on AlternativeDependency {\n              options {\n                id\n                installed\n                name\n                icon\n              }\n            }\n          }\n        }\n      }\n    }\n  }\n":
    types.GetAppByIdDocument,
  "\n  query getAvailableAppMetadata {\n    users {\n      me {\n        permissions\n        apps {\n          id\n          name\n          tagline\n          icon\n          category\n          description\n          installed\n          permissions {\n            ... on BuiltinPermission {\n              permission\n            }\n          }\n        }\n      }\n    }\n  }\n":
    types.GetAvailableAppMetadataDocument,
  "\n  query getStoreBasicById($id: String!) {\n    users {\n      me {\n        store(id: $id) {\n          name\n          icon\n        }\n      }\n    }\n  }\n":
    types.GetStoreBasicByIdDocument,
  "\n  query getStoreById($id: String!) {\n    users {\n      me {\n        store(id: $id) {\n          name\n          tagline\n          icon\n          description\n          license\n          developers\n          apps {\n            id\n            name\n            tagline\n            icon\n            category\n            description\n            installed\n            permissions {\n              ... on BuiltinPermission {\n                permission\n              }\n            }\n          }\n        }\n      }\n    }\n  }\n":
    types.GetStoreByIdDocument,
  "\n  mutation addStore($src: JSONObject!) {\n    addAppStore(src: $src, storeType: GIT)\n  }\n":
    types.AddStoreDocument,
  "\n  query getStorePlugins {\n    users {\n      me {\n        apps(installedOnly: true) {\n          storePlugins {\n            name\n            id\n            description\n            icon\n          }\n        }\n      }\n    }\n  }\n":
    types.GetStorePluginsDocument,
  "\n  query existingStores {\n    users {\n      me {\n        stores {\n          id\n          name\n          tagline\n          icon\n        }\n      }\n    }\n  }\n":
    types.ExistingStoresDocument,
  "\n  query userName {\n    users {\n      me {\n        name\n      }\n    }\n  }\n":
    types.UserNameDocument,
  "\n  query getEnabledAcmeProviders {\n    users {\n      me {\n        acmeProviders\n        permissions\n      }\n    }\n  }\n":
    types.GetEnabledAcmeProvidersDocument,
  "\n  query getVerificationCode($domain: String!) {\n    users {\n      me {\n        domainVerificationCode(domain: $domain)\n      }\n    }\n  }\n":
    types.GetVerificationCodeDocument,
  "\n  query getLinkedDomains {\n    users {\n      me {\n        domains {\n          domain\n          dnsProvider\n        }\n      }\n    }\n  }\n":
    types.GetLinkedDomainsDocument,
  "\n  query getHttpsApps {\n    users {\n      me {\n        apps(installedOnly: true) {\n          id\n          name\n          icon\n          path\n          supportsIngress\n          paused\n          domains {\n            name\n          }\n        }\n      }\n    }\n  }\n":
    types.GetHttpsAppsDocument,
  "\n  mutation addOnionDomain($app: String!) {\n    addOnionDomain(appId: $app)\n  }\n":
    types.AddOnionDomainDocument,
  "\n  mutation deleteDomain($name: String!) {\n    removeAppDomain(domain: $name)\n  }\n":
    types.DeleteDomainDocument,
  "\n  query getProxiesInfo {\n    users {\n      me {\n        proxies {\n          domain\n          routes {\n            id\n            localPath\n            targetPath\n            protocol\n            service {\n              upstream\n            }\n          }\n        }\n        services {\n          id\n          upstream\n          tcpPorts\n          udpPorts\n        }\n      }\n    }\n  }\n":
    types.GetProxiesInfoDocument,
  "\n  mutation createService(\n    $ip: String\n    $dnsName: String\n    $tcpPorts: [Int!]!\n    $udpPorts: [Int!]!\n  ) {\n    createService(\n      ip: $ip\n      dnsName: $dnsName\n      tcpPorts: $tcpPorts\n      udpPorts: $udpPorts\n    )\n  }\n":
    types.CreateServiceDocument,
  "\n  mutation addServicePorts(\n    $id: String!\n    $tcpPorts: [Int!]!\n    $udpPorts: [Int!]!\n  ) {\n    addServicePorts(id: $id, tcpPorts: $tcpPorts, udpPorts: $udpPorts)\n  }\n":
    types.AddServicePortsDocument,
  "\n  mutation createProxy($domain: String!) {\n    createProxy(domain: $domain, acmeProvider: LETS_ENCRYPT)\n  }\n":
    types.CreateProxyDocument,
  "\n  mutation deleteProxy($domain: String!) {\n    deleteProxy(domain: $domain)\n  }\n":
    types.DeleteProxyDocument,
  "\n  mutation createRoute(\n    $domain: String!\n    $serviceId: String!\n    $localPath: String!\n    $targetPath: String!\n    $targetPort: Int!\n    $protocol: Protocol!\n    $enableCompression: Boolean!\n    $allowInvalidCertificates: Boolean!\n    $targetSni: String\n  ) {\n    createRoute(\n      proxyDomain: $domain\n      serviceId: $serviceId\n      localPath: $localPath\n      targetPath: $targetPath\n      targetPort: $targetPort\n      protocol: $protocol\n      enableCompression: $enableCompression\n      allowInvalidCert: $allowInvalidCertificates\n      targetSni: $targetSni\n    )\n  }\n":
    types.CreateRouteDocument,
  "\n  mutation deleteRoute($id: String!) {\n    deleteRoute(routeId: $id)\n  }\n":
    types.DeleteRouteDocument,
  "\n  query getIsSetup {\n    info {\n      isUserSetup\n    }\n  }\n":
    types.GetIsSetupDocument,
  "\n  query getIsSetupFinished {\n    info {\n      isSetupFinished\n    }\n  }\n":
    types.GetIsSetupFinishedDocument,
  "\n  query getLoginUserInfo {\n    users {\n      loginUsernames {\n        username\n        name\n      }\n    }\n  }\n":
    types.GetLoginUserInfoDocument,
  "\n  mutation performLogin($id: String!, $password: String!) {\n    login(username: $id, password: $password)\n  }\n":
    types.PerformLoginDocument,
  "\n  query getIsSetupUserFinished {\n    users {\n      me {\n        setupFinished\n      }\n    }\n  }\n":
    types.GetIsSetupUserFinishedDocument,
  "\n  mutation createPasskeyChallenge($username: String!) {\n    createChallenge(username: $username)\n  }\n":
    types.CreatePasskeyChallengeDocument,
  "\n  mutation passkeyLogin(\n    $keyId: String!\n    $response: String!\n    $authenticatorData: String!\n    $clientDataJSON: String!\n  ) {\n    loginWithPasskey(\n      keyId: $keyId\n      signature: $response\n      authenticatorData: $authenticatorData\n      clientDataJson: $clientDataJSON\n    )\n  }\n":
    types.PasskeyLoginDocument,
  "\n  query getApp($id: String!) {\n    users {\n      me {\n        app(appId: $id) {\n          domains {\n            name\n          }\n          installed\n          name\n          icon\n        }\n      }\n    }\n  }\n":
    types.GetAppDocument,
  "\n  query getPluginJwt($audience: String!) {\n    users {\n      me {\n        jwt(audience: [$audience])\n      }\n    }\n  }\n":
    types.GetPluginJwtDocument,
  "\n  query existingPasskeys {\n    users {\n      me {\n        passkeys {\n          displayName\n          rpId\n          createdAt\n          keyIdBase64\n        }\n      }\n    }\n}":
    types.ExistingPasskeysDocument,
  "\n  mutation deletePasskey($id: String!) {\n    removePubkey(keyId: $id)\n  }\n":
    types.DeletePasskeyDocument,
  "\n  query userCustomDomainInfo {\n    users {\n      me {\n        acmeProviders\n        permissions\n      }\n    }\n    network {\n      ip\n    }\n  }\n":
    types.UserCustomDomainInfoDocument,
  "\n  query getDomainVerificationCode($domain: String!) {\n    users {\n      me {\n        domainVerificationCode(domain: $domain)\n      }\n    }\n  }\n":
    types.GetDomainVerificationCodeDocument,
  "\n  query ensureCodeExists($domain: String!) {\n    users {\n      me {\n        verifyTxt(domain: $domain)\n      }\n    }\n  }\n":
    types.EnsureCodeExistsDocument,
  "\n  mutation addVerificationCode(\n    $domain: String!\n    $dnsProvider: DnsProvider!\n    $auth: String!\n  ) {\n    addVerificationTxt(\n      domain: $domain\n      dnsAuth: $auth\n      dnsProvider: $dnsProvider\n    )\n  }\n":
    types.AddVerificationCodeDocument,
  "\n  query getBasicNetworkInfo {\n    network {\n      ip\n    }\n  }\n":
    types.GetBasicNetworkInfoDocument,
  '\n  query getUserNirvatiMeInfo {\n    network {\n      ip\n    }\n    users {\n      me {\n        nirvatiMeUsername: entropy(identifier: "nirvati.me-username")\n        nirvatiMePassword: entropy(identifier: "nirvati.me-password")\n      }\n    }\n  }\n':
    types.GetUserNirvatiMeInfoDocument,
  "\n  query getNirvatiMeAccount {\n    users {\n      me {\n        nirvatiMe {\n          name\n        }\n        acmeProviders\n      }\n    }\n  }\n":
    types.GetNirvatiMeAccountDocument,
  "\n  mutation finishSetup {\n    finishSetup\n  }\n":
    types.FinishSetupDocument,
  "\n  query getMyInitialDetails {\n    users {\n      me {\n        username\n        name\n        email\n      }\n    }\n  }\n":
    types.GetMyInitialDetailsDocument,
  "\n  mutation updateMyAccount(\n    $username: String!\n    $name: String!\n    $password: String!\n    $email: String\n    $isPublicName: Boolean\n  ) {\n    updateUser(\n      username: $username\n      name: $name\n      password: $password\n      email: $email\n      showOnLoginScreen: $isPublicName\n    )\n  }\n":
    types.UpdateMyAccountDocument,
  "\n  mutation doSetup(\n    $username: String!\n    $name: String!\n    $password: String!\n    $email: String\n    $isPublicName: Boolean!\n  ) {\n    createInitialUser(\n      username: $username\n      name: $name\n      password: $password\n      email: $email\n      isPublicName: $isPublicName\n    )\n  }\n":
    types.DoSetupDocument,
  "\n  mutation finalizeSetup {\n    installRequiredSystemApps\n  }\n":
    types.FinalizeSetupDocument,
  "\n  query customDomainInfo {\n    users {\n      me {\n        acmeProviders\n      }\n    }\n    network {\n      ip\n      tailscaleInfo {\n        ip\n      }\n    }\n  }\n":
    types.CustomDomainInfoDocument,
  "\n  query getNetworkInfo {\n    network {\n      ip\n      tailscaleInfo {\n        ip\n      }\n    }\n  }\n":
    types.GetNetworkInfoDocument,
  '\n  query getNirvatiMeInfo {\n    network {\n      ip\n      tailscaleInfo {\n        ip\n      }\n    }\n    users {\n      me {\n        nirvatiMeUsername: entropy(identifier: "nirvati.me-username")\n        nirvatiMePassword: entropy(identifier: "nirvati.me-password")\n      }\n    }\n  }\n':
    types.GetNirvatiMeInfoDocument,
  '\n  mutation installTelemetry {\n    installApp(appId: "nirvati-telemetry", settings: {})\n  }\n':
    types.InstallTelemetryDocument,
  '\n  query getDomainReadyState {\n    users {\n      me {\n        app(appId: "nirvati") {\n          domains {\n            name\n            ready\n          }\n        }\n      }\n    }\n  }\n':
    types.GetDomainReadyStateDocument,
  "\n  query getIsCitadel {\n    info {\n      isCitadelCompat\n    }\n  }\n":
    types.GetIsCitadelDocument,
  "\n  mutation tailscaleUp {\n    startTailscale\n  }\n":
    types.TailscaleUpDocument,
  "\n  query getTailscalePageInfo {\n    network {\n      tailscaleInfo {\n        authUrl\n        ip\n      }\n    }\n  }\n":
    types.GetTailscalePageInfoDocument,
  "\n  mutation startTailscaleLogin {\n    startTailscaleLogin\n  }\n":
    types.StartTailscaleLoginDocument,
  '\n  query getMyDomains {\n    users {\n      me {\n        app(appId: "nirvati") {\n          domains {\n            name\n          }\n        }\n      }\n    }\n  }\n':
    types.GetMyDomainsDocument,
  "\n  query storageInfo {\n    users {\n      me {\n        apps(installedOnly: true) {\n          name\n          icon\n          id\n          runningVolumes {\n            id\n            size\n            actualSize\n            replicaCount\n            humanReadableId\n            volumeDefinition {\n              name\n              description\n            }\n          }\n        }\n      }\n    }\n  }\n":
    types.StorageInfoDocument,
  "\n  query getAppVolume($appId: String!) {\n    users {\n      me {\n        app(appId: $appId) {\n          name\n          icon\n          runningVolumes {\n            replicaCount\n            actualSize\n            size\n            humanReadableId\n            volumeDefinition {\n              name\n              description\n            }\n          }\n        }\n      }\n    }\n  }\n":
    types.GetAppVolumeDocument,
  "\n  mutation updateReplicaCount(\n    $appId: String!\n    $volumeId: String!\n    $replicaCount: Int!\n  ) {\n    setReplicaCount(appId: $appId, volumeId: $volumeId, replicas: $replicaCount)\n  }\n":
    types.UpdateReplicaCountDocument,
  "\n  mutation updateSize($appId: String!, $volumeId: String!, $size: Int!) {\n    resizeVolume(appId: $appId, volumeId: $volumeId, size: $size)\n  }\n":
    types.UpdateSizeDocument,
  "\n  query getAvailableUpdates {\n    users {\n      me {\n        permissions\n        apps(installedOnly: true) {\n          id\n          name\n          icon\n          version\n          latestVersion {\n            latestVersion\n          }\n        }\n      }\n    }\n  }\n":
    types.GetAvailableUpdatesDocument,
  "\n  query getKubeVersion {\n    upgrades {\n      kube {\n        installedVersion\n      }\n    }\n  }\n":
    types.GetKubeVersionDocument,
  "\n  query getLatestKubeVersion {\n    upgrades {\n      kube {\n        latestVersion\n      }\n    }\n  }\n":
    types.GetLatestKubeVersionDocument,
  "\n  mutation updateKube($version: String!) {\n    upgradeKube(targetVersion: $version)\n  }\n":
    types.UpdateKubeDocument,
  "\n  mutation updateApp($id: String!) {\n    updateApp(appId: $id)\n  }\n":
    types.UpdateAppDocument,
  "\n  query getUsers {\n    users {\n      users {\n        name\n        username\n        email\n        createdAt\n      }\n    }\n  }\n":
    types.GetUsersDocument,
  "\n  query getJwt {\n    users {\n      me {\n        jwt\n      }\n    }\n  }\n":
    types.GetJwtDocument,
  "\n  mutation addDomain(\n    $domain: String!\n    $dnsProvider: DnsProvider\n    $auth: String\n    #$acmeProviders: [AcmeProvider!]!\n  ) {\n    addDomain(\n      domain: $domain\n      dnsProvider: $dnsProvider\n      dnsAuth: $auth\n      #acmeProviders: $acmeProviders\n    )\n  }\n":
    types.AddDomainDocument,
  "\n  mutation removeDomain($domain: String!) {\n    removeDomain(domain: $domain)\n  }\n":
    types.RemoveDomainDocument,
  "\n  mutation addAcmeProvider($provider: AcmeProvider!, $shareEmail: Boolean!) {\n    createAcmeIssuer(provider: $provider, shareEmail: $shareEmail)\n  }\n":
    types.AddAcmeProviderDocument,
  "\n  mutation addAppDomain(\n    $app: String!\n    $domain: String!\n    $acmeProvider: AcmeProvider!\n    $forceHttpOnly: Boolean!\n  ) {\n    addAppDomain(app: $app, domain: $domain, acmeProvider: $acmeProvider, forceHttpOnly: $forceHttpOnly)\n  }\n":
    types.AddAppDomainDocument,
  "\n  mutation performRegen {\n    generate\n  }\n":
    types.PerformRegenDocument,
  "\n  mutation performRefresh {\n    refreshSources\n  }\n":
    types.PerformRefreshDocument,
};

/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 *
 *
 * @example
 * ```ts
 * const query = graphql(`query GetUser($id: ID!) { user(id: $id) { name } }`);
 * ```
 *
 * The query argument is unknown!
 * Please regenerate the types.
 */
export function graphql(source: string): unknown;

/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function graphql(
  source: "\n  query getAppOverview {\n    users {\n      me {\n        username\n        apps(installedOnly: true) {\n          id\n          scope\n          name\n          icon\n          defaultUsername\n          defaultPassword\n          path\n          supportsIngress\n          protected\n          paused\n          domains {\n            name\n          }\n        }\n      }\n    }\n  }\n",
): (typeof documents)["\n  query getAppOverview {\n    users {\n      me {\n        username\n        apps(installedOnly: true) {\n          id\n          scope\n          name\n          icon\n          defaultUsername\n          defaultPassword\n          path\n          supportsIngress\n          protected\n          paused\n          domains {\n            name\n          }\n        }\n      }\n    }\n  }\n"];
/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function graphql(
  source: "\n  mutation uninstallApp($id: String!) {\n    uninstallApp(appId: $id)\n  }\n",
): (typeof documents)["\n  mutation uninstallApp($id: String!) {\n    uninstallApp(appId: $id)\n  }\n"];
/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function graphql(
  source: "\n  mutation setAppProtectionState($id: String!, $protect: Boolean!) {\n    setAppProtected(appId: $id, protected: $protect)\n  }\n",
): (typeof documents)["\n  mutation setAppProtectionState($id: String!, $protect: Boolean!) {\n    setAppProtected(appId: $id, protected: $protect)\n  }\n"];
/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function graphql(
  source: "\n  mutation pauseApp($id: String!) {\n    pauseApp(appId: $id)\n  }\n",
): (typeof documents)["\n  mutation pauseApp($id: String!) {\n    pauseApp(appId: $id)\n  }\n"];
/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function graphql(
  source: "\n  mutation resumeApp($id: String!) {\n    unpauseApp(appId: $id)\n  }\n",
): (typeof documents)["\n  mutation resumeApp($id: String!) {\n    unpauseApp(appId: $id)\n  }\n"];
/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function graphql(
  source: '\n  mutation upgradeToCitadel {\n    installApp(appId: "citadel-system-wide", settings: {})\n  }\n',
): (typeof documents)['\n  mutation upgradeToCitadel {\n    installApp(appId: "citadel-system-wide", settings: {})\n  }\n'];
/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function graphql(
  source: '\n  mutation updateNirvati {\n    updateApp(appId: "nirvati", skipDownload: true)\n  }\n',
): (typeof documents)['\n  mutation updateNirvati {\n    updateApp(appId: "nirvati", skipDownload: true)\n  }\n'];
/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function graphql(
  source: "\nmutation createUser($username: String!, $name: String!, $permissions: [UserPermission!]!, $email: String, $showOnLogin: Boolean!, $password: String!) {\n    createUser(username: $username, name: $name, email: $email, permissions: $permissions, showOnLoginScreen: $showOnLogin, password: $password)\n}",
): (typeof documents)["\nmutation createUser($username: String!, $name: String!, $permissions: [UserPermission!]!, $email: String, $showOnLogin: Boolean!, $password: String!) {\n    createUser(username: $username, name: $name, email: $email, permissions: $permissions, showOnLoginScreen: $showOnLogin, password: $password)\n}"];
/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function graphql(
  source: "\n  query getOtherUserDetails($username: String!) {\n    users {\n      user(username: $username) {\n        name\n        email\n        permissions\n        isPublic\n      }\n      me {\n        username\n      }\n    }\n  }\n",
): (typeof documents)["\n  query getOtherUserDetails($username: String!) {\n    users {\n      user(username: $username) {\n        name\n        email\n        permissions\n        isPublic\n      }\n      me {\n        username\n      }\n    }\n  }\n"];
/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function graphql(
  source: "\n  mutation editUser(\n    $username: String!\n    $name: String!\n    $permissions: [UserPermission!]!\n    $email: String\n    $showOnLogin: Boolean!\n    $password: String\n  ) {\n    updateUser(\n      username: $username\n      name: $name\n      email: $email\n      permissions: $permissions\n      showOnLoginScreen: $showOnLogin\n      password: $password\n    )\n  }\n",
): (typeof documents)["\n  mutation editUser(\n    $username: String!\n    $name: String!\n    $permissions: [UserPermission!]!\n    $email: String\n    $showOnLogin: Boolean!\n    $password: String\n  ) {\n    updateUser(\n      username: $username\n      name: $name\n      email: $email\n      permissions: $permissions\n      showOnLoginScreen: $showOnLogin\n      password: $password\n    )\n  }\n"];
/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function graphql(
  source: "\n  query getLinkedDomainNames {\n    users {\n      me {\n        domains {\n          domain\n        }\n        acmeProviders\n      }\n    }\n  }\n",
): (typeof documents)["\n  query getLinkedDomainNames {\n    users {\n      me {\n        domains {\n          domain\n        }\n        acmeProviders\n      }\n    }\n  }\n"];
/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function graphql(
  source: "\n  query getAppPermsWithSettings(\n    $id: String!\n    $settings: JSON!\n    $initialDomain: String\n  ) {\n    users {\n      me {\n        app(appId: $id, settings: $settings, initialDomain: $initialDomain) {\n          permissions {\n            __typename\n            ... on BuiltinPermission {\n              permission\n            }\n            ... on AppPermission {\n              appName\n              permName\n              description\n            }\n          }\n        }\n      }\n    }\n  }\n",
): (typeof documents)["\n  query getAppPermsWithSettings(\n    $id: String!\n    $settings: JSON!\n    $initialDomain: String\n  ) {\n    users {\n      me {\n        app(appId: $id, settings: $settings, initialDomain: $initialDomain) {\n          permissions {\n            __typename\n            ... on BuiltinPermission {\n              permission\n            }\n            ... on AppPermission {\n              appName\n              permName\n              description\n            }\n          }\n        }\n      }\n    }\n  }\n"];
/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function graphql(
  source: "\n  mutation installApp(\n    $id: String!\n    $domain: String\n    $acmeProvider: AcmeProvider\n    $settings: JSON!\n  ) {\n    installApp(\n      appId: $id\n      settings: $settings\n      initialDomain: $domain\n      acmeProvider: $acmeProvider\n    )\n  }\n",
): (typeof documents)["\n  mutation installApp(\n    $id: String!\n    $domain: String\n    $acmeProvider: AcmeProvider\n    $settings: JSON!\n  ) {\n    installApp(\n      appId: $id\n      settings: $settings\n      initialDomain: $domain\n      acmeProvider: $acmeProvider\n    )\n  }\n"];
/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function graphql(
  source: "\n  query BasicDetails {\n    users {\n      me {\n        name\n        username\n      }\n    }\n  }\n",
): (typeof documents)["\n  query BasicDetails {\n    users {\n      me {\n        name\n        username\n      }\n    }\n  }\n"];
/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function graphql(
  source: "\n  mutation AddPubkey($keyId: String!, $pubkey: String!, $displayName: String!, $rpId: String!) {\n    addPubkey(keyId: $keyId, pubkeyDer: $pubkey, displayName: $displayName, rpId: $rpId)\n  }\n",
): (typeof documents)["\n  mutation AddPubkey($keyId: String!, $pubkey: String!, $displayName: String!, $rpId: String!) {\n    addPubkey(keyId: $keyId, pubkeyDer: $pubkey, displayName: $displayName, rpId: $rpId)\n  }\n"];
/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function graphql(
  source: '\n  query userDetails {\n    users {\n      me {\n        name\n        permissions\n        apps(installedOnly: true) {\n          id\n          uiModule {\n            menuEntries {\n              name\n              icon\n              path\n            }\n          }\n        }\n        app(appId: "nirvati") {\n          version\n        }\n      }\n    }\n  }\n',
): (typeof documents)['\n  query userDetails {\n    users {\n      me {\n        name\n        permissions\n        apps(installedOnly: true) {\n          id\n          uiModule {\n            menuEntries {\n              name\n              icon\n              path\n            }\n          }\n        }\n        app(appId: "nirvati") {\n          version\n        }\n      }\n    }\n  }\n'];
/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function graphql(
  source: "\n  query getAppById($id: String!) {\n    users {\n      me {\n        permissions\n        app(appId: $id) {\n          name\n          tagline\n          icon\n          description\n          repos\n          gallery\n          defaultUsername\n          defaultPassword\n          license\n          installed\n          path\n          supportsIngress\n          permissions {\n            __typename\n            ... on AppPermission {\n              appName\n              permName\n              description\n            }\n            ... on BuiltinPermission {\n              permission\n            }\n          }\n          domains {\n            name\n          }\n          settings\n          dependencies {\n            __typename\n            ... on AppMetadata {\n              id\n              name\n              installed\n              icon\n            }\n            ... on AlternativeDependency {\n              options {\n                id\n                installed\n                name\n                icon\n              }\n            }\n          }\n        }\n      }\n    }\n  }\n",
): (typeof documents)["\n  query getAppById($id: String!) {\n    users {\n      me {\n        permissions\n        app(appId: $id) {\n          name\n          tagline\n          icon\n          description\n          repos\n          gallery\n          defaultUsername\n          defaultPassword\n          license\n          installed\n          path\n          supportsIngress\n          permissions {\n            __typename\n            ... on AppPermission {\n              appName\n              permName\n              description\n            }\n            ... on BuiltinPermission {\n              permission\n            }\n          }\n          domains {\n            name\n          }\n          settings\n          dependencies {\n            __typename\n            ... on AppMetadata {\n              id\n              name\n              installed\n              icon\n            }\n            ... on AlternativeDependency {\n              options {\n                id\n                installed\n                name\n                icon\n              }\n            }\n          }\n        }\n      }\n    }\n  }\n"];
/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function graphql(
  source: "\n  query getAvailableAppMetadata {\n    users {\n      me {\n        permissions\n        apps {\n          id\n          name\n          tagline\n          icon\n          category\n          description\n          installed\n          permissions {\n            ... on BuiltinPermission {\n              permission\n            }\n          }\n        }\n      }\n    }\n  }\n",
): (typeof documents)["\n  query getAvailableAppMetadata {\n    users {\n      me {\n        permissions\n        apps {\n          id\n          name\n          tagline\n          icon\n          category\n          description\n          installed\n          permissions {\n            ... on BuiltinPermission {\n              permission\n            }\n          }\n        }\n      }\n    }\n  }\n"];
/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function graphql(
  source: "\n  query getStoreBasicById($id: String!) {\n    users {\n      me {\n        store(id: $id) {\n          name\n          icon\n        }\n      }\n    }\n  }\n",
): (typeof documents)["\n  query getStoreBasicById($id: String!) {\n    users {\n      me {\n        store(id: $id) {\n          name\n          icon\n        }\n      }\n    }\n  }\n"];
/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function graphql(
  source: "\n  query getStoreById($id: String!) {\n    users {\n      me {\n        store(id: $id) {\n          name\n          tagline\n          icon\n          description\n          license\n          developers\n          apps {\n            id\n            name\n            tagline\n            icon\n            category\n            description\n            installed\n            permissions {\n              ... on BuiltinPermission {\n                permission\n              }\n            }\n          }\n        }\n      }\n    }\n  }\n",
): (typeof documents)["\n  query getStoreById($id: String!) {\n    users {\n      me {\n        store(id: $id) {\n          name\n          tagline\n          icon\n          description\n          license\n          developers\n          apps {\n            id\n            name\n            tagline\n            icon\n            category\n            description\n            installed\n            permissions {\n              ... on BuiltinPermission {\n                permission\n              }\n            }\n          }\n        }\n      }\n    }\n  }\n"];
/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function graphql(
  source: "\n  mutation addStore($src: JSONObject!) {\n    addAppStore(src: $src, storeType: GIT)\n  }\n",
): (typeof documents)["\n  mutation addStore($src: JSONObject!) {\n    addAppStore(src: $src, storeType: GIT)\n  }\n"];
/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function graphql(
  source: "\n  query getStorePlugins {\n    users {\n      me {\n        apps(installedOnly: true) {\n          storePlugins {\n            name\n            id\n            description\n            icon\n          }\n        }\n      }\n    }\n  }\n",
): (typeof documents)["\n  query getStorePlugins {\n    users {\n      me {\n        apps(installedOnly: true) {\n          storePlugins {\n            name\n            id\n            description\n            icon\n          }\n        }\n      }\n    }\n  }\n"];
/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function graphql(
  source: "\n  query existingStores {\n    users {\n      me {\n        stores {\n          id\n          name\n          tagline\n          icon\n        }\n      }\n    }\n  }\n",
): (typeof documents)["\n  query existingStores {\n    users {\n      me {\n        stores {\n          id\n          name\n          tagline\n          icon\n        }\n      }\n    }\n  }\n"];
/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function graphql(
  source: "\n  query userName {\n    users {\n      me {\n        name\n      }\n    }\n  }\n",
): (typeof documents)["\n  query userName {\n    users {\n      me {\n        name\n      }\n    }\n  }\n"];
/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function graphql(
  source: "\n  query getEnabledAcmeProviders {\n    users {\n      me {\n        acmeProviders\n        permissions\n      }\n    }\n  }\n",
): (typeof documents)["\n  query getEnabledAcmeProviders {\n    users {\n      me {\n        acmeProviders\n        permissions\n      }\n    }\n  }\n"];
/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function graphql(
  source: "\n  query getVerificationCode($domain: String!) {\n    users {\n      me {\n        domainVerificationCode(domain: $domain)\n      }\n    }\n  }\n",
): (typeof documents)["\n  query getVerificationCode($domain: String!) {\n    users {\n      me {\n        domainVerificationCode(domain: $domain)\n      }\n    }\n  }\n"];
/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function graphql(
  source: "\n  query getLinkedDomains {\n    users {\n      me {\n        domains {\n          domain\n          dnsProvider\n        }\n      }\n    }\n  }\n",
): (typeof documents)["\n  query getLinkedDomains {\n    users {\n      me {\n        domains {\n          domain\n          dnsProvider\n        }\n      }\n    }\n  }\n"];
/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function graphql(
  source: "\n  query getHttpsApps {\n    users {\n      me {\n        apps(installedOnly: true) {\n          id\n          name\n          icon\n          path\n          supportsIngress\n          paused\n          domains {\n            name\n          }\n        }\n      }\n    }\n  }\n",
): (typeof documents)["\n  query getHttpsApps {\n    users {\n      me {\n        apps(installedOnly: true) {\n          id\n          name\n          icon\n          path\n          supportsIngress\n          paused\n          domains {\n            name\n          }\n        }\n      }\n    }\n  }\n"];
/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function graphql(
  source: "\n  mutation addOnionDomain($app: String!) {\n    addOnionDomain(appId: $app)\n  }\n",
): (typeof documents)["\n  mutation addOnionDomain($app: String!) {\n    addOnionDomain(appId: $app)\n  }\n"];
/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function graphql(
  source: "\n  mutation deleteDomain($name: String!) {\n    removeAppDomain(domain: $name)\n  }\n",
): (typeof documents)["\n  mutation deleteDomain($name: String!) {\n    removeAppDomain(domain: $name)\n  }\n"];
/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function graphql(
  source: "\n  query getProxiesInfo {\n    users {\n      me {\n        proxies {\n          domain\n          routes {\n            id\n            localPath\n            targetPath\n            protocol\n            service {\n              upstream\n            }\n          }\n        }\n        services {\n          id\n          upstream\n          tcpPorts\n          udpPorts\n        }\n      }\n    }\n  }\n",
): (typeof documents)["\n  query getProxiesInfo {\n    users {\n      me {\n        proxies {\n          domain\n          routes {\n            id\n            localPath\n            targetPath\n            protocol\n            service {\n              upstream\n            }\n          }\n        }\n        services {\n          id\n          upstream\n          tcpPorts\n          udpPorts\n        }\n      }\n    }\n  }\n"];
/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function graphql(
  source: "\n  mutation createService(\n    $ip: String\n    $dnsName: String\n    $tcpPorts: [Int!]!\n    $udpPorts: [Int!]!\n  ) {\n    createService(\n      ip: $ip\n      dnsName: $dnsName\n      tcpPorts: $tcpPorts\n      udpPorts: $udpPorts\n    )\n  }\n",
): (typeof documents)["\n  mutation createService(\n    $ip: String\n    $dnsName: String\n    $tcpPorts: [Int!]!\n    $udpPorts: [Int!]!\n  ) {\n    createService(\n      ip: $ip\n      dnsName: $dnsName\n      tcpPorts: $tcpPorts\n      udpPorts: $udpPorts\n    )\n  }\n"];
/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function graphql(
  source: "\n  mutation addServicePorts(\n    $id: String!\n    $tcpPorts: [Int!]!\n    $udpPorts: [Int!]!\n  ) {\n    addServicePorts(id: $id, tcpPorts: $tcpPorts, udpPorts: $udpPorts)\n  }\n",
): (typeof documents)["\n  mutation addServicePorts(\n    $id: String!\n    $tcpPorts: [Int!]!\n    $udpPorts: [Int!]!\n  ) {\n    addServicePorts(id: $id, tcpPorts: $tcpPorts, udpPorts: $udpPorts)\n  }\n"];
/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function graphql(
  source: "\n  mutation createProxy($domain: String!) {\n    createProxy(domain: $domain, acmeProvider: LETS_ENCRYPT)\n  }\n",
): (typeof documents)["\n  mutation createProxy($domain: String!) {\n    createProxy(domain: $domain, acmeProvider: LETS_ENCRYPT)\n  }\n"];
/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function graphql(
  source: "\n  mutation deleteProxy($domain: String!) {\n    deleteProxy(domain: $domain)\n  }\n",
): (typeof documents)["\n  mutation deleteProxy($domain: String!) {\n    deleteProxy(domain: $domain)\n  }\n"];
/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function graphql(
  source: "\n  mutation createRoute(\n    $domain: String!\n    $serviceId: String!\n    $localPath: String!\n    $targetPath: String!\n    $targetPort: Int!\n    $protocol: Protocol!\n    $enableCompression: Boolean!\n    $allowInvalidCertificates: Boolean!\n    $targetSni: String\n  ) {\n    createRoute(\n      proxyDomain: $domain\n      serviceId: $serviceId\n      localPath: $localPath\n      targetPath: $targetPath\n      targetPort: $targetPort\n      protocol: $protocol\n      enableCompression: $enableCompression\n      allowInvalidCert: $allowInvalidCertificates\n      targetSni: $targetSni\n    )\n  }\n",
): (typeof documents)["\n  mutation createRoute(\n    $domain: String!\n    $serviceId: String!\n    $localPath: String!\n    $targetPath: String!\n    $targetPort: Int!\n    $protocol: Protocol!\n    $enableCompression: Boolean!\n    $allowInvalidCertificates: Boolean!\n    $targetSni: String\n  ) {\n    createRoute(\n      proxyDomain: $domain\n      serviceId: $serviceId\n      localPath: $localPath\n      targetPath: $targetPath\n      targetPort: $targetPort\n      protocol: $protocol\n      enableCompression: $enableCompression\n      allowInvalidCert: $allowInvalidCertificates\n      targetSni: $targetSni\n    )\n  }\n"];
/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function graphql(
  source: "\n  mutation deleteRoute($id: String!) {\n    deleteRoute(routeId: $id)\n  }\n",
): (typeof documents)["\n  mutation deleteRoute($id: String!) {\n    deleteRoute(routeId: $id)\n  }\n"];
/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function graphql(
  source: "\n  query getIsSetup {\n    info {\n      isUserSetup\n    }\n  }\n",
): (typeof documents)["\n  query getIsSetup {\n    info {\n      isUserSetup\n    }\n  }\n"];
/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function graphql(
  source: "\n  query getIsSetupFinished {\n    info {\n      isSetupFinished\n    }\n  }\n",
): (typeof documents)["\n  query getIsSetupFinished {\n    info {\n      isSetupFinished\n    }\n  }\n"];
/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function graphql(
  source: "\n  query getLoginUserInfo {\n    users {\n      loginUsernames {\n        username\n        name\n      }\n    }\n  }\n",
): (typeof documents)["\n  query getLoginUserInfo {\n    users {\n      loginUsernames {\n        username\n        name\n      }\n    }\n  }\n"];
/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function graphql(
  source: "\n  mutation performLogin($id: String!, $password: String!) {\n    login(username: $id, password: $password)\n  }\n",
): (typeof documents)["\n  mutation performLogin($id: String!, $password: String!) {\n    login(username: $id, password: $password)\n  }\n"];
/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function graphql(
  source: "\n  query getIsSetupUserFinished {\n    users {\n      me {\n        setupFinished\n      }\n    }\n  }\n",
): (typeof documents)["\n  query getIsSetupUserFinished {\n    users {\n      me {\n        setupFinished\n      }\n    }\n  }\n"];
/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function graphql(
  source: "\n  mutation createPasskeyChallenge($username: String!) {\n    createChallenge(username: $username)\n  }\n",
): (typeof documents)["\n  mutation createPasskeyChallenge($username: String!) {\n    createChallenge(username: $username)\n  }\n"];
/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function graphql(
  source: "\n  mutation passkeyLogin(\n    $keyId: String!\n    $response: String!\n    $authenticatorData: String!\n    $clientDataJSON: String!\n  ) {\n    loginWithPasskey(\n      keyId: $keyId\n      signature: $response\n      authenticatorData: $authenticatorData\n      clientDataJson: $clientDataJSON\n    )\n  }\n",
): (typeof documents)["\n  mutation passkeyLogin(\n    $keyId: String!\n    $response: String!\n    $authenticatorData: String!\n    $clientDataJSON: String!\n  ) {\n    loginWithPasskey(\n      keyId: $keyId\n      signature: $response\n      authenticatorData: $authenticatorData\n      clientDataJson: $clientDataJSON\n    )\n  }\n"];
/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function graphql(
  source: "\n  query getApp($id: String!) {\n    users {\n      me {\n        app(appId: $id) {\n          domains {\n            name\n          }\n          installed\n          name\n          icon\n        }\n      }\n    }\n  }\n",
): (typeof documents)["\n  query getApp($id: String!) {\n    users {\n      me {\n        app(appId: $id) {\n          domains {\n            name\n          }\n          installed\n          name\n          icon\n        }\n      }\n    }\n  }\n"];
/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function graphql(
  source: "\n  query getPluginJwt($audience: String!) {\n    users {\n      me {\n        jwt(audience: [$audience])\n      }\n    }\n  }\n",
): (typeof documents)["\n  query getPluginJwt($audience: String!) {\n    users {\n      me {\n        jwt(audience: [$audience])\n      }\n    }\n  }\n"];
/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function graphql(
  source: "\n  query existingPasskeys {\n    users {\n      me {\n        passkeys {\n          displayName\n          rpId\n          createdAt\n          keyIdBase64\n        }\n      }\n    }\n}",
): (typeof documents)["\n  query existingPasskeys {\n    users {\n      me {\n        passkeys {\n          displayName\n          rpId\n          createdAt\n          keyIdBase64\n        }\n      }\n    }\n}"];
/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function graphql(
  source: "\n  mutation deletePasskey($id: String!) {\n    removePubkey(keyId: $id)\n  }\n",
): (typeof documents)["\n  mutation deletePasskey($id: String!) {\n    removePubkey(keyId: $id)\n  }\n"];
/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function graphql(
  source: "\n  query userCustomDomainInfo {\n    users {\n      me {\n        acmeProviders\n        permissions\n      }\n    }\n    network {\n      ip\n    }\n  }\n",
): (typeof documents)["\n  query userCustomDomainInfo {\n    users {\n      me {\n        acmeProviders\n        permissions\n      }\n    }\n    network {\n      ip\n    }\n  }\n"];
/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function graphql(
  source: "\n  query getDomainVerificationCode($domain: String!) {\n    users {\n      me {\n        domainVerificationCode(domain: $domain)\n      }\n    }\n  }\n",
): (typeof documents)["\n  query getDomainVerificationCode($domain: String!) {\n    users {\n      me {\n        domainVerificationCode(domain: $domain)\n      }\n    }\n  }\n"];
/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function graphql(
  source: "\n  query ensureCodeExists($domain: String!) {\n    users {\n      me {\n        verifyTxt(domain: $domain)\n      }\n    }\n  }\n",
): (typeof documents)["\n  query ensureCodeExists($domain: String!) {\n    users {\n      me {\n        verifyTxt(domain: $domain)\n      }\n    }\n  }\n"];
/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function graphql(
  source: "\n  mutation addVerificationCode(\n    $domain: String!\n    $dnsProvider: DnsProvider!\n    $auth: String!\n  ) {\n    addVerificationTxt(\n      domain: $domain\n      dnsAuth: $auth\n      dnsProvider: $dnsProvider\n    )\n  }\n",
): (typeof documents)["\n  mutation addVerificationCode(\n    $domain: String!\n    $dnsProvider: DnsProvider!\n    $auth: String!\n  ) {\n    addVerificationTxt(\n      domain: $domain\n      dnsAuth: $auth\n      dnsProvider: $dnsProvider\n    )\n  }\n"];
/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function graphql(
  source: "\n  query getBasicNetworkInfo {\n    network {\n      ip\n    }\n  }\n",
): (typeof documents)["\n  query getBasicNetworkInfo {\n    network {\n      ip\n    }\n  }\n"];
/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function graphql(
  source: '\n  query getUserNirvatiMeInfo {\n    network {\n      ip\n    }\n    users {\n      me {\n        nirvatiMeUsername: entropy(identifier: "nirvati.me-username")\n        nirvatiMePassword: entropy(identifier: "nirvati.me-password")\n      }\n    }\n  }\n',
): (typeof documents)['\n  query getUserNirvatiMeInfo {\n    network {\n      ip\n    }\n    users {\n      me {\n        nirvatiMeUsername: entropy(identifier: "nirvati.me-username")\n        nirvatiMePassword: entropy(identifier: "nirvati.me-password")\n      }\n    }\n  }\n'];
/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function graphql(
  source: "\n  query getNirvatiMeAccount {\n    users {\n      me {\n        nirvatiMe {\n          name\n        }\n        acmeProviders\n      }\n    }\n  }\n",
): (typeof documents)["\n  query getNirvatiMeAccount {\n    users {\n      me {\n        nirvatiMe {\n          name\n        }\n        acmeProviders\n      }\n    }\n  }\n"];
/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function graphql(
  source: "\n  mutation finishSetup {\n    finishSetup\n  }\n",
): (typeof documents)["\n  mutation finishSetup {\n    finishSetup\n  }\n"];
/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function graphql(
  source: "\n  query getMyInitialDetails {\n    users {\n      me {\n        username\n        name\n        email\n      }\n    }\n  }\n",
): (typeof documents)["\n  query getMyInitialDetails {\n    users {\n      me {\n        username\n        name\n        email\n      }\n    }\n  }\n"];
/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function graphql(
  source: "\n  mutation updateMyAccount(\n    $username: String!\n    $name: String!\n    $password: String!\n    $email: String\n    $isPublicName: Boolean\n  ) {\n    updateUser(\n      username: $username\n      name: $name\n      password: $password\n      email: $email\n      showOnLoginScreen: $isPublicName\n    )\n  }\n",
): (typeof documents)["\n  mutation updateMyAccount(\n    $username: String!\n    $name: String!\n    $password: String!\n    $email: String\n    $isPublicName: Boolean\n  ) {\n    updateUser(\n      username: $username\n      name: $name\n      password: $password\n      email: $email\n      showOnLoginScreen: $isPublicName\n    )\n  }\n"];
/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function graphql(
  source: "\n  mutation doSetup(\n    $username: String!\n    $name: String!\n    $password: String!\n    $email: String\n    $isPublicName: Boolean!\n  ) {\n    createInitialUser(\n      username: $username\n      name: $name\n      password: $password\n      email: $email\n      isPublicName: $isPublicName\n    )\n  }\n",
): (typeof documents)["\n  mutation doSetup(\n    $username: String!\n    $name: String!\n    $password: String!\n    $email: String\n    $isPublicName: Boolean!\n  ) {\n    createInitialUser(\n      username: $username\n      name: $name\n      password: $password\n      email: $email\n      isPublicName: $isPublicName\n    )\n  }\n"];
/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function graphql(
  source: "\n  mutation finalizeSetup {\n    installRequiredSystemApps\n  }\n",
): (typeof documents)["\n  mutation finalizeSetup {\n    installRequiredSystemApps\n  }\n"];
/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function graphql(
  source: "\n  query customDomainInfo {\n    users {\n      me {\n        acmeProviders\n      }\n    }\n    network {\n      ip\n      tailscaleInfo {\n        ip\n      }\n    }\n  }\n",
): (typeof documents)["\n  query customDomainInfo {\n    users {\n      me {\n        acmeProviders\n      }\n    }\n    network {\n      ip\n      tailscaleInfo {\n        ip\n      }\n    }\n  }\n"];
/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function graphql(
  source: "\n  query getNetworkInfo {\n    network {\n      ip\n      tailscaleInfo {\n        ip\n      }\n    }\n  }\n",
): (typeof documents)["\n  query getNetworkInfo {\n    network {\n      ip\n      tailscaleInfo {\n        ip\n      }\n    }\n  }\n"];
/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function graphql(
  source: '\n  query getNirvatiMeInfo {\n    network {\n      ip\n      tailscaleInfo {\n        ip\n      }\n    }\n    users {\n      me {\n        nirvatiMeUsername: entropy(identifier: "nirvati.me-username")\n        nirvatiMePassword: entropy(identifier: "nirvati.me-password")\n      }\n    }\n  }\n',
): (typeof documents)['\n  query getNirvatiMeInfo {\n    network {\n      ip\n      tailscaleInfo {\n        ip\n      }\n    }\n    users {\n      me {\n        nirvatiMeUsername: entropy(identifier: "nirvati.me-username")\n        nirvatiMePassword: entropy(identifier: "nirvati.me-password")\n      }\n    }\n  }\n'];
/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function graphql(
  source: '\n  mutation installTelemetry {\n    installApp(appId: "nirvati-telemetry", settings: {})\n  }\n',
): (typeof documents)['\n  mutation installTelemetry {\n    installApp(appId: "nirvati-telemetry", settings: {})\n  }\n'];
/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function graphql(
  source: '\n  query getDomainReadyState {\n    users {\n      me {\n        app(appId: "nirvati") {\n          domains {\n            name\n            ready\n          }\n        }\n      }\n    }\n  }\n',
): (typeof documents)['\n  query getDomainReadyState {\n    users {\n      me {\n        app(appId: "nirvati") {\n          domains {\n            name\n            ready\n          }\n        }\n      }\n    }\n  }\n'];
/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function graphql(
  source: "\n  query getIsCitadel {\n    info {\n      isCitadelCompat\n    }\n  }\n",
): (typeof documents)["\n  query getIsCitadel {\n    info {\n      isCitadelCompat\n    }\n  }\n"];
/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function graphql(
  source: "\n  mutation tailscaleUp {\n    startTailscale\n  }\n",
): (typeof documents)["\n  mutation tailscaleUp {\n    startTailscale\n  }\n"];
/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function graphql(
  source: "\n  query getTailscalePageInfo {\n    network {\n      tailscaleInfo {\n        authUrl\n        ip\n      }\n    }\n  }\n",
): (typeof documents)["\n  query getTailscalePageInfo {\n    network {\n      tailscaleInfo {\n        authUrl\n        ip\n      }\n    }\n  }\n"];
/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function graphql(
  source: "\n  mutation startTailscaleLogin {\n    startTailscaleLogin\n  }\n",
): (typeof documents)["\n  mutation startTailscaleLogin {\n    startTailscaleLogin\n  }\n"];
/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function graphql(
  source: '\n  query getMyDomains {\n    users {\n      me {\n        app(appId: "nirvati") {\n          domains {\n            name\n          }\n        }\n      }\n    }\n  }\n',
): (typeof documents)['\n  query getMyDomains {\n    users {\n      me {\n        app(appId: "nirvati") {\n          domains {\n            name\n          }\n        }\n      }\n    }\n  }\n'];
/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function graphql(
  source: "\n  query storageInfo {\n    users {\n      me {\n        apps(installedOnly: true) {\n          name\n          icon\n          id\n          runningVolumes {\n            id\n            size\n            actualSize\n            replicaCount\n            humanReadableId\n            volumeDefinition {\n              name\n              description\n            }\n          }\n        }\n      }\n    }\n  }\n",
): (typeof documents)["\n  query storageInfo {\n    users {\n      me {\n        apps(installedOnly: true) {\n          name\n          icon\n          id\n          runningVolumes {\n            id\n            size\n            actualSize\n            replicaCount\n            humanReadableId\n            volumeDefinition {\n              name\n              description\n            }\n          }\n        }\n      }\n    }\n  }\n"];
/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function graphql(
  source: "\n  query getAppVolume($appId: String!) {\n    users {\n      me {\n        app(appId: $appId) {\n          name\n          icon\n          runningVolumes {\n            replicaCount\n            actualSize\n            size\n            humanReadableId\n            volumeDefinition {\n              name\n              description\n            }\n          }\n        }\n      }\n    }\n  }\n",
): (typeof documents)["\n  query getAppVolume($appId: String!) {\n    users {\n      me {\n        app(appId: $appId) {\n          name\n          icon\n          runningVolumes {\n            replicaCount\n            actualSize\n            size\n            humanReadableId\n            volumeDefinition {\n              name\n              description\n            }\n          }\n        }\n      }\n    }\n  }\n"];
/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function graphql(
  source: "\n  mutation updateReplicaCount(\n    $appId: String!\n    $volumeId: String!\n    $replicaCount: Int!\n  ) {\n    setReplicaCount(appId: $appId, volumeId: $volumeId, replicas: $replicaCount)\n  }\n",
): (typeof documents)["\n  mutation updateReplicaCount(\n    $appId: String!\n    $volumeId: String!\n    $replicaCount: Int!\n  ) {\n    setReplicaCount(appId: $appId, volumeId: $volumeId, replicas: $replicaCount)\n  }\n"];
/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function graphql(
  source: "\n  mutation updateSize($appId: String!, $volumeId: String!, $size: Int!) {\n    resizeVolume(appId: $appId, volumeId: $volumeId, size: $size)\n  }\n",
): (typeof documents)["\n  mutation updateSize($appId: String!, $volumeId: String!, $size: Int!) {\n    resizeVolume(appId: $appId, volumeId: $volumeId, size: $size)\n  }\n"];
/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function graphql(
  source: "\n  query getAvailableUpdates {\n    users {\n      me {\n        permissions\n        apps(installedOnly: true) {\n          id\n          name\n          icon\n          version\n          latestVersion {\n            latestVersion\n          }\n        }\n      }\n    }\n  }\n",
): (typeof documents)["\n  query getAvailableUpdates {\n    users {\n      me {\n        permissions\n        apps(installedOnly: true) {\n          id\n          name\n          icon\n          version\n          latestVersion {\n            latestVersion\n          }\n        }\n      }\n    }\n  }\n"];
/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function graphql(
  source: "\n  query getKubeVersion {\n    upgrades {\n      kube {\n        installedVersion\n      }\n    }\n  }\n",
): (typeof documents)["\n  query getKubeVersion {\n    upgrades {\n      kube {\n        installedVersion\n      }\n    }\n  }\n"];
/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function graphql(
  source: "\n  query getLatestKubeVersion {\n    upgrades {\n      kube {\n        latestVersion\n      }\n    }\n  }\n",
): (typeof documents)["\n  query getLatestKubeVersion {\n    upgrades {\n      kube {\n        latestVersion\n      }\n    }\n  }\n"];
/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function graphql(
  source: "\n  mutation updateKube($version: String!) {\n    upgradeKube(targetVersion: $version)\n  }\n",
): (typeof documents)["\n  mutation updateKube($version: String!) {\n    upgradeKube(targetVersion: $version)\n  }\n"];
/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function graphql(
  source: "\n  mutation updateApp($id: String!) {\n    updateApp(appId: $id)\n  }\n",
): (typeof documents)["\n  mutation updateApp($id: String!) {\n    updateApp(appId: $id)\n  }\n"];
/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function graphql(
  source: "\n  query getUsers {\n    users {\n      users {\n        name\n        username\n        email\n        createdAt\n      }\n    }\n  }\n",
): (typeof documents)["\n  query getUsers {\n    users {\n      users {\n        name\n        username\n        email\n        createdAt\n      }\n    }\n  }\n"];
/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function graphql(
  source: "\n  query getJwt {\n    users {\n      me {\n        jwt\n      }\n    }\n  }\n",
): (typeof documents)["\n  query getJwt {\n    users {\n      me {\n        jwt\n      }\n    }\n  }\n"];
/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function graphql(
  source: "\n  mutation addDomain(\n    $domain: String!\n    $dnsProvider: DnsProvider\n    $auth: String\n    #$acmeProviders: [AcmeProvider!]!\n  ) {\n    addDomain(\n      domain: $domain\n      dnsProvider: $dnsProvider\n      dnsAuth: $auth\n      #acmeProviders: $acmeProviders\n    )\n  }\n",
): (typeof documents)["\n  mutation addDomain(\n    $domain: String!\n    $dnsProvider: DnsProvider\n    $auth: String\n    #$acmeProviders: [AcmeProvider!]!\n  ) {\n    addDomain(\n      domain: $domain\n      dnsProvider: $dnsProvider\n      dnsAuth: $auth\n      #acmeProviders: $acmeProviders\n    )\n  }\n"];
/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function graphql(
  source: "\n  mutation removeDomain($domain: String!) {\n    removeDomain(domain: $domain)\n  }\n",
): (typeof documents)["\n  mutation removeDomain($domain: String!) {\n    removeDomain(domain: $domain)\n  }\n"];
/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function graphql(
  source: "\n  mutation addAcmeProvider($provider: AcmeProvider!, $shareEmail: Boolean!) {\n    createAcmeIssuer(provider: $provider, shareEmail: $shareEmail)\n  }\n",
): (typeof documents)["\n  mutation addAcmeProvider($provider: AcmeProvider!, $shareEmail: Boolean!) {\n    createAcmeIssuer(provider: $provider, shareEmail: $shareEmail)\n  }\n"];
/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function graphql(
  source: "\n  mutation addAppDomain(\n    $app: String!\n    $domain: String!\n    $acmeProvider: AcmeProvider!\n    $forceHttpOnly: Boolean!\n  ) {\n    addAppDomain(app: $app, domain: $domain, acmeProvider: $acmeProvider, forceHttpOnly: $forceHttpOnly)\n  }\n",
): (typeof documents)["\n  mutation addAppDomain(\n    $app: String!\n    $domain: String!\n    $acmeProvider: AcmeProvider!\n    $forceHttpOnly: Boolean!\n  ) {\n    addAppDomain(app: $app, domain: $domain, acmeProvider: $acmeProvider, forceHttpOnly: $forceHttpOnly)\n  }\n"];
/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function graphql(
  source: "\n  mutation performRegen {\n    generate\n  }\n",
): (typeof documents)["\n  mutation performRegen {\n    generate\n  }\n"];
/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function graphql(
  source: "\n  mutation performRefresh {\n    refreshSources\n  }\n",
): (typeof documents)["\n  mutation performRefresh {\n    refreshSources\n  }\n"];

export function graphql(source: string) {
  return (documents as any)[source] ?? {};
}

export type DocumentType<TDocumentNode extends DocumentNode<any, any>> =
  TDocumentNode extends DocumentNode<infer TType, any> ? TType : never;

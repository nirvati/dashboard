/* eslint-disable */
// This file is generated automatically. DO NOT EDIT IT MANUALLY.
import type { Setting } from "../customTypes";
import type { TypedDocumentNode as DocumentNode } from "@graphql-typed-document-node/core";
export type Maybe<T> = T | null;
export type InputMaybe<T> = Maybe<T>;
export type Exact<T extends { [key: string]: unknown }> = {
  [K in keyof T]: T[K];
};
export type MakeOptional<T, K extends keyof T> = Omit<T, K> & {
  [SubKey in K]?: Maybe<T[SubKey]>;
};
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> & {
  [SubKey in K]: Maybe<T[SubKey]>;
};
export type MakeEmpty<
  T extends { [key: string]: unknown },
  K extends keyof T,
> = { [_ in K]?: never };
export type Incremental<T> =
  | T
  | {
      [P in keyof T]?: P extends " $fragmentName" | "__typename" ? T[P] : never;
    };
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: { input: string; output: string };
  String: { input: string; output: string };
  Boolean: { input: boolean; output: boolean };
  Int: { input: number; output: number };
  Float: { input: number; output: number };
  /**
   * Implement the DateTime<Utc> scalar
   *
   * The input/output is a string in RFC3339 format.
   */
  DateTime: { input: any; output: any };
  /** A scalar that can represent any JSON value. */
  JSON: { input: { [key: string]: any }; output: { [key: string]: any } };
  /** A scalar that can represent any JSON Object value. */
  JSONObject: { input: { [key: string]: any }; output: { [key: string]: any } };
  MultiLanguageItem: {
    input: Record<string, string>;
    output: Record<string, string>;
  };
  Settings: { input: Record<string, Setting>; output: Record<string, Setting> };
};

export enum AcmeProvider {
  BuyPass = "BUY_PASS",
  LetsEncrypt = "LETS_ENCRYPT",
}

export type AlternativeDependency = {
  __typename?: "AlternativeDependency";
  options: Array<AppMetadata>;
};

export type AppComponent = {
  __typename?: "AppComponent";
  /** A description of the component */
  description: Scalars["MultiLanguageItem"]["output"];
  /** The component id */
  id: Scalars["String"]["output"];
  /** The name of the component */
  name: Scalars["MultiLanguageItem"]["output"];
  /** Whether the component is required */
  required: Scalars["Boolean"]["output"];
};

export type AppDomain = {
  __typename?: "AppDomain";
  app: AppMetadata;
  createdAt: Scalars["DateTime"]["output"];
  name: Scalars["String"]["output"];
  parentDomain?: Maybe<UserDomain>;
  ready: Scalars["Boolean"]["output"];
  user: User;
};

export type AppInstallable = {
  __typename?: "AppInstallable";
  canInstall: Scalars["Boolean"]["output"];
  conflictingUsers: Array<Scalars["String"]["output"]>;
};

export type AppMetadata = {
  __typename?: "AppMetadata";
  /** The scopes this app can be installed to */
  allowedScopes: Array<AppScope>;
  /** The category for the app */
  category: Scalars["MultiLanguageItem"]["output"];
  components: Array<AppComponent>;
  /** The app's default password. */
  defaultPassword?: Maybe<Scalars["String"]["output"]>;
  /** The default scopes */
  defaultScope: AppScope;
  /** The app's default username */
  defaultUsername?: Maybe<Scalars["String"]["output"]>;
  dependencies: Array<Dependency>;
  /** A description of the app */
  description: Scalars["MultiLanguageItem"]["output"];
  developers: Scalars["JSON"]["output"];
  domains: Array<AppDomain>;
  /** Permissions this app exposes */
  exposesPermissions: Array<Permission>;
  /** A list of promo images for the apps */
  gallery: Array<Scalars["String"]["output"]>;
  /** The URL to the app icon */
  icon?: Maybe<Scalars["String"]["output"]>;
  /** The app id */
  id: Scalars["String"]["output"];
  /** For "virtual" apps, the service the app implements */
  implements?: Maybe<Scalars["String"]["output"]>;
  installable: AppInstallable;
  installed: Scalars["Boolean"]["output"];
  /** True if the app is running in the Citadel compat layer */
  isCitadel: Scalars["Boolean"]["output"];
  latestVersion?: Maybe<AppUpdate>;
  /** The SPDX identifier of the app license */
  license: Scalars["String"]["output"];
  /** The name of the app */
  name: Scalars["MultiLanguageItem"]["output"];
  owner: User;
  /** The path the "Open" link on the dashboard should lead to */
  path?: Maybe<Scalars["String"]["output"]>;
  paused: Scalars["Boolean"]["output"];
  permissions: Array<AppRequestedPermission>;
  /**
   * Ports this app uses
   * Before installing, agent clients need to check if any of these ports are already in use by other apps
   * If so, the user needs to be notified
   */
  ports: Array<Scalars["Int"]["output"]>;
  /** URL to redirect to post-install */
  postInstallRedirect?: Maybe<Scalars["String"]["output"]>;
  protected: Scalars["Boolean"]["output"];
  /** True if the app receives ingress as TCP (without TLS being handled by Traefik) instead of HTTP */
  rawIngress: Scalars["Boolean"]["output"];
  releaseNotes: Scalars["JSONObject"]["output"];
  /** App repository name -> repo URL */
  repos: Scalars["JSONObject"]["output"];
  runningVolumes: Array<Volume>;
  saasCompatibility: SaasCompatibility;
  /** Whether this app is installed system-wide or per user */
  scope: AppScope;
  /** Available settings for this app */
  settings: Scalars["Settings"]["output"];
  storePlugins: Array<StorePlugin>;
  /** A support link for the app */
  support: Scalars["String"]["output"];
  /**
   * Whether the app supports ingress
   * This is declared by agent based on whether an app's ingress vec is empty,
   * setting it during generation has no effect
   */
  supportsIngress: Scalars["Boolean"]["output"];
  /** A short tagline for the app */
  tagline: Scalars["MultiLanguageItem"]["output"];
  uiModule?: Maybe<UiModule>;
  /** The version of the app to display */
  version: Scalars["String"]["output"];
  /** Volumes this app exposes */
  volumes: Scalars["JSONObject"]["output"];
};

export type AppPermission = {
  __typename?: "AppPermission";
  appName: Scalars["MultiLanguageItem"]["output"];
  description: Scalars["MultiLanguageItem"]["output"];
  hidden: Array<Scalars["String"]["output"]>;
  permName: Scalars["MultiLanguageItem"]["output"];
};

export type AppRequestedPermission = AppPermission | BuiltinPermission;

export enum AppScope {
  System = "SYSTEM",
  User = "USER",
}

export type AppStore = {
  __typename?: "AppStore";
  apps: Array<AppMetadata>;
  description: Scalars["MultiLanguageItem"]["output"];
  developers: Scalars["JSONObject"]["output"];
  icon: Scalars["String"]["output"];
  id: Scalars["String"]["output"];
  license: Scalars["String"]["output"];
  name: Scalars["MultiLanguageItem"]["output"];
  owner: User;
  src: Scalars["JSONObject"]["output"];
  tagline: Scalars["MultiLanguageItem"]["output"];
  type: StoreType;
};

export type AppUpdate = {
  __typename?: "AppUpdate";
  latestVersion: Scalars["String"]["output"];
  newPermissions: Array<Scalars["String"]["output"]>;
  releasesNotes: Scalars["JSON"]["output"];
  removedPermissions: Array<Scalars["String"]["output"]>;
};

export type BuiltinPermission = {
  __typename?: "BuiltinPermission";
  permission: Scalars["String"]["output"];
};

export type CitadelImportResult = {
  __typename?: "CitadelImportResult";
  failedApps: Array<Scalars["String"]["output"]>;
};

export type Dependency = AlternativeDependency | AppMetadata;

export enum DnsProvider {
  Cloudflare = "CLOUDFLARE",
  NirvatiMe = "NIRVATI_ME",
}

export type Info = {
  __typename?: "Info";
  isCitadelCompat: Scalars["Boolean"]["output"];
  isSetupFinished: Scalars["Boolean"]["output"];
  isUserSetup: Scalars["Boolean"]["output"];
};

export type KubeInfo = {
  __typename?: "KubeInfo";
  installedVersion: Scalars["String"]["output"];
  /** Returns the latest version of Kubernetes that we can safely upgrade to, taking version skew into account. */
  latestVersion?: Maybe<Scalars["String"]["output"]>;
};

export type LoginUser = {
  __typename?: "LoginUser";
  avatar?: Maybe<Scalars["String"]["output"]>;
  name: Scalars["String"]["output"];
  username: Scalars["String"]["output"];
};

export type Mutation = {
  __typename?: "Mutation";
  addAppDomain: Scalars["Boolean"]["output"];
  addAppStore: Scalars["Boolean"]["output"];
  addDomain: Scalars["Boolean"]["output"];
  addOnionDomain: Scalars["String"]["output"];
  addPubkey: Scalars["Boolean"]["output"];
  addServicePorts: Scalars["Boolean"]["output"];
  addVerificationTxt: Scalars["Boolean"]["output"];
  createAcmeIssuer: Scalars["Boolean"]["output"];
  createChallenge: Scalars["String"]["output"];
  createInitialUser: Scalars["Boolean"]["output"];
  createProxy: Scalars["Boolean"]["output"];
  createRoute: Scalars["String"]["output"];
  createService: Scalars["String"]["output"];
  createUser: Scalars["Boolean"]["output"];
  createUserGroup: Scalars["Boolean"]["output"];
  deleteAcmeIssuer: Scalars["Boolean"]["output"];
  deleteProxy: Scalars["Boolean"]["output"];
  deleteRoute: Scalars["Boolean"]["output"];
  deleteService: Scalars["Boolean"]["output"];
  deleteUser: Scalars["Boolean"]["output"];
  finishCitadelImport: CitadelImportResult;
  finishSetup: Scalars["Boolean"]["output"];
  generate: Scalars["Boolean"]["output"];
  generateTotpUrl: Scalars["String"]["output"];
  installApp: Scalars["Boolean"]["output"];
  installRequiredSystemApps: Scalars["Boolean"]["output"];
  login?: Maybe<Scalars["String"]["output"]>;
  loginWithPasskey: Scalars["String"]["output"];
  loginWithTotp: Scalars["String"]["output"];
  pauseApp: Scalars["Boolean"]["output"];
  refreshSources: Scalars["Boolean"]["output"];
  removeAppDomain: Scalars["Boolean"]["output"];
  removeAppStore: Scalars["Boolean"]["output"];
  removeDomain: Scalars["Boolean"]["output"];
  removePubkey: Scalars["Boolean"]["output"];
  removeTotp: Scalars["Boolean"]["output"];
  resizeVolume: Scalars["Boolean"]["output"];
  saveTotp: Scalars["Boolean"]["output"];
  setAppProtected: Scalars["Boolean"]["output"];
  setReplicaCount: Scalars["Boolean"]["output"];
  startTailscale: Scalars["Boolean"]["output"];
  startTailscaleLogin: Scalars["Boolean"]["output"];
  uninstallApp: Scalars["Boolean"]["output"];
  unpauseApp: Scalars["Boolean"]["output"];
  updateApp: Scalars["Boolean"]["output"];
  updateAppSettings: Scalars["Boolean"]["output"];
  updateUser: Scalars["Boolean"]["output"];
  upgradeKube: Scalars["Boolean"]["output"];
};

export type MutationAddAppDomainArgs = {
  acmeProvider: AcmeProvider;
  app: Scalars["String"]["input"];
  appComponent?: InputMaybe<Scalars["String"]["input"]>;
  customPrefix?: InputMaybe<Scalars["String"]["input"]>;
  domain: Scalars["String"]["input"];
  forceHttpOnly: Scalars["Boolean"]["input"];
};

export type MutationAddAppStoreArgs = {
  src: Scalars["JSONObject"]["input"];
  storePlugin?: InputMaybe<Scalars["String"]["input"]>;
  storeType: StoreType;
};

export type MutationAddDomainArgs = {
  dnsAuth?: InputMaybe<Scalars["String"]["input"]>;
  dnsProvider?: InputMaybe<DnsProvider>;
  domain: Scalars["String"]["input"];
};

export type MutationAddOnionDomainArgs = {
  appComponent?: InputMaybe<Scalars["String"]["input"]>;
  appId: Scalars["String"]["input"];
};

export type MutationAddPubkeyArgs = {
  displayName: Scalars["String"]["input"];
  keyId: Scalars["String"]["input"];
  pubkeyDer: Scalars["String"]["input"];
  rpId: Scalars["String"]["input"];
};

export type MutationAddServicePortsArgs = {
  id: Scalars["String"]["input"];
  tcpPorts: Array<Scalars["Int"]["input"]>;
  udpPorts: Array<Scalars["Int"]["input"]>;
};

export type MutationAddVerificationTxtArgs = {
  dnsAuth: Scalars["String"]["input"];
  dnsProvider: DnsProvider;
  domain: Scalars["String"]["input"];
};

export type MutationCreateAcmeIssuerArgs = {
  provider: AcmeProvider;
  shareEmail?: InputMaybe<Scalars["Boolean"]["input"]>;
};

export type MutationCreateChallengeArgs = {
  username: Scalars["String"]["input"];
};

export type MutationCreateInitialUserArgs = {
  email?: InputMaybe<Scalars["String"]["input"]>;
  isPublicName: Scalars["Boolean"]["input"];
  name: Scalars["String"]["input"];
  password: Scalars["String"]["input"];
  username: Scalars["String"]["input"];
};

export type MutationCreateProxyArgs = {
  acmeProvider: AcmeProvider;
  domain: Scalars["String"]["input"];
};

export type MutationCreateRouteArgs = {
  allowInvalidCert: Scalars["Boolean"]["input"];
  enableCompression: Scalars["Boolean"]["input"];
  localPath: Scalars["String"]["input"];
  protocol: Protocol;
  proxyDomain: Scalars["String"]["input"];
  serviceId: Scalars["String"]["input"];
  targetPath: Scalars["String"]["input"];
  targetPort: Scalars["Int"]["input"];
  targetSni?: InputMaybe<Scalars["String"]["input"]>;
};

export type MutationCreateServiceArgs = {
  dnsName?: InputMaybe<Scalars["String"]["input"]>;
  ip?: InputMaybe<Scalars["String"]["input"]>;
  tcpPorts: Array<Scalars["Int"]["input"]>;
  udpPorts: Array<Scalars["Int"]["input"]>;
};

export type MutationCreateUserArgs = {
  avatar?: InputMaybe<Scalars["String"]["input"]>;
  email?: InputMaybe<Scalars["String"]["input"]>;
  group?: InputMaybe<Scalars["String"]["input"]>;
  name: Scalars["String"]["input"];
  password: Scalars["String"]["input"];
  permissions?: InputMaybe<Array<UserPermission>>;
  showOnLoginScreen: Scalars["Boolean"]["input"];
  username: Scalars["String"]["input"];
};

export type MutationCreateUserGroupArgs = {
  name: Scalars["String"]["input"];
};

export type MutationDeleteAcmeIssuerArgs = {
  provider: AcmeProvider;
};

export type MutationDeleteProxyArgs = {
  domain: Scalars["String"]["input"];
};

export type MutationDeleteRouteArgs = {
  routeId: Scalars["String"]["input"];
};

export type MutationDeleteServiceArgs = {
  id: Scalars["String"]["input"];
};

export type MutationDeleteUserArgs = {
  username: Scalars["String"]["input"];
};

export type MutationInstallAppArgs = {
  acmeProvider?: InputMaybe<AcmeProvider>;
  appId: Scalars["String"]["input"];
  initialDomain?: InputMaybe<Scalars["String"]["input"]>;
  protect?: InputMaybe<Scalars["Boolean"]["input"]>;
  settings: Scalars["JSON"]["input"];
};

export type MutationLoginArgs = {
  password: Scalars["String"]["input"];
  username: Scalars["String"]["input"];
};

export type MutationLoginWithPasskeyArgs = {
  authenticatorData: Scalars["String"]["input"];
  clientDataJson: Scalars["String"]["input"];
  keyId: Scalars["String"]["input"];
  signature: Scalars["String"]["input"];
};

export type MutationLoginWithTotpArgs = {
  code: Scalars["String"]["input"];
  password: Scalars["String"]["input"];
  username: Scalars["String"]["input"];
};

export type MutationPauseAppArgs = {
  appId: Scalars["String"]["input"];
};

export type MutationRemoveAppDomainArgs = {
  appComponent?: InputMaybe<Scalars["String"]["input"]>;
  domain: Scalars["String"]["input"];
};

export type MutationRemoveAppStoreArgs = {
  src: Scalars["JSONObject"]["input"];
};

export type MutationRemoveDomainArgs = {
  domain: Scalars["String"]["input"];
};

export type MutationRemovePubkeyArgs = {
  keyId: Scalars["String"]["input"];
};

export type MutationRemoveTotpArgs = {
  code: Scalars["String"]["input"];
};

export type MutationResizeVolumeArgs = {
  appId: Scalars["String"]["input"];
  size: Scalars["Int"]["input"];
  volumeId: Scalars["String"]["input"];
};

export type MutationSaveTotpArgs = {
  code: Scalars["String"]["input"];
  secret: Scalars["String"]["input"];
};

export type MutationSetAppProtectedArgs = {
  appId: Scalars["String"]["input"];
  protected: Scalars["Boolean"]["input"];
};

export type MutationSetReplicaCountArgs = {
  appId: Scalars["String"]["input"];
  replicas: Scalars["Int"]["input"];
  volumeId: Scalars["String"]["input"];
};

export type MutationUninstallAppArgs = {
  appId: Scalars["String"]["input"];
};

export type MutationUnpauseAppArgs = {
  appId: Scalars["String"]["input"];
};

export type MutationUpdateAppArgs = {
  appId: Scalars["String"]["input"];
  skipDownload?: InputMaybe<Scalars["Boolean"]["input"]>;
};

export type MutationUpdateAppSettingsArgs = {
  appId: Scalars["String"]["input"];
  settings: Scalars["JSON"]["input"];
};

export type MutationUpdateUserArgs = {
  avatar?: InputMaybe<Scalars["String"]["input"]>;
  email?: InputMaybe<Scalars["String"]["input"]>;
  eraseEmail?: InputMaybe<Scalars["Boolean"]["input"]>;
  group?: InputMaybe<Scalars["String"]["input"]>;
  name?: InputMaybe<Scalars["String"]["input"]>;
  password?: InputMaybe<Scalars["String"]["input"]>;
  permissions?: InputMaybe<Array<UserPermission>>;
  showOnLoginScreen?: InputMaybe<Scalars["Boolean"]["input"]>;
  username?: InputMaybe<Scalars["String"]["input"]>;
};

export type MutationUpgradeKubeArgs = {
  targetVersion: Scalars["String"]["input"];
};

export type Network = {
  __typename?: "Network";
  ip: Scalars["String"]["output"];
  tailscaleInfo: TailscaleInfo;
};

export type NirvatiMeDetails = {
  __typename?: "NirvatiMeDetails";
  createdAt: Scalars["String"]["output"];
  ip: Scalars["String"]["output"];
  lastPing: Scalars["String"]["output"];
  name: Scalars["String"]["output"];
};

export type PassKey = {
  __typename?: "PassKey";
  createdAt: Scalars["DateTime"]["output"];
  displayName: Scalars["String"]["output"];
  keyIdBase64: Scalars["String"]["output"];
  rpId: Scalars["String"]["output"];
};

export type Permission = {
  __typename?: "Permission";
  description: Scalars["MultiLanguageItem"]["output"];
  /**
   * Makes this permission "invisible" (Hidden from the UI) if requested by the apps listed in this field
   * The * wildcard can be used to hide from all apps
   */
  hidden: Array<Scalars["String"]["output"]>;
  id: Scalars["String"]["output"];
  /**
   * Other permissions this permission implies
   * May also contain permissions of other apps
   */
  includes: Array<Scalars["String"]["output"]>;
  name: Scalars["MultiLanguageItem"]["output"];
  /**
   * For system apps, set this to true to allow all users to access this permission
   * If false, only other system apps can access this permission
   */
  openToAllUsers: Scalars["Boolean"]["output"];
  /** Includes access to certain services and ports on these services */
  services: Scalars["JSONObject"]["output"];
  /** The volumes this permission grants access to */
  volumes: Array<VolumePermission>;
};

export enum Protocol {
  Http = "HTTP",
  Https = "HTTPS",
}

export type Proxy = {
  __typename?: "Proxy";
  domain: Scalars["String"]["output"];
  owner: User;
  routes: Array<Route>;
};

export type Query = {
  __typename?: "Query";
  info: Info;
  network: Network;
  upgrades: Upgrades;
  users: Users;
};

export type Route = {
  __typename?: "Route";
  enableCompression: Scalars["Boolean"]["output"];
  id: Scalars["String"]["output"];
  localPath: Scalars["String"]["output"];
  owner: User;
  protocol: Protocol;
  service: Service;
  targetPath: Scalars["String"]["output"];
  targetPort: Scalars["Int"]["output"];
};

export enum SaasCompatibility {
  Compatible = "COMPATIBLE",
  Incompatible = "INCOMPATIBLE",
  Unknown = "UNKNOWN",
}

export type Service = {
  __typename?: "Service";
  id: Scalars["String"]["output"];
  owner: User;
  tcpPorts: Array<Scalars["Int"]["output"]>;
  udpPorts: Array<Scalars["Int"]["output"]>;
  upstream: Scalars["String"]["output"];
  upstreamType: UpstreamType;
};

export type StorePlugin = {
  __typename?: "StorePlugin";
  description: Scalars["String"]["output"];
  icon: Scalars["String"]["output"];
  id: Scalars["String"]["output"];
  inputs: Scalars["JSONObject"]["output"];
  name: Scalars["String"]["output"];
};

export enum StoreType {
  Git = "GIT",
  Plugin = "PLUGIN",
}

export type TailscaleInfo = {
  __typename?: "TailscaleInfo";
  authUrl?: Maybe<Scalars["String"]["output"]>;
  ip?: Maybe<Scalars["String"]["output"]>;
};

export type UiMenuEntry = {
  __typename?: "UiMenuEntry";
  icon: Scalars["String"]["output"];
  name: Scalars["MultiLanguageItem"]["output"];
  path: Scalars["String"]["output"];
};

export type UiModule = {
  __typename?: "UiModule";
  menuEntries: Array<UiMenuEntry>;
};

export type Upgrades = {
  __typename?: "Upgrades";
  kube: KubeInfo;
};

export enum UpstreamType {
  Dns = "DNS",
  Ip = "IP",
}

export type User = {
  __typename?: "User";
  acmeProviders: Array<AcmeProvider>;
  app: AppMetadata;
  appDomains: Array<AppDomain>;
  apps: Array<AppMetadata>;
  avatar?: Maybe<Scalars["String"]["output"]>;
  createdAt: Scalars["DateTime"]["output"];
  domainVerificationCode: Scalars["String"]["output"];
  domains: Array<UserDomain>;
  email?: Maybe<Scalars["String"]["output"]>;
  enabled: Scalars["Boolean"]["output"];
  entropy: Scalars["String"]["output"];
  isPublic: Scalars["Boolean"]["output"];
  jwt: Scalars["String"]["output"];
  lastVerifiedEmail?: Maybe<Scalars["String"]["output"]>;
  name: Scalars["String"]["output"];
  nirvatiMe?: Maybe<NirvatiMeDetails>;
  passkeys: Array<PassKey>;
  permissions: Array<UserPermission>;
  proxies: Array<Proxy>;
  proxy: Proxy;
  service: Service;
  services: Array<Service>;
  setupFinished: Scalars["Boolean"]["output"];
  store: AppStore;
  stores: Array<AppStore>;
  username: Scalars["String"]["output"];
  verifyTxt: Scalars["Boolean"]["output"];
};

export type UserAppArgs = {
  appId: Scalars["String"]["input"];
  initialDomain?: InputMaybe<Scalars["String"]["input"]>;
  settings?: InputMaybe<Scalars["JSON"]["input"]>;
};

export type UserAppsArgs = {
  installedOnly?: InputMaybe<Scalars["Boolean"]["input"]>;
};

export type UserDomainVerificationCodeArgs = {
  domain: Scalars["String"]["input"];
};

export type UserEntropyArgs = {
  identifier: Scalars["String"]["input"];
};

export type UserJwtArgs = {
  audience?: InputMaybe<Array<Scalars["String"]["input"]>>;
};

export type UserProxyArgs = {
  domain: Scalars["String"]["input"];
};

export type UserServiceArgs = {
  id: Scalars["String"]["input"];
};

export type UserStoreArgs = {
  id: Scalars["String"]["input"];
};

export type UserVerifyTxtArgs = {
  domain: Scalars["String"]["input"];
};

export type UserDomain = {
  __typename?: "UserDomain";
  appDomains: Array<AppDomain>;
  createdAt: Scalars["DateTime"]["output"];
  dnsProvider?: Maybe<DnsProvider>;
  dnsProviderAuth?: Maybe<Scalars["String"]["output"]>;
  domain: Scalars["String"]["output"];
  user: User;
};

export enum UserPermission {
  AddTrustedDomains = "ADD_TRUSTED_DOMAINS",
  ConfigureNetwork = "CONFIGURE_NETWORK",
  Escalate = "ESCALATE",
  InstallRootApps = "INSTALL_ROOT_APPS",
  ManageApps = "MANAGE_APPS",
  ManageAppStorage = "MANAGE_APP_STORAGE",
  ManageAppStores = "MANAGE_APP_STORES",
  ManageDomains = "MANAGE_DOMAINS",
  ManageGroups = "MANAGE_GROUPS",
  ManageHardware = "MANAGE_HARDWARE",
  ManageIssuers = "MANAGE_ISSUERS",
  ManageProxies = "MANAGE_PROXIES",
  ManageSysComponents = "MANAGE_SYS_COMPONENTS",
  ManageUsers = "MANAGE_USERS",
  UseNirvatiMe = "USE_NIRVATI_ME",
}

export type Users = {
  __typename?: "Users";
  count: Scalars["Int"]["output"];
  loginUsernames: Array<LoginUser>;
  me: User;
  user: User;
  users: Array<User>;
};

export type UsersUserArgs = {
  username: Scalars["String"]["input"];
};

export type UsersUsersArgs = {
  email?: InputMaybe<Scalars["String"]["input"]>;
  searchNameId?: InputMaybe<Scalars["String"]["input"]>;
  userGroup?: InputMaybe<Scalars["String"]["input"]>;
};

export type Volume = {
  __typename?: "Volume";
  actualSize: Scalars["Int"]["output"];
  humanReadableId: Scalars["String"]["output"];
  id: Scalars["String"]["output"];
  replicaCount: Scalars["Int"]["output"];
  size: Scalars["Int"]["output"];
  volumeDefinition: VolumeDefinition;
};

export type VolumeDefinition = {
  __typename?: "VolumeDefinition";
  description: Scalars["MultiLanguageItem"]["output"];
  minimumSize: Scalars["Int"]["output"];
  name: Scalars["MultiLanguageItem"]["output"];
  recommendedSize: Scalars["Int"]["output"];
};

export type VolumePermission = {
  __typename?: "VolumePermission";
  subPaths?: Maybe<Array<Scalars["String"]["output"]>>;
  volume: Scalars["String"]["output"];
};

export type GetAppOverviewQueryVariables = Exact<{ [key: string]: never }>;

export type GetAppOverviewQuery = {
  __typename?: "Query";
  users: {
    __typename?: "Users";
    me: {
      __typename?: "User";
      username: string;
      apps: Array<{
        __typename?: "AppMetadata";
        id: string;
        scope: AppScope;
        name: Record<string, string>;
        icon?: string | null;
        defaultUsername?: string | null;
        defaultPassword?: string | null;
        path?: string | null;
        supportsIngress: boolean;
        protected: boolean;
        paused: boolean;
        domains: Array<{ __typename?: "AppDomain"; name: string }>;
      }>;
    };
  };
};

export type UninstallAppMutationVariables = Exact<{
  id: Scalars["String"]["input"];
}>;

export type UninstallAppMutation = {
  __typename?: "Mutation";
  uninstallApp: boolean;
};

export type SetAppProtectionStateMutationVariables = Exact<{
  id: Scalars["String"]["input"];
  protect: Scalars["Boolean"]["input"];
}>;

export type SetAppProtectionStateMutation = {
  __typename?: "Mutation";
  setAppProtected: boolean;
};

export type PauseAppMutationVariables = Exact<{
  id: Scalars["String"]["input"];
}>;

export type PauseAppMutation = { __typename?: "Mutation"; pauseApp: boolean };

export type ResumeAppMutationVariables = Exact<{
  id: Scalars["String"]["input"];
}>;

export type ResumeAppMutation = {
  __typename?: "Mutation";
  unpauseApp: boolean;
};

export type UpgradeToCitadelMutationVariables = Exact<{ [key: string]: never }>;

export type UpgradeToCitadelMutation = {
  __typename?: "Mutation";
  installApp: boolean;
};

export type UpdateNirvatiMutationVariables = Exact<{ [key: string]: never }>;

export type UpdateNirvatiMutation = {
  __typename?: "Mutation";
  updateApp: boolean;
};

export type CreateUserMutationVariables = Exact<{
  username: Scalars["String"]["input"];
  name: Scalars["String"]["input"];
  permissions: Array<UserPermission> | UserPermission;
  email?: InputMaybe<Scalars["String"]["input"]>;
  showOnLogin: Scalars["Boolean"]["input"];
  password: Scalars["String"]["input"];
}>;

export type CreateUserMutation = {
  __typename?: "Mutation";
  createUser: boolean;
};

export type GetOtherUserDetailsQueryVariables = Exact<{
  username: Scalars["String"]["input"];
}>;

export type GetOtherUserDetailsQuery = {
  __typename?: "Query";
  users: {
    __typename?: "Users";
    user: {
      __typename?: "User";
      name: string;
      email?: string | null;
      permissions: Array<UserPermission>;
      isPublic: boolean;
    };
    me: { __typename?: "User"; username: string };
  };
};

export type EditUserMutationVariables = Exact<{
  username: Scalars["String"]["input"];
  name: Scalars["String"]["input"];
  permissions: Array<UserPermission> | UserPermission;
  email?: InputMaybe<Scalars["String"]["input"]>;
  showOnLogin: Scalars["Boolean"]["input"];
  password?: InputMaybe<Scalars["String"]["input"]>;
}>;

export type EditUserMutation = { __typename?: "Mutation"; updateUser: boolean };

export type GetLinkedDomainNamesQueryVariables = Exact<{
  [key: string]: never;
}>;

export type GetLinkedDomainNamesQuery = {
  __typename?: "Query";
  users: {
    __typename?: "Users";
    me: {
      __typename?: "User";
      acmeProviders: Array<AcmeProvider>;
      domains: Array<{ __typename?: "UserDomain"; domain: string }>;
    };
  };
};

export type GetAppPermsWithSettingsQueryVariables = Exact<{
  id: Scalars["String"]["input"];
  settings: Scalars["JSON"]["input"];
  initialDomain?: InputMaybe<Scalars["String"]["input"]>;
}>;

export type GetAppPermsWithSettingsQuery = {
  __typename?: "Query";
  users: {
    __typename?: "Users";
    me: {
      __typename?: "User";
      app: {
        __typename?: "AppMetadata";
        permissions: Array<
          | {
              __typename: "AppPermission";
              appName: Record<string, string>;
              permName: Record<string, string>;
              description: Record<string, string>;
            }
          | { __typename: "BuiltinPermission"; permission: string }
        >;
      };
    };
  };
};

export type InstallAppMutationVariables = Exact<{
  id: Scalars["String"]["input"];
  domain?: InputMaybe<Scalars["String"]["input"]>;
  acmeProvider?: InputMaybe<AcmeProvider>;
  settings: Scalars["JSON"]["input"];
}>;

export type InstallAppMutation = {
  __typename?: "Mutation";
  installApp: boolean;
};

export type BasicDetailsQueryVariables = Exact<{ [key: string]: never }>;

export type BasicDetailsQuery = {
  __typename?: "Query";
  users: {
    __typename?: "Users";
    me: { __typename?: "User"; name: string; username: string };
  };
};

export type AddPubkeyMutationVariables = Exact<{
  keyId: Scalars["String"]["input"];
  pubkey: Scalars["String"]["input"];
  displayName: Scalars["String"]["input"];
  rpId: Scalars["String"]["input"];
}>;

export type AddPubkeyMutation = { __typename?: "Mutation"; addPubkey: boolean };

export type UserDetailsQueryVariables = Exact<{ [key: string]: never }>;

export type UserDetailsQuery = {
  __typename?: "Query";
  users: {
    __typename?: "Users";
    me: {
      __typename?: "User";
      name: string;
      permissions: Array<UserPermission>;
      apps: Array<{
        __typename?: "AppMetadata";
        id: string;
        uiModule?: {
          __typename?: "UiModule";
          menuEntries: Array<{
            __typename?: "UiMenuEntry";
            name: Record<string, string>;
            icon: string;
            path: string;
          }>;
        } | null;
      }>;
      app: { __typename?: "AppMetadata"; version: string };
    };
  };
};

export type GetAppByIdQueryVariables = Exact<{
  id: Scalars["String"]["input"];
}>;

export type GetAppByIdQuery = {
  __typename?: "Query";
  users: {
    __typename?: "Users";
    me: {
      __typename?: "User";
      permissions: Array<UserPermission>;
      app: {
        __typename?: "AppMetadata";
        name: Record<string, string>;
        tagline: Record<string, string>;
        icon?: string | null;
        description: Record<string, string>;
        repos: { [key: string]: any };
        gallery: Array<string>;
        defaultUsername?: string | null;
        defaultPassword?: string | null;
        license: string;
        installed: boolean;
        path?: string | null;
        supportsIngress: boolean;
        settings: Record<string, Setting>;
        permissions: Array<
          | {
              __typename: "AppPermission";
              appName: Record<string, string>;
              permName: Record<string, string>;
              description: Record<string, string>;
            }
          | { __typename: "BuiltinPermission"; permission: string }
        >;
        domains: Array<{ __typename?: "AppDomain"; name: string }>;
        dependencies: Array<
          | {
              __typename: "AlternativeDependency";
              options: Array<{
                __typename?: "AppMetadata";
                id: string;
                installed: boolean;
                name: Record<string, string>;
                icon?: string | null;
              }>;
            }
          | {
              __typename: "AppMetadata";
              id: string;
              name: Record<string, string>;
              installed: boolean;
              icon?: string | null;
            }
        >;
      };
    };
  };
};

export type GetAvailableAppMetadataQueryVariables = Exact<{
  [key: string]: never;
}>;

export type GetAvailableAppMetadataQuery = {
  __typename?: "Query";
  users: {
    __typename?: "Users";
    me: {
      __typename?: "User";
      permissions: Array<UserPermission>;
      apps: Array<{
        __typename?: "AppMetadata";
        id: string;
        name: Record<string, string>;
        tagline: Record<string, string>;
        icon?: string | null;
        category: Record<string, string>;
        description: Record<string, string>;
        installed: boolean;
        permissions: Array<
          | { __typename?: "AppPermission" }
          | { __typename?: "BuiltinPermission"; permission: string }
        >;
      }>;
    };
  };
};

export type GetStoreBasicByIdQueryVariables = Exact<{
  id: Scalars["String"]["input"];
}>;

export type GetStoreBasicByIdQuery = {
  __typename?: "Query";
  users: {
    __typename?: "Users";
    me: {
      __typename?: "User";
      store: {
        __typename?: "AppStore";
        name: Record<string, string>;
        icon: string;
      };
    };
  };
};

export type GetStoreByIdQueryVariables = Exact<{
  id: Scalars["String"]["input"];
}>;

export type GetStoreByIdQuery = {
  __typename?: "Query";
  users: {
    __typename?: "Users";
    me: {
      __typename?: "User";
      store: {
        __typename?: "AppStore";
        name: Record<string, string>;
        tagline: Record<string, string>;
        icon: string;
        description: Record<string, string>;
        license: string;
        developers: { [key: string]: any };
        apps: Array<{
          __typename?: "AppMetadata";
          id: string;
          name: Record<string, string>;
          tagline: Record<string, string>;
          icon?: string | null;
          category: Record<string, string>;
          description: Record<string, string>;
          installed: boolean;
          permissions: Array<
            | { __typename?: "AppPermission" }
            | { __typename?: "BuiltinPermission"; permission: string }
          >;
        }>;
      };
    };
  };
};

export type AddStoreMutationVariables = Exact<{
  src: Scalars["JSONObject"]["input"];
}>;

export type AddStoreMutation = {
  __typename?: "Mutation";
  addAppStore: boolean;
};

export type GetStorePluginsQueryVariables = Exact<{ [key: string]: never }>;

export type GetStorePluginsQuery = {
  __typename?: "Query";
  users: {
    __typename?: "Users";
    me: {
      __typename?: "User";
      apps: Array<{
        __typename?: "AppMetadata";
        storePlugins: Array<{
          __typename?: "StorePlugin";
          name: string;
          id: string;
          description: string;
          icon: string;
        }>;
      }>;
    };
  };
};

export type ExistingStoresQueryVariables = Exact<{ [key: string]: never }>;

export type ExistingStoresQuery = {
  __typename?: "Query";
  users: {
    __typename?: "Users";
    me: {
      __typename?: "User";
      stores: Array<{
        __typename?: "AppStore";
        id: string;
        name: Record<string, string>;
        tagline: Record<string, string>;
        icon: string;
      }>;
    };
  };
};

export type UserNameQueryVariables = Exact<{ [key: string]: never }>;

export type UserNameQuery = {
  __typename?: "Query";
  users: { __typename?: "Users"; me: { __typename?: "User"; name: string } };
};

export type GetEnabledAcmeProvidersQueryVariables = Exact<{
  [key: string]: never;
}>;

export type GetEnabledAcmeProvidersQuery = {
  __typename?: "Query";
  users: {
    __typename?: "Users";
    me: {
      __typename?: "User";
      acmeProviders: Array<AcmeProvider>;
      permissions: Array<UserPermission>;
    };
  };
};

export type GetVerificationCodeQueryVariables = Exact<{
  domain: Scalars["String"]["input"];
}>;

export type GetVerificationCodeQuery = {
  __typename?: "Query";
  users: {
    __typename?: "Users";
    me: { __typename?: "User"; domainVerificationCode: string };
  };
};

export type GetLinkedDomainsQueryVariables = Exact<{ [key: string]: never }>;

export type GetLinkedDomainsQuery = {
  __typename?: "Query";
  users: {
    __typename?: "Users";
    me: {
      __typename?: "User";
      domains: Array<{
        __typename?: "UserDomain";
        domain: string;
        dnsProvider?: DnsProvider | null;
      }>;
    };
  };
};

export type GetHttpsAppsQueryVariables = Exact<{ [key: string]: never }>;

export type GetHttpsAppsQuery = {
  __typename?: "Query";
  users: {
    __typename?: "Users";
    me: {
      __typename?: "User";
      apps: Array<{
        __typename?: "AppMetadata";
        id: string;
        name: Record<string, string>;
        icon?: string | null;
        path?: string | null;
        supportsIngress: boolean;
        paused: boolean;
        domains: Array<{ __typename?: "AppDomain"; name: string }>;
      }>;
    };
  };
};

export type AddOnionDomainMutationVariables = Exact<{
  app: Scalars["String"]["input"];
}>;

export type AddOnionDomainMutation = {
  __typename?: "Mutation";
  addOnionDomain: string;
};

export type DeleteDomainMutationVariables = Exact<{
  name: Scalars["String"]["input"];
}>;

export type DeleteDomainMutation = {
  __typename?: "Mutation";
  removeAppDomain: boolean;
};

export type GetProxiesInfoQueryVariables = Exact<{ [key: string]: never }>;

export type GetProxiesInfoQuery = {
  __typename?: "Query";
  users: {
    __typename?: "Users";
    me: {
      __typename?: "User";
      proxies: Array<{
        __typename?: "Proxy";
        domain: string;
        routes: Array<{
          __typename?: "Route";
          id: string;
          localPath: string;
          targetPath: string;
          protocol: Protocol;
          service: { __typename?: "Service"; upstream: string };
        }>;
      }>;
      services: Array<{
        __typename?: "Service";
        id: string;
        upstream: string;
        tcpPorts: Array<number>;
        udpPorts: Array<number>;
      }>;
    };
  };
};

export type CreateServiceMutationVariables = Exact<{
  ip?: InputMaybe<Scalars["String"]["input"]>;
  dnsName?: InputMaybe<Scalars["String"]["input"]>;
  tcpPorts: Array<Scalars["Int"]["input"]> | Scalars["Int"]["input"];
  udpPorts: Array<Scalars["Int"]["input"]> | Scalars["Int"]["input"];
}>;

export type CreateServiceMutation = {
  __typename?: "Mutation";
  createService: string;
};

export type AddServicePortsMutationVariables = Exact<{
  id: Scalars["String"]["input"];
  tcpPorts: Array<Scalars["Int"]["input"]> | Scalars["Int"]["input"];
  udpPorts: Array<Scalars["Int"]["input"]> | Scalars["Int"]["input"];
}>;

export type AddServicePortsMutation = {
  __typename?: "Mutation";
  addServicePorts: boolean;
};

export type CreateProxyMutationVariables = Exact<{
  domain: Scalars["String"]["input"];
}>;

export type CreateProxyMutation = {
  __typename?: "Mutation";
  createProxy: boolean;
};

export type DeleteProxyMutationVariables = Exact<{
  domain: Scalars["String"]["input"];
}>;

export type DeleteProxyMutation = {
  __typename?: "Mutation";
  deleteProxy: boolean;
};

export type CreateRouteMutationVariables = Exact<{
  domain: Scalars["String"]["input"];
  serviceId: Scalars["String"]["input"];
  localPath: Scalars["String"]["input"];
  targetPath: Scalars["String"]["input"];
  targetPort: Scalars["Int"]["input"];
  protocol: Protocol;
  enableCompression: Scalars["Boolean"]["input"];
  allowInvalidCertificates: Scalars["Boolean"]["input"];
  targetSni?: InputMaybe<Scalars["String"]["input"]>;
}>;

export type CreateRouteMutation = {
  __typename?: "Mutation";
  createRoute: string;
};

export type DeleteRouteMutationVariables = Exact<{
  id: Scalars["String"]["input"];
}>;

export type DeleteRouteMutation = {
  __typename?: "Mutation";
  deleteRoute: boolean;
};

export type GetIsSetupQueryVariables = Exact<{ [key: string]: never }>;

export type GetIsSetupQuery = {
  __typename?: "Query";
  info: { __typename?: "Info"; isUserSetup: boolean };
};

export type GetIsSetupFinishedQueryVariables = Exact<{ [key: string]: never }>;

export type GetIsSetupFinishedQuery = {
  __typename?: "Query";
  info: { __typename?: "Info"; isSetupFinished: boolean };
};

export type GetLoginUserInfoQueryVariables = Exact<{ [key: string]: never }>;

export type GetLoginUserInfoQuery = {
  __typename?: "Query";
  users: {
    __typename?: "Users";
    loginUsernames: Array<{
      __typename?: "LoginUser";
      username: string;
      name: string;
    }>;
  };
};

export type PerformLoginMutationVariables = Exact<{
  id: Scalars["String"]["input"];
  password: Scalars["String"]["input"];
}>;

export type PerformLoginMutation = {
  __typename?: "Mutation";
  login?: string | null;
};

export type GetIsSetupUserFinishedQueryVariables = Exact<{
  [key: string]: never;
}>;

export type GetIsSetupUserFinishedQuery = {
  __typename?: "Query";
  users: {
    __typename?: "Users";
    me: { __typename?: "User"; setupFinished: boolean };
  };
};

export type CreatePasskeyChallengeMutationVariables = Exact<{
  username: Scalars["String"]["input"];
}>;

export type CreatePasskeyChallengeMutation = {
  __typename?: "Mutation";
  createChallenge: string;
};

export type PasskeyLoginMutationVariables = Exact<{
  keyId: Scalars["String"]["input"];
  response: Scalars["String"]["input"];
  authenticatorData: Scalars["String"]["input"];
  clientDataJSON: Scalars["String"]["input"];
}>;

export type PasskeyLoginMutation = {
  __typename?: "Mutation";
  loginWithPasskey: string;
};

export type GetAppQueryVariables = Exact<{
  id: Scalars["String"]["input"];
}>;

export type GetAppQuery = {
  __typename?: "Query";
  users: {
    __typename?: "Users";
    me: {
      __typename?: "User";
      app: {
        __typename?: "AppMetadata";
        installed: boolean;
        name: Record<string, string>;
        icon?: string | null;
        domains: Array<{ __typename?: "AppDomain"; name: string }>;
      };
    };
  };
};

export type GetPluginJwtQueryVariables = Exact<{
  audience: Scalars["String"]["input"];
}>;

export type GetPluginJwtQuery = {
  __typename?: "Query";
  users: { __typename?: "Users"; me: { __typename?: "User"; jwt: string } };
};

export type ExistingPasskeysQueryVariables = Exact<{ [key: string]: never }>;

export type ExistingPasskeysQuery = {
  __typename?: "Query";
  users: {
    __typename?: "Users";
    me: {
      __typename?: "User";
      passkeys: Array<{
        __typename?: "PassKey";
        displayName: string;
        rpId: string;
        createdAt: any;
        keyIdBase64: string;
      }>;
    };
  };
};

export type DeletePasskeyMutationVariables = Exact<{
  id: Scalars["String"]["input"];
}>;

export type DeletePasskeyMutation = {
  __typename?: "Mutation";
  removePubkey: boolean;
};

export type UserCustomDomainInfoQueryVariables = Exact<{
  [key: string]: never;
}>;

export type UserCustomDomainInfoQuery = {
  __typename?: "Query";
  users: {
    __typename?: "Users";
    me: {
      __typename?: "User";
      acmeProviders: Array<AcmeProvider>;
      permissions: Array<UserPermission>;
    };
  };
  network: { __typename?: "Network"; ip: string };
};

export type GetDomainVerificationCodeQueryVariables = Exact<{
  domain: Scalars["String"]["input"];
}>;

export type GetDomainVerificationCodeQuery = {
  __typename?: "Query";
  users: {
    __typename?: "Users";
    me: { __typename?: "User"; domainVerificationCode: string };
  };
};

export type EnsureCodeExistsQueryVariables = Exact<{
  domain: Scalars["String"]["input"];
}>;

export type EnsureCodeExistsQuery = {
  __typename?: "Query";
  users: {
    __typename?: "Users";
    me: { __typename?: "User"; verifyTxt: boolean };
  };
};

export type AddVerificationCodeMutationVariables = Exact<{
  domain: Scalars["String"]["input"];
  dnsProvider: DnsProvider;
  auth: Scalars["String"]["input"];
}>;

export type AddVerificationCodeMutation = {
  __typename?: "Mutation";
  addVerificationTxt: boolean;
};

export type GetBasicNetworkInfoQueryVariables = Exact<{ [key: string]: never }>;

export type GetBasicNetworkInfoQuery = {
  __typename?: "Query";
  network: { __typename?: "Network"; ip: string };
};

export type GetUserNirvatiMeInfoQueryVariables = Exact<{
  [key: string]: never;
}>;

export type GetUserNirvatiMeInfoQuery = {
  __typename?: "Query";
  network: { __typename?: "Network"; ip: string };
  users: {
    __typename?: "Users";
    me: {
      __typename?: "User";
      nirvatiMeUsername: string;
      nirvatiMePassword: string;
    };
  };
};

export type GetNirvatiMeAccountQueryVariables = Exact<{ [key: string]: never }>;

export type GetNirvatiMeAccountQuery = {
  __typename?: "Query";
  users: {
    __typename?: "Users";
    me: {
      __typename?: "User";
      acmeProviders: Array<AcmeProvider>;
      nirvatiMe?: { __typename?: "NirvatiMeDetails"; name: string } | null;
    };
  };
};

export type FinishSetupMutationVariables = Exact<{ [key: string]: never }>;

export type FinishSetupMutation = {
  __typename?: "Mutation";
  finishSetup: boolean;
};

export type GetMyInitialDetailsQueryVariables = Exact<{ [key: string]: never }>;

export type GetMyInitialDetailsQuery = {
  __typename?: "Query";
  users: {
    __typename?: "Users";
    me: {
      __typename?: "User";
      username: string;
      name: string;
      email?: string | null;
    };
  };
};

export type UpdateMyAccountMutationVariables = Exact<{
  username: Scalars["String"]["input"];
  name: Scalars["String"]["input"];
  password: Scalars["String"]["input"];
  email?: InputMaybe<Scalars["String"]["input"]>;
  isPublicName?: InputMaybe<Scalars["Boolean"]["input"]>;
}>;

export type UpdateMyAccountMutation = {
  __typename?: "Mutation";
  updateUser: boolean;
};

export type DoSetupMutationVariables = Exact<{
  username: Scalars["String"]["input"];
  name: Scalars["String"]["input"];
  password: Scalars["String"]["input"];
  email?: InputMaybe<Scalars["String"]["input"]>;
  isPublicName: Scalars["Boolean"]["input"];
}>;

export type DoSetupMutation = {
  __typename?: "Mutation";
  createInitialUser: boolean;
};

export type FinalizeSetupMutationVariables = Exact<{ [key: string]: never }>;

export type FinalizeSetupMutation = {
  __typename?: "Mutation";
  installRequiredSystemApps: boolean;
};

export type CustomDomainInfoQueryVariables = Exact<{ [key: string]: never }>;

export type CustomDomainInfoQuery = {
  __typename?: "Query";
  users: {
    __typename?: "Users";
    me: { __typename?: "User"; acmeProviders: Array<AcmeProvider> };
  };
  network: {
    __typename?: "Network";
    ip: string;
    tailscaleInfo: { __typename?: "TailscaleInfo"; ip?: string | null };
  };
};

export type GetNetworkInfoQueryVariables = Exact<{ [key: string]: never }>;

export type GetNetworkInfoQuery = {
  __typename?: "Query";
  network: {
    __typename?: "Network";
    ip: string;
    tailscaleInfo: { __typename?: "TailscaleInfo"; ip?: string | null };
  };
};

export type GetNirvatiMeInfoQueryVariables = Exact<{ [key: string]: never }>;

export type GetNirvatiMeInfoQuery = {
  __typename?: "Query";
  network: {
    __typename?: "Network";
    ip: string;
    tailscaleInfo: { __typename?: "TailscaleInfo"; ip?: string | null };
  };
  users: {
    __typename?: "Users";
    me: {
      __typename?: "User";
      nirvatiMeUsername: string;
      nirvatiMePassword: string;
    };
  };
};

export type InstallTelemetryMutationVariables = Exact<{ [key: string]: never }>;

export type InstallTelemetryMutation = {
  __typename?: "Mutation";
  installApp: boolean;
};

export type GetDomainReadyStateQueryVariables = Exact<{ [key: string]: never }>;

export type GetDomainReadyStateQuery = {
  __typename?: "Query";
  users: {
    __typename?: "Users";
    me: {
      __typename?: "User";
      app: {
        __typename?: "AppMetadata";
        domains: Array<{
          __typename?: "AppDomain";
          name: string;
          ready: boolean;
        }>;
      };
    };
  };
};

export type GetIsCitadelQueryVariables = Exact<{ [key: string]: never }>;

export type GetIsCitadelQuery = {
  __typename?: "Query";
  info: { __typename?: "Info"; isCitadelCompat: boolean };
};

export type TailscaleUpMutationVariables = Exact<{ [key: string]: never }>;

export type TailscaleUpMutation = {
  __typename?: "Mutation";
  startTailscale: boolean;
};

export type GetTailscalePageInfoQueryVariables = Exact<{
  [key: string]: never;
}>;

export type GetTailscalePageInfoQuery = {
  __typename?: "Query";
  network: {
    __typename?: "Network";
    tailscaleInfo: {
      __typename?: "TailscaleInfo";
      authUrl?: string | null;
      ip?: string | null;
    };
  };
};

export type StartTailscaleLoginMutationVariables = Exact<{
  [key: string]: never;
}>;

export type StartTailscaleLoginMutation = {
  __typename?: "Mutation";
  startTailscaleLogin: boolean;
};

export type GetMyDomainsQueryVariables = Exact<{ [key: string]: never }>;

export type GetMyDomainsQuery = {
  __typename?: "Query";
  users: {
    __typename?: "Users";
    me: {
      __typename?: "User";
      app: {
        __typename?: "AppMetadata";
        domains: Array<{ __typename?: "AppDomain"; name: string }>;
      };
    };
  };
};

export type StorageInfoQueryVariables = Exact<{ [key: string]: never }>;

export type StorageInfoQuery = {
  __typename?: "Query";
  users: {
    __typename?: "Users";
    me: {
      __typename?: "User";
      apps: Array<{
        __typename?: "AppMetadata";
        name: Record<string, string>;
        icon?: string | null;
        id: string;
        runningVolumes: Array<{
          __typename?: "Volume";
          id: string;
          size: number;
          actualSize: number;
          replicaCount: number;
          humanReadableId: string;
          volumeDefinition: {
            __typename?: "VolumeDefinition";
            name: Record<string, string>;
            description: Record<string, string>;
          };
        }>;
      }>;
    };
  };
};

export type GetAppVolumeQueryVariables = Exact<{
  appId: Scalars["String"]["input"];
}>;

export type GetAppVolumeQuery = {
  __typename?: "Query";
  users: {
    __typename?: "Users";
    me: {
      __typename?: "User";
      app: {
        __typename?: "AppMetadata";
        name: Record<string, string>;
        icon?: string | null;
        runningVolumes: Array<{
          __typename?: "Volume";
          replicaCount: number;
          actualSize: number;
          size: number;
          humanReadableId: string;
          volumeDefinition: {
            __typename?: "VolumeDefinition";
            name: Record<string, string>;
            description: Record<string, string>;
          };
        }>;
      };
    };
  };
};

export type UpdateReplicaCountMutationVariables = Exact<{
  appId: Scalars["String"]["input"];
  volumeId: Scalars["String"]["input"];
  replicaCount: Scalars["Int"]["input"];
}>;

export type UpdateReplicaCountMutation = {
  __typename?: "Mutation";
  setReplicaCount: boolean;
};

export type UpdateSizeMutationVariables = Exact<{
  appId: Scalars["String"]["input"];
  volumeId: Scalars["String"]["input"];
  size: Scalars["Int"]["input"];
}>;

export type UpdateSizeMutation = {
  __typename?: "Mutation";
  resizeVolume: boolean;
};

export type GetAvailableUpdatesQueryVariables = Exact<{ [key: string]: never }>;

export type GetAvailableUpdatesQuery = {
  __typename?: "Query";
  users: {
    __typename?: "Users";
    me: {
      __typename?: "User";
      permissions: Array<UserPermission>;
      apps: Array<{
        __typename?: "AppMetadata";
        id: string;
        name: Record<string, string>;
        icon?: string | null;
        version: string;
        latestVersion?: {
          __typename?: "AppUpdate";
          latestVersion: string;
        } | null;
      }>;
    };
  };
};

export type GetKubeVersionQueryVariables = Exact<{ [key: string]: never }>;

export type GetKubeVersionQuery = {
  __typename?: "Query";
  upgrades: {
    __typename?: "Upgrades";
    kube: { __typename?: "KubeInfo"; installedVersion: string };
  };
};

export type GetLatestKubeVersionQueryVariables = Exact<{
  [key: string]: never;
}>;

export type GetLatestKubeVersionQuery = {
  __typename?: "Query";
  upgrades: {
    __typename?: "Upgrades";
    kube: { __typename?: "KubeInfo"; latestVersion?: string | null };
  };
};

export type UpdateKubeMutationVariables = Exact<{
  version: Scalars["String"]["input"];
}>;

export type UpdateKubeMutation = {
  __typename?: "Mutation";
  upgradeKube: boolean;
};

export type UpdateAppMutationVariables = Exact<{
  id: Scalars["String"]["input"];
}>;

export type UpdateAppMutation = { __typename?: "Mutation"; updateApp: boolean };

export type GetUsersQueryVariables = Exact<{ [key: string]: never }>;

export type GetUsersQuery = {
  __typename?: "Query";
  users: {
    __typename?: "Users";
    users: Array<{
      __typename?: "User";
      name: string;
      username: string;
      email?: string | null;
      createdAt: any;
    }>;
  };
};

export type GetJwtQueryVariables = Exact<{ [key: string]: never }>;

export type GetJwtQuery = {
  __typename?: "Query";
  users: { __typename?: "Users"; me: { __typename?: "User"; jwt: string } };
};

export type AddDomainMutationVariables = Exact<{
  domain: Scalars["String"]["input"];
  dnsProvider?: InputMaybe<DnsProvider>;
  auth?: InputMaybe<Scalars["String"]["input"]>;
}>;

export type AddDomainMutation = { __typename?: "Mutation"; addDomain: boolean };

export type RemoveDomainMutationVariables = Exact<{
  domain: Scalars["String"]["input"];
}>;

export type RemoveDomainMutation = {
  __typename?: "Mutation";
  removeDomain: boolean;
};

export type AddAcmeProviderMutationVariables = Exact<{
  provider: AcmeProvider;
  shareEmail: Scalars["Boolean"]["input"];
}>;

export type AddAcmeProviderMutation = {
  __typename?: "Mutation";
  createAcmeIssuer: boolean;
};

export type AddAppDomainMutationVariables = Exact<{
  app: Scalars["String"]["input"];
  domain: Scalars["String"]["input"];
  acmeProvider: AcmeProvider;
  forceHttpOnly: Scalars["Boolean"]["input"];
}>;

export type AddAppDomainMutation = {
  __typename?: "Mutation";
  addAppDomain: boolean;
};

export type PerformRegenMutationVariables = Exact<{ [key: string]: never }>;

export type PerformRegenMutation = {
  __typename?: "Mutation";
  generate: boolean;
};

export type PerformRefreshMutationVariables = Exact<{ [key: string]: never }>;

export type PerformRefreshMutation = {
  __typename?: "Mutation";
  refreshSources: boolean;
};

export const GetAppOverviewDocument = {
  kind: "Document",
  definitions: [
    {
      kind: "OperationDefinition",
      operation: "query",
      name: { kind: "Name", value: "getAppOverview" },
      selectionSet: {
        kind: "SelectionSet",
        selections: [
          {
            kind: "Field",
            name: { kind: "Name", value: "users" },
            selectionSet: {
              kind: "SelectionSet",
              selections: [
                {
                  kind: "Field",
                  name: { kind: "Name", value: "me" },
                  selectionSet: {
                    kind: "SelectionSet",
                    selections: [
                      {
                        kind: "Field",
                        name: { kind: "Name", value: "username" },
                      },
                      {
                        kind: "Field",
                        name: { kind: "Name", value: "apps" },
                        arguments: [
                          {
                            kind: "Argument",
                            name: { kind: "Name", value: "installedOnly" },
                            value: { kind: "BooleanValue", value: true },
                          },
                        ],
                        selectionSet: {
                          kind: "SelectionSet",
                          selections: [
                            {
                              kind: "Field",
                              name: { kind: "Name", value: "id" },
                            },
                            {
                              kind: "Field",
                              name: { kind: "Name", value: "scope" },
                            },
                            {
                              kind: "Field",
                              name: { kind: "Name", value: "name" },
                            },
                            {
                              kind: "Field",
                              name: { kind: "Name", value: "icon" },
                            },
                            {
                              kind: "Field",
                              name: { kind: "Name", value: "defaultUsername" },
                            },
                            {
                              kind: "Field",
                              name: { kind: "Name", value: "defaultPassword" },
                            },
                            {
                              kind: "Field",
                              name: { kind: "Name", value: "path" },
                            },
                            {
                              kind: "Field",
                              name: { kind: "Name", value: "supportsIngress" },
                            },
                            {
                              kind: "Field",
                              name: { kind: "Name", value: "protected" },
                            },
                            {
                              kind: "Field",
                              name: { kind: "Name", value: "paused" },
                            },
                            {
                              kind: "Field",
                              name: { kind: "Name", value: "domains" },
                              selectionSet: {
                                kind: "SelectionSet",
                                selections: [
                                  {
                                    kind: "Field",
                                    name: { kind: "Name", value: "name" },
                                  },
                                ],
                              },
                            },
                          ],
                        },
                      },
                    ],
                  },
                },
              ],
            },
          },
        ],
      },
    },
  ],
} as unknown as DocumentNode<GetAppOverviewQuery, GetAppOverviewQueryVariables>;
export const UninstallAppDocument = {
  kind: "Document",
  definitions: [
    {
      kind: "OperationDefinition",
      operation: "mutation",
      name: { kind: "Name", value: "uninstallApp" },
      variableDefinitions: [
        {
          kind: "VariableDefinition",
          variable: { kind: "Variable", name: { kind: "Name", value: "id" } },
          type: {
            kind: "NonNullType",
            type: {
              kind: "NamedType",
              name: { kind: "Name", value: "String" },
            },
          },
        },
      ],
      selectionSet: {
        kind: "SelectionSet",
        selections: [
          {
            kind: "Field",
            name: { kind: "Name", value: "uninstallApp" },
            arguments: [
              {
                kind: "Argument",
                name: { kind: "Name", value: "appId" },
                value: {
                  kind: "Variable",
                  name: { kind: "Name", value: "id" },
                },
              },
            ],
          },
        ],
      },
    },
  ],
} as unknown as DocumentNode<
  UninstallAppMutation,
  UninstallAppMutationVariables
>;
export const SetAppProtectionStateDocument = {
  kind: "Document",
  definitions: [
    {
      kind: "OperationDefinition",
      operation: "mutation",
      name: { kind: "Name", value: "setAppProtectionState" },
      variableDefinitions: [
        {
          kind: "VariableDefinition",
          variable: { kind: "Variable", name: { kind: "Name", value: "id" } },
          type: {
            kind: "NonNullType",
            type: {
              kind: "NamedType",
              name: { kind: "Name", value: "String" },
            },
          },
        },
        {
          kind: "VariableDefinition",
          variable: {
            kind: "Variable",
            name: { kind: "Name", value: "protect" },
          },
          type: {
            kind: "NonNullType",
            type: {
              kind: "NamedType",
              name: { kind: "Name", value: "Boolean" },
            },
          },
        },
      ],
      selectionSet: {
        kind: "SelectionSet",
        selections: [
          {
            kind: "Field",
            name: { kind: "Name", value: "setAppProtected" },
            arguments: [
              {
                kind: "Argument",
                name: { kind: "Name", value: "appId" },
                value: {
                  kind: "Variable",
                  name: { kind: "Name", value: "id" },
                },
              },
              {
                kind: "Argument",
                name: { kind: "Name", value: "protected" },
                value: {
                  kind: "Variable",
                  name: { kind: "Name", value: "protect" },
                },
              },
            ],
          },
        ],
      },
    },
  ],
} as unknown as DocumentNode<
  SetAppProtectionStateMutation,
  SetAppProtectionStateMutationVariables
>;
export const PauseAppDocument = {
  kind: "Document",
  definitions: [
    {
      kind: "OperationDefinition",
      operation: "mutation",
      name: { kind: "Name", value: "pauseApp" },
      variableDefinitions: [
        {
          kind: "VariableDefinition",
          variable: { kind: "Variable", name: { kind: "Name", value: "id" } },
          type: {
            kind: "NonNullType",
            type: {
              kind: "NamedType",
              name: { kind: "Name", value: "String" },
            },
          },
        },
      ],
      selectionSet: {
        kind: "SelectionSet",
        selections: [
          {
            kind: "Field",
            name: { kind: "Name", value: "pauseApp" },
            arguments: [
              {
                kind: "Argument",
                name: { kind: "Name", value: "appId" },
                value: {
                  kind: "Variable",
                  name: { kind: "Name", value: "id" },
                },
              },
            ],
          },
        ],
      },
    },
  ],
} as unknown as DocumentNode<PauseAppMutation, PauseAppMutationVariables>;
export const ResumeAppDocument = {
  kind: "Document",
  definitions: [
    {
      kind: "OperationDefinition",
      operation: "mutation",
      name: { kind: "Name", value: "resumeApp" },
      variableDefinitions: [
        {
          kind: "VariableDefinition",
          variable: { kind: "Variable", name: { kind: "Name", value: "id" } },
          type: {
            kind: "NonNullType",
            type: {
              kind: "NamedType",
              name: { kind: "Name", value: "String" },
            },
          },
        },
      ],
      selectionSet: {
        kind: "SelectionSet",
        selections: [
          {
            kind: "Field",
            name: { kind: "Name", value: "unpauseApp" },
            arguments: [
              {
                kind: "Argument",
                name: { kind: "Name", value: "appId" },
                value: {
                  kind: "Variable",
                  name: { kind: "Name", value: "id" },
                },
              },
            ],
          },
        ],
      },
    },
  ],
} as unknown as DocumentNode<ResumeAppMutation, ResumeAppMutationVariables>;
export const UpgradeToCitadelDocument = {
  kind: "Document",
  definitions: [
    {
      kind: "OperationDefinition",
      operation: "mutation",
      name: { kind: "Name", value: "upgradeToCitadel" },
      selectionSet: {
        kind: "SelectionSet",
        selections: [
          {
            kind: "Field",
            name: { kind: "Name", value: "installApp" },
            arguments: [
              {
                kind: "Argument",
                name: { kind: "Name", value: "appId" },
                value: {
                  kind: "StringValue",
                  value: "citadel-system-wide",
                  block: false,
                },
              },
              {
                kind: "Argument",
                name: { kind: "Name", value: "settings" },
                value: { kind: "ObjectValue", fields: [] },
              },
            ],
          },
        ],
      },
    },
  ],
} as unknown as DocumentNode<
  UpgradeToCitadelMutation,
  UpgradeToCitadelMutationVariables
>;
export const UpdateNirvatiDocument = {
  kind: "Document",
  definitions: [
    {
      kind: "OperationDefinition",
      operation: "mutation",
      name: { kind: "Name", value: "updateNirvati" },
      selectionSet: {
        kind: "SelectionSet",
        selections: [
          {
            kind: "Field",
            name: { kind: "Name", value: "updateApp" },
            arguments: [
              {
                kind: "Argument",
                name: { kind: "Name", value: "appId" },
                value: { kind: "StringValue", value: "nirvati", block: false },
              },
              {
                kind: "Argument",
                name: { kind: "Name", value: "skipDownload" },
                value: { kind: "BooleanValue", value: true },
              },
            ],
          },
        ],
      },
    },
  ],
} as unknown as DocumentNode<
  UpdateNirvatiMutation,
  UpdateNirvatiMutationVariables
>;
export const CreateUserDocument = {
  kind: "Document",
  definitions: [
    {
      kind: "OperationDefinition",
      operation: "mutation",
      name: { kind: "Name", value: "createUser" },
      variableDefinitions: [
        {
          kind: "VariableDefinition",
          variable: {
            kind: "Variable",
            name: { kind: "Name", value: "username" },
          },
          type: {
            kind: "NonNullType",
            type: {
              kind: "NamedType",
              name: { kind: "Name", value: "String" },
            },
          },
        },
        {
          kind: "VariableDefinition",
          variable: { kind: "Variable", name: { kind: "Name", value: "name" } },
          type: {
            kind: "NonNullType",
            type: {
              kind: "NamedType",
              name: { kind: "Name", value: "String" },
            },
          },
        },
        {
          kind: "VariableDefinition",
          variable: {
            kind: "Variable",
            name: { kind: "Name", value: "permissions" },
          },
          type: {
            kind: "NonNullType",
            type: {
              kind: "ListType",
              type: {
                kind: "NonNullType",
                type: {
                  kind: "NamedType",
                  name: { kind: "Name", value: "UserPermission" },
                },
              },
            },
          },
        },
        {
          kind: "VariableDefinition",
          variable: {
            kind: "Variable",
            name: { kind: "Name", value: "email" },
          },
          type: { kind: "NamedType", name: { kind: "Name", value: "String" } },
        },
        {
          kind: "VariableDefinition",
          variable: {
            kind: "Variable",
            name: { kind: "Name", value: "showOnLogin" },
          },
          type: {
            kind: "NonNullType",
            type: {
              kind: "NamedType",
              name: { kind: "Name", value: "Boolean" },
            },
          },
        },
        {
          kind: "VariableDefinition",
          variable: {
            kind: "Variable",
            name: { kind: "Name", value: "password" },
          },
          type: {
            kind: "NonNullType",
            type: {
              kind: "NamedType",
              name: { kind: "Name", value: "String" },
            },
          },
        },
      ],
      selectionSet: {
        kind: "SelectionSet",
        selections: [
          {
            kind: "Field",
            name: { kind: "Name", value: "createUser" },
            arguments: [
              {
                kind: "Argument",
                name: { kind: "Name", value: "username" },
                value: {
                  kind: "Variable",
                  name: { kind: "Name", value: "username" },
                },
              },
              {
                kind: "Argument",
                name: { kind: "Name", value: "name" },
                value: {
                  kind: "Variable",
                  name: { kind: "Name", value: "name" },
                },
              },
              {
                kind: "Argument",
                name: { kind: "Name", value: "email" },
                value: {
                  kind: "Variable",
                  name: { kind: "Name", value: "email" },
                },
              },
              {
                kind: "Argument",
                name: { kind: "Name", value: "permissions" },
                value: {
                  kind: "Variable",
                  name: { kind: "Name", value: "permissions" },
                },
              },
              {
                kind: "Argument",
                name: { kind: "Name", value: "showOnLoginScreen" },
                value: {
                  kind: "Variable",
                  name: { kind: "Name", value: "showOnLogin" },
                },
              },
              {
                kind: "Argument",
                name: { kind: "Name", value: "password" },
                value: {
                  kind: "Variable",
                  name: { kind: "Name", value: "password" },
                },
              },
            ],
          },
        ],
      },
    },
  ],
} as unknown as DocumentNode<CreateUserMutation, CreateUserMutationVariables>;
export const GetOtherUserDetailsDocument = {
  kind: "Document",
  definitions: [
    {
      kind: "OperationDefinition",
      operation: "query",
      name: { kind: "Name", value: "getOtherUserDetails" },
      variableDefinitions: [
        {
          kind: "VariableDefinition",
          variable: {
            kind: "Variable",
            name: { kind: "Name", value: "username" },
          },
          type: {
            kind: "NonNullType",
            type: {
              kind: "NamedType",
              name: { kind: "Name", value: "String" },
            },
          },
        },
      ],
      selectionSet: {
        kind: "SelectionSet",
        selections: [
          {
            kind: "Field",
            name: { kind: "Name", value: "users" },
            selectionSet: {
              kind: "SelectionSet",
              selections: [
                {
                  kind: "Field",
                  name: { kind: "Name", value: "user" },
                  arguments: [
                    {
                      kind: "Argument",
                      name: { kind: "Name", value: "username" },
                      value: {
                        kind: "Variable",
                        name: { kind: "Name", value: "username" },
                      },
                    },
                  ],
                  selectionSet: {
                    kind: "SelectionSet",
                    selections: [
                      { kind: "Field", name: { kind: "Name", value: "name" } },
                      { kind: "Field", name: { kind: "Name", value: "email" } },
                      {
                        kind: "Field",
                        name: { kind: "Name", value: "permissions" },
                      },
                      {
                        kind: "Field",
                        name: { kind: "Name", value: "isPublic" },
                      },
                    ],
                  },
                },
                {
                  kind: "Field",
                  name: { kind: "Name", value: "me" },
                  selectionSet: {
                    kind: "SelectionSet",
                    selections: [
                      {
                        kind: "Field",
                        name: { kind: "Name", value: "username" },
                      },
                    ],
                  },
                },
              ],
            },
          },
        ],
      },
    },
  ],
} as unknown as DocumentNode<
  GetOtherUserDetailsQuery,
  GetOtherUserDetailsQueryVariables
>;
export const EditUserDocument = {
  kind: "Document",
  definitions: [
    {
      kind: "OperationDefinition",
      operation: "mutation",
      name: { kind: "Name", value: "editUser" },
      variableDefinitions: [
        {
          kind: "VariableDefinition",
          variable: {
            kind: "Variable",
            name: { kind: "Name", value: "username" },
          },
          type: {
            kind: "NonNullType",
            type: {
              kind: "NamedType",
              name: { kind: "Name", value: "String" },
            },
          },
        },
        {
          kind: "VariableDefinition",
          variable: { kind: "Variable", name: { kind: "Name", value: "name" } },
          type: {
            kind: "NonNullType",
            type: {
              kind: "NamedType",
              name: { kind: "Name", value: "String" },
            },
          },
        },
        {
          kind: "VariableDefinition",
          variable: {
            kind: "Variable",
            name: { kind: "Name", value: "permissions" },
          },
          type: {
            kind: "NonNullType",
            type: {
              kind: "ListType",
              type: {
                kind: "NonNullType",
                type: {
                  kind: "NamedType",
                  name: { kind: "Name", value: "UserPermission" },
                },
              },
            },
          },
        },
        {
          kind: "VariableDefinition",
          variable: {
            kind: "Variable",
            name: { kind: "Name", value: "email" },
          },
          type: { kind: "NamedType", name: { kind: "Name", value: "String" } },
        },
        {
          kind: "VariableDefinition",
          variable: {
            kind: "Variable",
            name: { kind: "Name", value: "showOnLogin" },
          },
          type: {
            kind: "NonNullType",
            type: {
              kind: "NamedType",
              name: { kind: "Name", value: "Boolean" },
            },
          },
        },
        {
          kind: "VariableDefinition",
          variable: {
            kind: "Variable",
            name: { kind: "Name", value: "password" },
          },
          type: { kind: "NamedType", name: { kind: "Name", value: "String" } },
        },
      ],
      selectionSet: {
        kind: "SelectionSet",
        selections: [
          {
            kind: "Field",
            name: { kind: "Name", value: "updateUser" },
            arguments: [
              {
                kind: "Argument",
                name: { kind: "Name", value: "username" },
                value: {
                  kind: "Variable",
                  name: { kind: "Name", value: "username" },
                },
              },
              {
                kind: "Argument",
                name: { kind: "Name", value: "name" },
                value: {
                  kind: "Variable",
                  name: { kind: "Name", value: "name" },
                },
              },
              {
                kind: "Argument",
                name: { kind: "Name", value: "email" },
                value: {
                  kind: "Variable",
                  name: { kind: "Name", value: "email" },
                },
              },
              {
                kind: "Argument",
                name: { kind: "Name", value: "permissions" },
                value: {
                  kind: "Variable",
                  name: { kind: "Name", value: "permissions" },
                },
              },
              {
                kind: "Argument",
                name: { kind: "Name", value: "showOnLoginScreen" },
                value: {
                  kind: "Variable",
                  name: { kind: "Name", value: "showOnLogin" },
                },
              },
              {
                kind: "Argument",
                name: { kind: "Name", value: "password" },
                value: {
                  kind: "Variable",
                  name: { kind: "Name", value: "password" },
                },
              },
            ],
          },
        ],
      },
    },
  ],
} as unknown as DocumentNode<EditUserMutation, EditUserMutationVariables>;
export const GetLinkedDomainNamesDocument = {
  kind: "Document",
  definitions: [
    {
      kind: "OperationDefinition",
      operation: "query",
      name: { kind: "Name", value: "getLinkedDomainNames" },
      selectionSet: {
        kind: "SelectionSet",
        selections: [
          {
            kind: "Field",
            name: { kind: "Name", value: "users" },
            selectionSet: {
              kind: "SelectionSet",
              selections: [
                {
                  kind: "Field",
                  name: { kind: "Name", value: "me" },
                  selectionSet: {
                    kind: "SelectionSet",
                    selections: [
                      {
                        kind: "Field",
                        name: { kind: "Name", value: "domains" },
                        selectionSet: {
                          kind: "SelectionSet",
                          selections: [
                            {
                              kind: "Field",
                              name: { kind: "Name", value: "domain" },
                            },
                          ],
                        },
                      },
                      {
                        kind: "Field",
                        name: { kind: "Name", value: "acmeProviders" },
                      },
                    ],
                  },
                },
              ],
            },
          },
        ],
      },
    },
  ],
} as unknown as DocumentNode<
  GetLinkedDomainNamesQuery,
  GetLinkedDomainNamesQueryVariables
>;
export const GetAppPermsWithSettingsDocument = {
  kind: "Document",
  definitions: [
    {
      kind: "OperationDefinition",
      operation: "query",
      name: { kind: "Name", value: "getAppPermsWithSettings" },
      variableDefinitions: [
        {
          kind: "VariableDefinition",
          variable: { kind: "Variable", name: { kind: "Name", value: "id" } },
          type: {
            kind: "NonNullType",
            type: {
              kind: "NamedType",
              name: { kind: "Name", value: "String" },
            },
          },
        },
        {
          kind: "VariableDefinition",
          variable: {
            kind: "Variable",
            name: { kind: "Name", value: "settings" },
          },
          type: {
            kind: "NonNullType",
            type: { kind: "NamedType", name: { kind: "Name", value: "JSON" } },
          },
        },
        {
          kind: "VariableDefinition",
          variable: {
            kind: "Variable",
            name: { kind: "Name", value: "initialDomain" },
          },
          type: { kind: "NamedType", name: { kind: "Name", value: "String" } },
        },
      ],
      selectionSet: {
        kind: "SelectionSet",
        selections: [
          {
            kind: "Field",
            name: { kind: "Name", value: "users" },
            selectionSet: {
              kind: "SelectionSet",
              selections: [
                {
                  kind: "Field",
                  name: { kind: "Name", value: "me" },
                  selectionSet: {
                    kind: "SelectionSet",
                    selections: [
                      {
                        kind: "Field",
                        name: { kind: "Name", value: "app" },
                        arguments: [
                          {
                            kind: "Argument",
                            name: { kind: "Name", value: "appId" },
                            value: {
                              kind: "Variable",
                              name: { kind: "Name", value: "id" },
                            },
                          },
                          {
                            kind: "Argument",
                            name: { kind: "Name", value: "settings" },
                            value: {
                              kind: "Variable",
                              name: { kind: "Name", value: "settings" },
                            },
                          },
                          {
                            kind: "Argument",
                            name: { kind: "Name", value: "initialDomain" },
                            value: {
                              kind: "Variable",
                              name: { kind: "Name", value: "initialDomain" },
                            },
                          },
                        ],
                        selectionSet: {
                          kind: "SelectionSet",
                          selections: [
                            {
                              kind: "Field",
                              name: { kind: "Name", value: "permissions" },
                              selectionSet: {
                                kind: "SelectionSet",
                                selections: [
                                  {
                                    kind: "Field",
                                    name: { kind: "Name", value: "__typename" },
                                  },
                                  {
                                    kind: "InlineFragment",
                                    typeCondition: {
                                      kind: "NamedType",
                                      name: {
                                        kind: "Name",
                                        value: "BuiltinPermission",
                                      },
                                    },
                                    selectionSet: {
                                      kind: "SelectionSet",
                                      selections: [
                                        {
                                          kind: "Field",
                                          name: {
                                            kind: "Name",
                                            value: "permission",
                                          },
                                        },
                                      ],
                                    },
                                  },
                                  {
                                    kind: "InlineFragment",
                                    typeCondition: {
                                      kind: "NamedType",
                                      name: {
                                        kind: "Name",
                                        value: "AppPermission",
                                      },
                                    },
                                    selectionSet: {
                                      kind: "SelectionSet",
                                      selections: [
                                        {
                                          kind: "Field",
                                          name: {
                                            kind: "Name",
                                            value: "appName",
                                          },
                                        },
                                        {
                                          kind: "Field",
                                          name: {
                                            kind: "Name",
                                            value: "permName",
                                          },
                                        },
                                        {
                                          kind: "Field",
                                          name: {
                                            kind: "Name",
                                            value: "description",
                                          },
                                        },
                                      ],
                                    },
                                  },
                                ],
                              },
                            },
                          ],
                        },
                      },
                    ],
                  },
                },
              ],
            },
          },
        ],
      },
    },
  ],
} as unknown as DocumentNode<
  GetAppPermsWithSettingsQuery,
  GetAppPermsWithSettingsQueryVariables
>;
export const InstallAppDocument = {
  kind: "Document",
  definitions: [
    {
      kind: "OperationDefinition",
      operation: "mutation",
      name: { kind: "Name", value: "installApp" },
      variableDefinitions: [
        {
          kind: "VariableDefinition",
          variable: { kind: "Variable", name: { kind: "Name", value: "id" } },
          type: {
            kind: "NonNullType",
            type: {
              kind: "NamedType",
              name: { kind: "Name", value: "String" },
            },
          },
        },
        {
          kind: "VariableDefinition",
          variable: {
            kind: "Variable",
            name: { kind: "Name", value: "domain" },
          },
          type: { kind: "NamedType", name: { kind: "Name", value: "String" } },
        },
        {
          kind: "VariableDefinition",
          variable: {
            kind: "Variable",
            name: { kind: "Name", value: "acmeProvider" },
          },
          type: {
            kind: "NamedType",
            name: { kind: "Name", value: "AcmeProvider" },
          },
        },
        {
          kind: "VariableDefinition",
          variable: {
            kind: "Variable",
            name: { kind: "Name", value: "settings" },
          },
          type: {
            kind: "NonNullType",
            type: { kind: "NamedType", name: { kind: "Name", value: "JSON" } },
          },
        },
      ],
      selectionSet: {
        kind: "SelectionSet",
        selections: [
          {
            kind: "Field",
            name: { kind: "Name", value: "installApp" },
            arguments: [
              {
                kind: "Argument",
                name: { kind: "Name", value: "appId" },
                value: {
                  kind: "Variable",
                  name: { kind: "Name", value: "id" },
                },
              },
              {
                kind: "Argument",
                name: { kind: "Name", value: "settings" },
                value: {
                  kind: "Variable",
                  name: { kind: "Name", value: "settings" },
                },
              },
              {
                kind: "Argument",
                name: { kind: "Name", value: "initialDomain" },
                value: {
                  kind: "Variable",
                  name: { kind: "Name", value: "domain" },
                },
              },
              {
                kind: "Argument",
                name: { kind: "Name", value: "acmeProvider" },
                value: {
                  kind: "Variable",
                  name: { kind: "Name", value: "acmeProvider" },
                },
              },
            ],
          },
        ],
      },
    },
  ],
} as unknown as DocumentNode<InstallAppMutation, InstallAppMutationVariables>;
export const BasicDetailsDocument = {
  kind: "Document",
  definitions: [
    {
      kind: "OperationDefinition",
      operation: "query",
      name: { kind: "Name", value: "BasicDetails" },
      selectionSet: {
        kind: "SelectionSet",
        selections: [
          {
            kind: "Field",
            name: { kind: "Name", value: "users" },
            selectionSet: {
              kind: "SelectionSet",
              selections: [
                {
                  kind: "Field",
                  name: { kind: "Name", value: "me" },
                  selectionSet: {
                    kind: "SelectionSet",
                    selections: [
                      { kind: "Field", name: { kind: "Name", value: "name" } },
                      {
                        kind: "Field",
                        name: { kind: "Name", value: "username" },
                      },
                    ],
                  },
                },
              ],
            },
          },
        ],
      },
    },
  ],
} as unknown as DocumentNode<BasicDetailsQuery, BasicDetailsQueryVariables>;
export const AddPubkeyDocument = {
  kind: "Document",
  definitions: [
    {
      kind: "OperationDefinition",
      operation: "mutation",
      name: { kind: "Name", value: "AddPubkey" },
      variableDefinitions: [
        {
          kind: "VariableDefinition",
          variable: {
            kind: "Variable",
            name: { kind: "Name", value: "keyId" },
          },
          type: {
            kind: "NonNullType",
            type: {
              kind: "NamedType",
              name: { kind: "Name", value: "String" },
            },
          },
        },
        {
          kind: "VariableDefinition",
          variable: {
            kind: "Variable",
            name: { kind: "Name", value: "pubkey" },
          },
          type: {
            kind: "NonNullType",
            type: {
              kind: "NamedType",
              name: { kind: "Name", value: "String" },
            },
          },
        },
        {
          kind: "VariableDefinition",
          variable: {
            kind: "Variable",
            name: { kind: "Name", value: "displayName" },
          },
          type: {
            kind: "NonNullType",
            type: {
              kind: "NamedType",
              name: { kind: "Name", value: "String" },
            },
          },
        },
        {
          kind: "VariableDefinition",
          variable: { kind: "Variable", name: { kind: "Name", value: "rpId" } },
          type: {
            kind: "NonNullType",
            type: {
              kind: "NamedType",
              name: { kind: "Name", value: "String" },
            },
          },
        },
      ],
      selectionSet: {
        kind: "SelectionSet",
        selections: [
          {
            kind: "Field",
            name: { kind: "Name", value: "addPubkey" },
            arguments: [
              {
                kind: "Argument",
                name: { kind: "Name", value: "keyId" },
                value: {
                  kind: "Variable",
                  name: { kind: "Name", value: "keyId" },
                },
              },
              {
                kind: "Argument",
                name: { kind: "Name", value: "pubkeyDer" },
                value: {
                  kind: "Variable",
                  name: { kind: "Name", value: "pubkey" },
                },
              },
              {
                kind: "Argument",
                name: { kind: "Name", value: "displayName" },
                value: {
                  kind: "Variable",
                  name: { kind: "Name", value: "displayName" },
                },
              },
              {
                kind: "Argument",
                name: { kind: "Name", value: "rpId" },
                value: {
                  kind: "Variable",
                  name: { kind: "Name", value: "rpId" },
                },
              },
            ],
          },
        ],
      },
    },
  ],
} as unknown as DocumentNode<AddPubkeyMutation, AddPubkeyMutationVariables>;
export const UserDetailsDocument = {
  kind: "Document",
  definitions: [
    {
      kind: "OperationDefinition",
      operation: "query",
      name: { kind: "Name", value: "userDetails" },
      selectionSet: {
        kind: "SelectionSet",
        selections: [
          {
            kind: "Field",
            name: { kind: "Name", value: "users" },
            selectionSet: {
              kind: "SelectionSet",
              selections: [
                {
                  kind: "Field",
                  name: { kind: "Name", value: "me" },
                  selectionSet: {
                    kind: "SelectionSet",
                    selections: [
                      { kind: "Field", name: { kind: "Name", value: "name" } },
                      {
                        kind: "Field",
                        name: { kind: "Name", value: "permissions" },
                      },
                      {
                        kind: "Field",
                        name: { kind: "Name", value: "apps" },
                        arguments: [
                          {
                            kind: "Argument",
                            name: { kind: "Name", value: "installedOnly" },
                            value: { kind: "BooleanValue", value: true },
                          },
                        ],
                        selectionSet: {
                          kind: "SelectionSet",
                          selections: [
                            {
                              kind: "Field",
                              name: { kind: "Name", value: "id" },
                            },
                            {
                              kind: "Field",
                              name: { kind: "Name", value: "uiModule" },
                              selectionSet: {
                                kind: "SelectionSet",
                                selections: [
                                  {
                                    kind: "Field",
                                    name: {
                                      kind: "Name",
                                      value: "menuEntries",
                                    },
                                    selectionSet: {
                                      kind: "SelectionSet",
                                      selections: [
                                        {
                                          kind: "Field",
                                          name: { kind: "Name", value: "name" },
                                        },
                                        {
                                          kind: "Field",
                                          name: { kind: "Name", value: "icon" },
                                        },
                                        {
                                          kind: "Field",
                                          name: { kind: "Name", value: "path" },
                                        },
                                      ],
                                    },
                                  },
                                ],
                              },
                            },
                          ],
                        },
                      },
                      {
                        kind: "Field",
                        name: { kind: "Name", value: "app" },
                        arguments: [
                          {
                            kind: "Argument",
                            name: { kind: "Name", value: "appId" },
                            value: {
                              kind: "StringValue",
                              value: "nirvati",
                              block: false,
                            },
                          },
                        ],
                        selectionSet: {
                          kind: "SelectionSet",
                          selections: [
                            {
                              kind: "Field",
                              name: { kind: "Name", value: "version" },
                            },
                          ],
                        },
                      },
                    ],
                  },
                },
              ],
            },
          },
        ],
      },
    },
  ],
} as unknown as DocumentNode<UserDetailsQuery, UserDetailsQueryVariables>;
export const GetAppByIdDocument = {
  kind: "Document",
  definitions: [
    {
      kind: "OperationDefinition",
      operation: "query",
      name: { kind: "Name", value: "getAppById" },
      variableDefinitions: [
        {
          kind: "VariableDefinition",
          variable: { kind: "Variable", name: { kind: "Name", value: "id" } },
          type: {
            kind: "NonNullType",
            type: {
              kind: "NamedType",
              name: { kind: "Name", value: "String" },
            },
          },
        },
      ],
      selectionSet: {
        kind: "SelectionSet",
        selections: [
          {
            kind: "Field",
            name: { kind: "Name", value: "users" },
            selectionSet: {
              kind: "SelectionSet",
              selections: [
                {
                  kind: "Field",
                  name: { kind: "Name", value: "me" },
                  selectionSet: {
                    kind: "SelectionSet",
                    selections: [
                      {
                        kind: "Field",
                        name: { kind: "Name", value: "permissions" },
                      },
                      {
                        kind: "Field",
                        name: { kind: "Name", value: "app" },
                        arguments: [
                          {
                            kind: "Argument",
                            name: { kind: "Name", value: "appId" },
                            value: {
                              kind: "Variable",
                              name: { kind: "Name", value: "id" },
                            },
                          },
                        ],
                        selectionSet: {
                          kind: "SelectionSet",
                          selections: [
                            {
                              kind: "Field",
                              name: { kind: "Name", value: "name" },
                            },
                            {
                              kind: "Field",
                              name: { kind: "Name", value: "tagline" },
                            },
                            {
                              kind: "Field",
                              name: { kind: "Name", value: "icon" },
                            },
                            {
                              kind: "Field",
                              name: { kind: "Name", value: "description" },
                            },
                            {
                              kind: "Field",
                              name: { kind: "Name", value: "repos" },
                            },
                            {
                              kind: "Field",
                              name: { kind: "Name", value: "gallery" },
                            },
                            {
                              kind: "Field",
                              name: { kind: "Name", value: "defaultUsername" },
                            },
                            {
                              kind: "Field",
                              name: { kind: "Name", value: "defaultPassword" },
                            },
                            {
                              kind: "Field",
                              name: { kind: "Name", value: "license" },
                            },
                            {
                              kind: "Field",
                              name: { kind: "Name", value: "installed" },
                            },
                            {
                              kind: "Field",
                              name: { kind: "Name", value: "path" },
                            },
                            {
                              kind: "Field",
                              name: { kind: "Name", value: "supportsIngress" },
                            },
                            {
                              kind: "Field",
                              name: { kind: "Name", value: "permissions" },
                              selectionSet: {
                                kind: "SelectionSet",
                                selections: [
                                  {
                                    kind: "Field",
                                    name: { kind: "Name", value: "__typename" },
                                  },
                                  {
                                    kind: "InlineFragment",
                                    typeCondition: {
                                      kind: "NamedType",
                                      name: {
                                        kind: "Name",
                                        value: "AppPermission",
                                      },
                                    },
                                    selectionSet: {
                                      kind: "SelectionSet",
                                      selections: [
                                        {
                                          kind: "Field",
                                          name: {
                                            kind: "Name",
                                            value: "appName",
                                          },
                                        },
                                        {
                                          kind: "Field",
                                          name: {
                                            kind: "Name",
                                            value: "permName",
                                          },
                                        },
                                        {
                                          kind: "Field",
                                          name: {
                                            kind: "Name",
                                            value: "description",
                                          },
                                        },
                                      ],
                                    },
                                  },
                                  {
                                    kind: "InlineFragment",
                                    typeCondition: {
                                      kind: "NamedType",
                                      name: {
                                        kind: "Name",
                                        value: "BuiltinPermission",
                                      },
                                    },
                                    selectionSet: {
                                      kind: "SelectionSet",
                                      selections: [
                                        {
                                          kind: "Field",
                                          name: {
                                            kind: "Name",
                                            value: "permission",
                                          },
                                        },
                                      ],
                                    },
                                  },
                                ],
                              },
                            },
                            {
                              kind: "Field",
                              name: { kind: "Name", value: "domains" },
                              selectionSet: {
                                kind: "SelectionSet",
                                selections: [
                                  {
                                    kind: "Field",
                                    name: { kind: "Name", value: "name" },
                                  },
                                ],
                              },
                            },
                            {
                              kind: "Field",
                              name: { kind: "Name", value: "settings" },
                            },
                            {
                              kind: "Field",
                              name: { kind: "Name", value: "dependencies" },
                              selectionSet: {
                                kind: "SelectionSet",
                                selections: [
                                  {
                                    kind: "Field",
                                    name: { kind: "Name", value: "__typename" },
                                  },
                                  {
                                    kind: "InlineFragment",
                                    typeCondition: {
                                      kind: "NamedType",
                                      name: {
                                        kind: "Name",
                                        value: "AppMetadata",
                                      },
                                    },
                                    selectionSet: {
                                      kind: "SelectionSet",
                                      selections: [
                                        {
                                          kind: "Field",
                                          name: { kind: "Name", value: "id" },
                                        },
                                        {
                                          kind: "Field",
                                          name: { kind: "Name", value: "name" },
                                        },
                                        {
                                          kind: "Field",
                                          name: {
                                            kind: "Name",
                                            value: "installed",
                                          },
                                        },
                                        {
                                          kind: "Field",
                                          name: { kind: "Name", value: "icon" },
                                        },
                                      ],
                                    },
                                  },
                                  {
                                    kind: "InlineFragment",
                                    typeCondition: {
                                      kind: "NamedType",
                                      name: {
                                        kind: "Name",
                                        value: "AlternativeDependency",
                                      },
                                    },
                                    selectionSet: {
                                      kind: "SelectionSet",
                                      selections: [
                                        {
                                          kind: "Field",
                                          name: {
                                            kind: "Name",
                                            value: "options",
                                          },
                                          selectionSet: {
                                            kind: "SelectionSet",
                                            selections: [
                                              {
                                                kind: "Field",
                                                name: {
                                                  kind: "Name",
                                                  value: "id",
                                                },
                                              },
                                              {
                                                kind: "Field",
                                                name: {
                                                  kind: "Name",
                                                  value: "installed",
                                                },
                                              },
                                              {
                                                kind: "Field",
                                                name: {
                                                  kind: "Name",
                                                  value: "name",
                                                },
                                              },
                                              {
                                                kind: "Field",
                                                name: {
                                                  kind: "Name",
                                                  value: "icon",
                                                },
                                              },
                                            ],
                                          },
                                        },
                                      ],
                                    },
                                  },
                                ],
                              },
                            },
                          ],
                        },
                      },
                    ],
                  },
                },
              ],
            },
          },
        ],
      },
    },
  ],
} as unknown as DocumentNode<GetAppByIdQuery, GetAppByIdQueryVariables>;
export const GetAvailableAppMetadataDocument = {
  kind: "Document",
  definitions: [
    {
      kind: "OperationDefinition",
      operation: "query",
      name: { kind: "Name", value: "getAvailableAppMetadata" },
      selectionSet: {
        kind: "SelectionSet",
        selections: [
          {
            kind: "Field",
            name: { kind: "Name", value: "users" },
            selectionSet: {
              kind: "SelectionSet",
              selections: [
                {
                  kind: "Field",
                  name: { kind: "Name", value: "me" },
                  selectionSet: {
                    kind: "SelectionSet",
                    selections: [
                      {
                        kind: "Field",
                        name: { kind: "Name", value: "permissions" },
                      },
                      {
                        kind: "Field",
                        name: { kind: "Name", value: "apps" },
                        selectionSet: {
                          kind: "SelectionSet",
                          selections: [
                            {
                              kind: "Field",
                              name: { kind: "Name", value: "id" },
                            },
                            {
                              kind: "Field",
                              name: { kind: "Name", value: "name" },
                            },
                            {
                              kind: "Field",
                              name: { kind: "Name", value: "tagline" },
                            },
                            {
                              kind: "Field",
                              name: { kind: "Name", value: "icon" },
                            },
                            {
                              kind: "Field",
                              name: { kind: "Name", value: "category" },
                            },
                            {
                              kind: "Field",
                              name: { kind: "Name", value: "description" },
                            },
                            {
                              kind: "Field",
                              name: { kind: "Name", value: "installed" },
                            },
                            {
                              kind: "Field",
                              name: { kind: "Name", value: "permissions" },
                              selectionSet: {
                                kind: "SelectionSet",
                                selections: [
                                  {
                                    kind: "InlineFragment",
                                    typeCondition: {
                                      kind: "NamedType",
                                      name: {
                                        kind: "Name",
                                        value: "BuiltinPermission",
                                      },
                                    },
                                    selectionSet: {
                                      kind: "SelectionSet",
                                      selections: [
                                        {
                                          kind: "Field",
                                          name: {
                                            kind: "Name",
                                            value: "permission",
                                          },
                                        },
                                      ],
                                    },
                                  },
                                ],
                              },
                            },
                          ],
                        },
                      },
                    ],
                  },
                },
              ],
            },
          },
        ],
      },
    },
  ],
} as unknown as DocumentNode<
  GetAvailableAppMetadataQuery,
  GetAvailableAppMetadataQueryVariables
>;
export const GetStoreBasicByIdDocument = {
  kind: "Document",
  definitions: [
    {
      kind: "OperationDefinition",
      operation: "query",
      name: { kind: "Name", value: "getStoreBasicById" },
      variableDefinitions: [
        {
          kind: "VariableDefinition",
          variable: { kind: "Variable", name: { kind: "Name", value: "id" } },
          type: {
            kind: "NonNullType",
            type: {
              kind: "NamedType",
              name: { kind: "Name", value: "String" },
            },
          },
        },
      ],
      selectionSet: {
        kind: "SelectionSet",
        selections: [
          {
            kind: "Field",
            name: { kind: "Name", value: "users" },
            selectionSet: {
              kind: "SelectionSet",
              selections: [
                {
                  kind: "Field",
                  name: { kind: "Name", value: "me" },
                  selectionSet: {
                    kind: "SelectionSet",
                    selections: [
                      {
                        kind: "Field",
                        name: { kind: "Name", value: "store" },
                        arguments: [
                          {
                            kind: "Argument",
                            name: { kind: "Name", value: "id" },
                            value: {
                              kind: "Variable",
                              name: { kind: "Name", value: "id" },
                            },
                          },
                        ],
                        selectionSet: {
                          kind: "SelectionSet",
                          selections: [
                            {
                              kind: "Field",
                              name: { kind: "Name", value: "name" },
                            },
                            {
                              kind: "Field",
                              name: { kind: "Name", value: "icon" },
                            },
                          ],
                        },
                      },
                    ],
                  },
                },
              ],
            },
          },
        ],
      },
    },
  ],
} as unknown as DocumentNode<
  GetStoreBasicByIdQuery,
  GetStoreBasicByIdQueryVariables
>;
export const GetStoreByIdDocument = {
  kind: "Document",
  definitions: [
    {
      kind: "OperationDefinition",
      operation: "query",
      name: { kind: "Name", value: "getStoreById" },
      variableDefinitions: [
        {
          kind: "VariableDefinition",
          variable: { kind: "Variable", name: { kind: "Name", value: "id" } },
          type: {
            kind: "NonNullType",
            type: {
              kind: "NamedType",
              name: { kind: "Name", value: "String" },
            },
          },
        },
      ],
      selectionSet: {
        kind: "SelectionSet",
        selections: [
          {
            kind: "Field",
            name: { kind: "Name", value: "users" },
            selectionSet: {
              kind: "SelectionSet",
              selections: [
                {
                  kind: "Field",
                  name: { kind: "Name", value: "me" },
                  selectionSet: {
                    kind: "SelectionSet",
                    selections: [
                      {
                        kind: "Field",
                        name: { kind: "Name", value: "store" },
                        arguments: [
                          {
                            kind: "Argument",
                            name: { kind: "Name", value: "id" },
                            value: {
                              kind: "Variable",
                              name: { kind: "Name", value: "id" },
                            },
                          },
                        ],
                        selectionSet: {
                          kind: "SelectionSet",
                          selections: [
                            {
                              kind: "Field",
                              name: { kind: "Name", value: "name" },
                            },
                            {
                              kind: "Field",
                              name: { kind: "Name", value: "tagline" },
                            },
                            {
                              kind: "Field",
                              name: { kind: "Name", value: "icon" },
                            },
                            {
                              kind: "Field",
                              name: { kind: "Name", value: "description" },
                            },
                            {
                              kind: "Field",
                              name: { kind: "Name", value: "license" },
                            },
                            {
                              kind: "Field",
                              name: { kind: "Name", value: "developers" },
                            },
                            {
                              kind: "Field",
                              name: { kind: "Name", value: "apps" },
                              selectionSet: {
                                kind: "SelectionSet",
                                selections: [
                                  {
                                    kind: "Field",
                                    name: { kind: "Name", value: "id" },
                                  },
                                  {
                                    kind: "Field",
                                    name: { kind: "Name", value: "name" },
                                  },
                                  {
                                    kind: "Field",
                                    name: { kind: "Name", value: "tagline" },
                                  },
                                  {
                                    kind: "Field",
                                    name: { kind: "Name", value: "icon" },
                                  },
                                  {
                                    kind: "Field",
                                    name: { kind: "Name", value: "category" },
                                  },
                                  {
                                    kind: "Field",
                                    name: {
                                      kind: "Name",
                                      value: "description",
                                    },
                                  },
                                  {
                                    kind: "Field",
                                    name: { kind: "Name", value: "installed" },
                                  },
                                  {
                                    kind: "Field",
                                    name: {
                                      kind: "Name",
                                      value: "permissions",
                                    },
                                    selectionSet: {
                                      kind: "SelectionSet",
                                      selections: [
                                        {
                                          kind: "InlineFragment",
                                          typeCondition: {
                                            kind: "NamedType",
                                            name: {
                                              kind: "Name",
                                              value: "BuiltinPermission",
                                            },
                                          },
                                          selectionSet: {
                                            kind: "SelectionSet",
                                            selections: [
                                              {
                                                kind: "Field",
                                                name: {
                                                  kind: "Name",
                                                  value: "permission",
                                                },
                                              },
                                            ],
                                          },
                                        },
                                      ],
                                    },
                                  },
                                ],
                              },
                            },
                          ],
                        },
                      },
                    ],
                  },
                },
              ],
            },
          },
        ],
      },
    },
  ],
} as unknown as DocumentNode<GetStoreByIdQuery, GetStoreByIdQueryVariables>;
export const AddStoreDocument = {
  kind: "Document",
  definitions: [
    {
      kind: "OperationDefinition",
      operation: "mutation",
      name: { kind: "Name", value: "addStore" },
      variableDefinitions: [
        {
          kind: "VariableDefinition",
          variable: { kind: "Variable", name: { kind: "Name", value: "src" } },
          type: {
            kind: "NonNullType",
            type: {
              kind: "NamedType",
              name: { kind: "Name", value: "JSONObject" },
            },
          },
        },
      ],
      selectionSet: {
        kind: "SelectionSet",
        selections: [
          {
            kind: "Field",
            name: { kind: "Name", value: "addAppStore" },
            arguments: [
              {
                kind: "Argument",
                name: { kind: "Name", value: "src" },
                value: {
                  kind: "Variable",
                  name: { kind: "Name", value: "src" },
                },
              },
              {
                kind: "Argument",
                name: { kind: "Name", value: "storeType" },
                value: { kind: "EnumValue", value: "GIT" },
              },
            ],
          },
        ],
      },
    },
  ],
} as unknown as DocumentNode<AddStoreMutation, AddStoreMutationVariables>;
export const GetStorePluginsDocument = {
  kind: "Document",
  definitions: [
    {
      kind: "OperationDefinition",
      operation: "query",
      name: { kind: "Name", value: "getStorePlugins" },
      selectionSet: {
        kind: "SelectionSet",
        selections: [
          {
            kind: "Field",
            name: { kind: "Name", value: "users" },
            selectionSet: {
              kind: "SelectionSet",
              selections: [
                {
                  kind: "Field",
                  name: { kind: "Name", value: "me" },
                  selectionSet: {
                    kind: "SelectionSet",
                    selections: [
                      {
                        kind: "Field",
                        name: { kind: "Name", value: "apps" },
                        arguments: [
                          {
                            kind: "Argument",
                            name: { kind: "Name", value: "installedOnly" },
                            value: { kind: "BooleanValue", value: true },
                          },
                        ],
                        selectionSet: {
                          kind: "SelectionSet",
                          selections: [
                            {
                              kind: "Field",
                              name: { kind: "Name", value: "storePlugins" },
                              selectionSet: {
                                kind: "SelectionSet",
                                selections: [
                                  {
                                    kind: "Field",
                                    name: { kind: "Name", value: "name" },
                                  },
                                  {
                                    kind: "Field",
                                    name: { kind: "Name", value: "id" },
                                  },
                                  {
                                    kind: "Field",
                                    name: {
                                      kind: "Name",
                                      value: "description",
                                    },
                                  },
                                  {
                                    kind: "Field",
                                    name: { kind: "Name", value: "icon" },
                                  },
                                ],
                              },
                            },
                          ],
                        },
                      },
                    ],
                  },
                },
              ],
            },
          },
        ],
      },
    },
  ],
} as unknown as DocumentNode<
  GetStorePluginsQuery,
  GetStorePluginsQueryVariables
>;
export const ExistingStoresDocument = {
  kind: "Document",
  definitions: [
    {
      kind: "OperationDefinition",
      operation: "query",
      name: { kind: "Name", value: "existingStores" },
      selectionSet: {
        kind: "SelectionSet",
        selections: [
          {
            kind: "Field",
            name: { kind: "Name", value: "users" },
            selectionSet: {
              kind: "SelectionSet",
              selections: [
                {
                  kind: "Field",
                  name: { kind: "Name", value: "me" },
                  selectionSet: {
                    kind: "SelectionSet",
                    selections: [
                      {
                        kind: "Field",
                        name: { kind: "Name", value: "stores" },
                        selectionSet: {
                          kind: "SelectionSet",
                          selections: [
                            {
                              kind: "Field",
                              name: { kind: "Name", value: "id" },
                            },
                            {
                              kind: "Field",
                              name: { kind: "Name", value: "name" },
                            },
                            {
                              kind: "Field",
                              name: { kind: "Name", value: "tagline" },
                            },
                            {
                              kind: "Field",
                              name: { kind: "Name", value: "icon" },
                            },
                          ],
                        },
                      },
                    ],
                  },
                },
              ],
            },
          },
        ],
      },
    },
  ],
} as unknown as DocumentNode<ExistingStoresQuery, ExistingStoresQueryVariables>;
export const UserNameDocument = {
  kind: "Document",
  definitions: [
    {
      kind: "OperationDefinition",
      operation: "query",
      name: { kind: "Name", value: "userName" },
      selectionSet: {
        kind: "SelectionSet",
        selections: [
          {
            kind: "Field",
            name: { kind: "Name", value: "users" },
            selectionSet: {
              kind: "SelectionSet",
              selections: [
                {
                  kind: "Field",
                  name: { kind: "Name", value: "me" },
                  selectionSet: {
                    kind: "SelectionSet",
                    selections: [
                      { kind: "Field", name: { kind: "Name", value: "name" } },
                    ],
                  },
                },
              ],
            },
          },
        ],
      },
    },
  ],
} as unknown as DocumentNode<UserNameQuery, UserNameQueryVariables>;
export const GetEnabledAcmeProvidersDocument = {
  kind: "Document",
  definitions: [
    {
      kind: "OperationDefinition",
      operation: "query",
      name: { kind: "Name", value: "getEnabledAcmeProviders" },
      selectionSet: {
        kind: "SelectionSet",
        selections: [
          {
            kind: "Field",
            name: { kind: "Name", value: "users" },
            selectionSet: {
              kind: "SelectionSet",
              selections: [
                {
                  kind: "Field",
                  name: { kind: "Name", value: "me" },
                  selectionSet: {
                    kind: "SelectionSet",
                    selections: [
                      {
                        kind: "Field",
                        name: { kind: "Name", value: "acmeProviders" },
                      },
                      {
                        kind: "Field",
                        name: { kind: "Name", value: "permissions" },
                      },
                    ],
                  },
                },
              ],
            },
          },
        ],
      },
    },
  ],
} as unknown as DocumentNode<
  GetEnabledAcmeProvidersQuery,
  GetEnabledAcmeProvidersQueryVariables
>;
export const GetVerificationCodeDocument = {
  kind: "Document",
  definitions: [
    {
      kind: "OperationDefinition",
      operation: "query",
      name: { kind: "Name", value: "getVerificationCode" },
      variableDefinitions: [
        {
          kind: "VariableDefinition",
          variable: {
            kind: "Variable",
            name: { kind: "Name", value: "domain" },
          },
          type: {
            kind: "NonNullType",
            type: {
              kind: "NamedType",
              name: { kind: "Name", value: "String" },
            },
          },
        },
      ],
      selectionSet: {
        kind: "SelectionSet",
        selections: [
          {
            kind: "Field",
            name: { kind: "Name", value: "users" },
            selectionSet: {
              kind: "SelectionSet",
              selections: [
                {
                  kind: "Field",
                  name: { kind: "Name", value: "me" },
                  selectionSet: {
                    kind: "SelectionSet",
                    selections: [
                      {
                        kind: "Field",
                        name: { kind: "Name", value: "domainVerificationCode" },
                        arguments: [
                          {
                            kind: "Argument",
                            name: { kind: "Name", value: "domain" },
                            value: {
                              kind: "Variable",
                              name: { kind: "Name", value: "domain" },
                            },
                          },
                        ],
                      },
                    ],
                  },
                },
              ],
            },
          },
        ],
      },
    },
  ],
} as unknown as DocumentNode<
  GetVerificationCodeQuery,
  GetVerificationCodeQueryVariables
>;
export const GetLinkedDomainsDocument = {
  kind: "Document",
  definitions: [
    {
      kind: "OperationDefinition",
      operation: "query",
      name: { kind: "Name", value: "getLinkedDomains" },
      selectionSet: {
        kind: "SelectionSet",
        selections: [
          {
            kind: "Field",
            name: { kind: "Name", value: "users" },
            selectionSet: {
              kind: "SelectionSet",
              selections: [
                {
                  kind: "Field",
                  name: { kind: "Name", value: "me" },
                  selectionSet: {
                    kind: "SelectionSet",
                    selections: [
                      {
                        kind: "Field",
                        name: { kind: "Name", value: "domains" },
                        selectionSet: {
                          kind: "SelectionSet",
                          selections: [
                            {
                              kind: "Field",
                              name: { kind: "Name", value: "domain" },
                            },
                            {
                              kind: "Field",
                              name: { kind: "Name", value: "dnsProvider" },
                            },
                          ],
                        },
                      },
                    ],
                  },
                },
              ],
            },
          },
        ],
      },
    },
  ],
} as unknown as DocumentNode<
  GetLinkedDomainsQuery,
  GetLinkedDomainsQueryVariables
>;
export const GetHttpsAppsDocument = {
  kind: "Document",
  definitions: [
    {
      kind: "OperationDefinition",
      operation: "query",
      name: { kind: "Name", value: "getHttpsApps" },
      selectionSet: {
        kind: "SelectionSet",
        selections: [
          {
            kind: "Field",
            name: { kind: "Name", value: "users" },
            selectionSet: {
              kind: "SelectionSet",
              selections: [
                {
                  kind: "Field",
                  name: { kind: "Name", value: "me" },
                  selectionSet: {
                    kind: "SelectionSet",
                    selections: [
                      {
                        kind: "Field",
                        name: { kind: "Name", value: "apps" },
                        arguments: [
                          {
                            kind: "Argument",
                            name: { kind: "Name", value: "installedOnly" },
                            value: { kind: "BooleanValue", value: true },
                          },
                        ],
                        selectionSet: {
                          kind: "SelectionSet",
                          selections: [
                            {
                              kind: "Field",
                              name: { kind: "Name", value: "id" },
                            },
                            {
                              kind: "Field",
                              name: { kind: "Name", value: "name" },
                            },
                            {
                              kind: "Field",
                              name: { kind: "Name", value: "icon" },
                            },
                            {
                              kind: "Field",
                              name: { kind: "Name", value: "path" },
                            },
                            {
                              kind: "Field",
                              name: { kind: "Name", value: "supportsIngress" },
                            },
                            {
                              kind: "Field",
                              name: { kind: "Name", value: "paused" },
                            },
                            {
                              kind: "Field",
                              name: { kind: "Name", value: "domains" },
                              selectionSet: {
                                kind: "SelectionSet",
                                selections: [
                                  {
                                    kind: "Field",
                                    name: { kind: "Name", value: "name" },
                                  },
                                ],
                              },
                            },
                          ],
                        },
                      },
                    ],
                  },
                },
              ],
            },
          },
        ],
      },
    },
  ],
} as unknown as DocumentNode<GetHttpsAppsQuery, GetHttpsAppsQueryVariables>;
export const AddOnionDomainDocument = {
  kind: "Document",
  definitions: [
    {
      kind: "OperationDefinition",
      operation: "mutation",
      name: { kind: "Name", value: "addOnionDomain" },
      variableDefinitions: [
        {
          kind: "VariableDefinition",
          variable: { kind: "Variable", name: { kind: "Name", value: "app" } },
          type: {
            kind: "NonNullType",
            type: {
              kind: "NamedType",
              name: { kind: "Name", value: "String" },
            },
          },
        },
      ],
      selectionSet: {
        kind: "SelectionSet",
        selections: [
          {
            kind: "Field",
            name: { kind: "Name", value: "addOnionDomain" },
            arguments: [
              {
                kind: "Argument",
                name: { kind: "Name", value: "appId" },
                value: {
                  kind: "Variable",
                  name: { kind: "Name", value: "app" },
                },
              },
            ],
          },
        ],
      },
    },
  ],
} as unknown as DocumentNode<
  AddOnionDomainMutation,
  AddOnionDomainMutationVariables
>;
export const DeleteDomainDocument = {
  kind: "Document",
  definitions: [
    {
      kind: "OperationDefinition",
      operation: "mutation",
      name: { kind: "Name", value: "deleteDomain" },
      variableDefinitions: [
        {
          kind: "VariableDefinition",
          variable: { kind: "Variable", name: { kind: "Name", value: "name" } },
          type: {
            kind: "NonNullType",
            type: {
              kind: "NamedType",
              name: { kind: "Name", value: "String" },
            },
          },
        },
      ],
      selectionSet: {
        kind: "SelectionSet",
        selections: [
          {
            kind: "Field",
            name: { kind: "Name", value: "removeAppDomain" },
            arguments: [
              {
                kind: "Argument",
                name: { kind: "Name", value: "domain" },
                value: {
                  kind: "Variable",
                  name: { kind: "Name", value: "name" },
                },
              },
            ],
          },
        ],
      },
    },
  ],
} as unknown as DocumentNode<
  DeleteDomainMutation,
  DeleteDomainMutationVariables
>;
export const GetProxiesInfoDocument = {
  kind: "Document",
  definitions: [
    {
      kind: "OperationDefinition",
      operation: "query",
      name: { kind: "Name", value: "getProxiesInfo" },
      selectionSet: {
        kind: "SelectionSet",
        selections: [
          {
            kind: "Field",
            name: { kind: "Name", value: "users" },
            selectionSet: {
              kind: "SelectionSet",
              selections: [
                {
                  kind: "Field",
                  name: { kind: "Name", value: "me" },
                  selectionSet: {
                    kind: "SelectionSet",
                    selections: [
                      {
                        kind: "Field",
                        name: { kind: "Name", value: "proxies" },
                        selectionSet: {
                          kind: "SelectionSet",
                          selections: [
                            {
                              kind: "Field",
                              name: { kind: "Name", value: "domain" },
                            },
                            {
                              kind: "Field",
                              name: { kind: "Name", value: "routes" },
                              selectionSet: {
                                kind: "SelectionSet",
                                selections: [
                                  {
                                    kind: "Field",
                                    name: { kind: "Name", value: "id" },
                                  },
                                  {
                                    kind: "Field",
                                    name: { kind: "Name", value: "localPath" },
                                  },
                                  {
                                    kind: "Field",
                                    name: { kind: "Name", value: "targetPath" },
                                  },
                                  {
                                    kind: "Field",
                                    name: { kind: "Name", value: "protocol" },
                                  },
                                  {
                                    kind: "Field",
                                    name: { kind: "Name", value: "service" },
                                    selectionSet: {
                                      kind: "SelectionSet",
                                      selections: [
                                        {
                                          kind: "Field",
                                          name: {
                                            kind: "Name",
                                            value: "upstream",
                                          },
                                        },
                                      ],
                                    },
                                  },
                                ],
                              },
                            },
                          ],
                        },
                      },
                      {
                        kind: "Field",
                        name: { kind: "Name", value: "services" },
                        selectionSet: {
                          kind: "SelectionSet",
                          selections: [
                            {
                              kind: "Field",
                              name: { kind: "Name", value: "id" },
                            },
                            {
                              kind: "Field",
                              name: { kind: "Name", value: "upstream" },
                            },
                            {
                              kind: "Field",
                              name: { kind: "Name", value: "tcpPorts" },
                            },
                            {
                              kind: "Field",
                              name: { kind: "Name", value: "udpPorts" },
                            },
                          ],
                        },
                      },
                    ],
                  },
                },
              ],
            },
          },
        ],
      },
    },
  ],
} as unknown as DocumentNode<GetProxiesInfoQuery, GetProxiesInfoQueryVariables>;
export const CreateServiceDocument = {
  kind: "Document",
  definitions: [
    {
      kind: "OperationDefinition",
      operation: "mutation",
      name: { kind: "Name", value: "createService" },
      variableDefinitions: [
        {
          kind: "VariableDefinition",
          variable: { kind: "Variable", name: { kind: "Name", value: "ip" } },
          type: { kind: "NamedType", name: { kind: "Name", value: "String" } },
        },
        {
          kind: "VariableDefinition",
          variable: {
            kind: "Variable",
            name: { kind: "Name", value: "dnsName" },
          },
          type: { kind: "NamedType", name: { kind: "Name", value: "String" } },
        },
        {
          kind: "VariableDefinition",
          variable: {
            kind: "Variable",
            name: { kind: "Name", value: "tcpPorts" },
          },
          type: {
            kind: "NonNullType",
            type: {
              kind: "ListType",
              type: {
                kind: "NonNullType",
                type: {
                  kind: "NamedType",
                  name: { kind: "Name", value: "Int" },
                },
              },
            },
          },
        },
        {
          kind: "VariableDefinition",
          variable: {
            kind: "Variable",
            name: { kind: "Name", value: "udpPorts" },
          },
          type: {
            kind: "NonNullType",
            type: {
              kind: "ListType",
              type: {
                kind: "NonNullType",
                type: {
                  kind: "NamedType",
                  name: { kind: "Name", value: "Int" },
                },
              },
            },
          },
        },
      ],
      selectionSet: {
        kind: "SelectionSet",
        selections: [
          {
            kind: "Field",
            name: { kind: "Name", value: "createService" },
            arguments: [
              {
                kind: "Argument",
                name: { kind: "Name", value: "ip" },
                value: {
                  kind: "Variable",
                  name: { kind: "Name", value: "ip" },
                },
              },
              {
                kind: "Argument",
                name: { kind: "Name", value: "dnsName" },
                value: {
                  kind: "Variable",
                  name: { kind: "Name", value: "dnsName" },
                },
              },
              {
                kind: "Argument",
                name: { kind: "Name", value: "tcpPorts" },
                value: {
                  kind: "Variable",
                  name: { kind: "Name", value: "tcpPorts" },
                },
              },
              {
                kind: "Argument",
                name: { kind: "Name", value: "udpPorts" },
                value: {
                  kind: "Variable",
                  name: { kind: "Name", value: "udpPorts" },
                },
              },
            ],
          },
        ],
      },
    },
  ],
} as unknown as DocumentNode<
  CreateServiceMutation,
  CreateServiceMutationVariables
>;
export const AddServicePortsDocument = {
  kind: "Document",
  definitions: [
    {
      kind: "OperationDefinition",
      operation: "mutation",
      name: { kind: "Name", value: "addServicePorts" },
      variableDefinitions: [
        {
          kind: "VariableDefinition",
          variable: { kind: "Variable", name: { kind: "Name", value: "id" } },
          type: {
            kind: "NonNullType",
            type: {
              kind: "NamedType",
              name: { kind: "Name", value: "String" },
            },
          },
        },
        {
          kind: "VariableDefinition",
          variable: {
            kind: "Variable",
            name: { kind: "Name", value: "tcpPorts" },
          },
          type: {
            kind: "NonNullType",
            type: {
              kind: "ListType",
              type: {
                kind: "NonNullType",
                type: {
                  kind: "NamedType",
                  name: { kind: "Name", value: "Int" },
                },
              },
            },
          },
        },
        {
          kind: "VariableDefinition",
          variable: {
            kind: "Variable",
            name: { kind: "Name", value: "udpPorts" },
          },
          type: {
            kind: "NonNullType",
            type: {
              kind: "ListType",
              type: {
                kind: "NonNullType",
                type: {
                  kind: "NamedType",
                  name: { kind: "Name", value: "Int" },
                },
              },
            },
          },
        },
      ],
      selectionSet: {
        kind: "SelectionSet",
        selections: [
          {
            kind: "Field",
            name: { kind: "Name", value: "addServicePorts" },
            arguments: [
              {
                kind: "Argument",
                name: { kind: "Name", value: "id" },
                value: {
                  kind: "Variable",
                  name: { kind: "Name", value: "id" },
                },
              },
              {
                kind: "Argument",
                name: { kind: "Name", value: "tcpPorts" },
                value: {
                  kind: "Variable",
                  name: { kind: "Name", value: "tcpPorts" },
                },
              },
              {
                kind: "Argument",
                name: { kind: "Name", value: "udpPorts" },
                value: {
                  kind: "Variable",
                  name: { kind: "Name", value: "udpPorts" },
                },
              },
            ],
          },
        ],
      },
    },
  ],
} as unknown as DocumentNode<
  AddServicePortsMutation,
  AddServicePortsMutationVariables
>;
export const CreateProxyDocument = {
  kind: "Document",
  definitions: [
    {
      kind: "OperationDefinition",
      operation: "mutation",
      name: { kind: "Name", value: "createProxy" },
      variableDefinitions: [
        {
          kind: "VariableDefinition",
          variable: {
            kind: "Variable",
            name: { kind: "Name", value: "domain" },
          },
          type: {
            kind: "NonNullType",
            type: {
              kind: "NamedType",
              name: { kind: "Name", value: "String" },
            },
          },
        },
      ],
      selectionSet: {
        kind: "SelectionSet",
        selections: [
          {
            kind: "Field",
            name: { kind: "Name", value: "createProxy" },
            arguments: [
              {
                kind: "Argument",
                name: { kind: "Name", value: "domain" },
                value: {
                  kind: "Variable",
                  name: { kind: "Name", value: "domain" },
                },
              },
              {
                kind: "Argument",
                name: { kind: "Name", value: "acmeProvider" },
                value: { kind: "EnumValue", value: "LETS_ENCRYPT" },
              },
            ],
          },
        ],
      },
    },
  ],
} as unknown as DocumentNode<CreateProxyMutation, CreateProxyMutationVariables>;
export const DeleteProxyDocument = {
  kind: "Document",
  definitions: [
    {
      kind: "OperationDefinition",
      operation: "mutation",
      name: { kind: "Name", value: "deleteProxy" },
      variableDefinitions: [
        {
          kind: "VariableDefinition",
          variable: {
            kind: "Variable",
            name: { kind: "Name", value: "domain" },
          },
          type: {
            kind: "NonNullType",
            type: {
              kind: "NamedType",
              name: { kind: "Name", value: "String" },
            },
          },
        },
      ],
      selectionSet: {
        kind: "SelectionSet",
        selections: [
          {
            kind: "Field",
            name: { kind: "Name", value: "deleteProxy" },
            arguments: [
              {
                kind: "Argument",
                name: { kind: "Name", value: "domain" },
                value: {
                  kind: "Variable",
                  name: { kind: "Name", value: "domain" },
                },
              },
            ],
          },
        ],
      },
    },
  ],
} as unknown as DocumentNode<DeleteProxyMutation, DeleteProxyMutationVariables>;
export const CreateRouteDocument = {
  kind: "Document",
  definitions: [
    {
      kind: "OperationDefinition",
      operation: "mutation",
      name: { kind: "Name", value: "createRoute" },
      variableDefinitions: [
        {
          kind: "VariableDefinition",
          variable: {
            kind: "Variable",
            name: { kind: "Name", value: "domain" },
          },
          type: {
            kind: "NonNullType",
            type: {
              kind: "NamedType",
              name: { kind: "Name", value: "String" },
            },
          },
        },
        {
          kind: "VariableDefinition",
          variable: {
            kind: "Variable",
            name: { kind: "Name", value: "serviceId" },
          },
          type: {
            kind: "NonNullType",
            type: {
              kind: "NamedType",
              name: { kind: "Name", value: "String" },
            },
          },
        },
        {
          kind: "VariableDefinition",
          variable: {
            kind: "Variable",
            name: { kind: "Name", value: "localPath" },
          },
          type: {
            kind: "NonNullType",
            type: {
              kind: "NamedType",
              name: { kind: "Name", value: "String" },
            },
          },
        },
        {
          kind: "VariableDefinition",
          variable: {
            kind: "Variable",
            name: { kind: "Name", value: "targetPath" },
          },
          type: {
            kind: "NonNullType",
            type: {
              kind: "NamedType",
              name: { kind: "Name", value: "String" },
            },
          },
        },
        {
          kind: "VariableDefinition",
          variable: {
            kind: "Variable",
            name: { kind: "Name", value: "targetPort" },
          },
          type: {
            kind: "NonNullType",
            type: { kind: "NamedType", name: { kind: "Name", value: "Int" } },
          },
        },
        {
          kind: "VariableDefinition",
          variable: {
            kind: "Variable",
            name: { kind: "Name", value: "protocol" },
          },
          type: {
            kind: "NonNullType",
            type: {
              kind: "NamedType",
              name: { kind: "Name", value: "Protocol" },
            },
          },
        },
        {
          kind: "VariableDefinition",
          variable: {
            kind: "Variable",
            name: { kind: "Name", value: "enableCompression" },
          },
          type: {
            kind: "NonNullType",
            type: {
              kind: "NamedType",
              name: { kind: "Name", value: "Boolean" },
            },
          },
        },
        {
          kind: "VariableDefinition",
          variable: {
            kind: "Variable",
            name: { kind: "Name", value: "allowInvalidCertificates" },
          },
          type: {
            kind: "NonNullType",
            type: {
              kind: "NamedType",
              name: { kind: "Name", value: "Boolean" },
            },
          },
        },
        {
          kind: "VariableDefinition",
          variable: {
            kind: "Variable",
            name: { kind: "Name", value: "targetSni" },
          },
          type: { kind: "NamedType", name: { kind: "Name", value: "String" } },
        },
      ],
      selectionSet: {
        kind: "SelectionSet",
        selections: [
          {
            kind: "Field",
            name: { kind: "Name", value: "createRoute" },
            arguments: [
              {
                kind: "Argument",
                name: { kind: "Name", value: "proxyDomain" },
                value: {
                  kind: "Variable",
                  name: { kind: "Name", value: "domain" },
                },
              },
              {
                kind: "Argument",
                name: { kind: "Name", value: "serviceId" },
                value: {
                  kind: "Variable",
                  name: { kind: "Name", value: "serviceId" },
                },
              },
              {
                kind: "Argument",
                name: { kind: "Name", value: "localPath" },
                value: {
                  kind: "Variable",
                  name: { kind: "Name", value: "localPath" },
                },
              },
              {
                kind: "Argument",
                name: { kind: "Name", value: "targetPath" },
                value: {
                  kind: "Variable",
                  name: { kind: "Name", value: "targetPath" },
                },
              },
              {
                kind: "Argument",
                name: { kind: "Name", value: "targetPort" },
                value: {
                  kind: "Variable",
                  name: { kind: "Name", value: "targetPort" },
                },
              },
              {
                kind: "Argument",
                name: { kind: "Name", value: "protocol" },
                value: {
                  kind: "Variable",
                  name: { kind: "Name", value: "protocol" },
                },
              },
              {
                kind: "Argument",
                name: { kind: "Name", value: "enableCompression" },
                value: {
                  kind: "Variable",
                  name: { kind: "Name", value: "enableCompression" },
                },
              },
              {
                kind: "Argument",
                name: { kind: "Name", value: "allowInvalidCert" },
                value: {
                  kind: "Variable",
                  name: { kind: "Name", value: "allowInvalidCertificates" },
                },
              },
              {
                kind: "Argument",
                name: { kind: "Name", value: "targetSni" },
                value: {
                  kind: "Variable",
                  name: { kind: "Name", value: "targetSni" },
                },
              },
            ],
          },
        ],
      },
    },
  ],
} as unknown as DocumentNode<CreateRouteMutation, CreateRouteMutationVariables>;
export const DeleteRouteDocument = {
  kind: "Document",
  definitions: [
    {
      kind: "OperationDefinition",
      operation: "mutation",
      name: { kind: "Name", value: "deleteRoute" },
      variableDefinitions: [
        {
          kind: "VariableDefinition",
          variable: { kind: "Variable", name: { kind: "Name", value: "id" } },
          type: {
            kind: "NonNullType",
            type: {
              kind: "NamedType",
              name: { kind: "Name", value: "String" },
            },
          },
        },
      ],
      selectionSet: {
        kind: "SelectionSet",
        selections: [
          {
            kind: "Field",
            name: { kind: "Name", value: "deleteRoute" },
            arguments: [
              {
                kind: "Argument",
                name: { kind: "Name", value: "routeId" },
                value: {
                  kind: "Variable",
                  name: { kind: "Name", value: "id" },
                },
              },
            ],
          },
        ],
      },
    },
  ],
} as unknown as DocumentNode<DeleteRouteMutation, DeleteRouteMutationVariables>;
export const GetIsSetupDocument = {
  kind: "Document",
  definitions: [
    {
      kind: "OperationDefinition",
      operation: "query",
      name: { kind: "Name", value: "getIsSetup" },
      selectionSet: {
        kind: "SelectionSet",
        selections: [
          {
            kind: "Field",
            name: { kind: "Name", value: "info" },
            selectionSet: {
              kind: "SelectionSet",
              selections: [
                { kind: "Field", name: { kind: "Name", value: "isUserSetup" } },
              ],
            },
          },
        ],
      },
    },
  ],
} as unknown as DocumentNode<GetIsSetupQuery, GetIsSetupQueryVariables>;
export const GetIsSetupFinishedDocument = {
  kind: "Document",
  definitions: [
    {
      kind: "OperationDefinition",
      operation: "query",
      name: { kind: "Name", value: "getIsSetupFinished" },
      selectionSet: {
        kind: "SelectionSet",
        selections: [
          {
            kind: "Field",
            name: { kind: "Name", value: "info" },
            selectionSet: {
              kind: "SelectionSet",
              selections: [
                {
                  kind: "Field",
                  name: { kind: "Name", value: "isSetupFinished" },
                },
              ],
            },
          },
        ],
      },
    },
  ],
} as unknown as DocumentNode<
  GetIsSetupFinishedQuery,
  GetIsSetupFinishedQueryVariables
>;
export const GetLoginUserInfoDocument = {
  kind: "Document",
  definitions: [
    {
      kind: "OperationDefinition",
      operation: "query",
      name: { kind: "Name", value: "getLoginUserInfo" },
      selectionSet: {
        kind: "SelectionSet",
        selections: [
          {
            kind: "Field",
            name: { kind: "Name", value: "users" },
            selectionSet: {
              kind: "SelectionSet",
              selections: [
                {
                  kind: "Field",
                  name: { kind: "Name", value: "loginUsernames" },
                  selectionSet: {
                    kind: "SelectionSet",
                    selections: [
                      {
                        kind: "Field",
                        name: { kind: "Name", value: "username" },
                      },
                      { kind: "Field", name: { kind: "Name", value: "name" } },
                    ],
                  },
                },
              ],
            },
          },
        ],
      },
    },
  ],
} as unknown as DocumentNode<
  GetLoginUserInfoQuery,
  GetLoginUserInfoQueryVariables
>;
export const PerformLoginDocument = {
  kind: "Document",
  definitions: [
    {
      kind: "OperationDefinition",
      operation: "mutation",
      name: { kind: "Name", value: "performLogin" },
      variableDefinitions: [
        {
          kind: "VariableDefinition",
          variable: { kind: "Variable", name: { kind: "Name", value: "id" } },
          type: {
            kind: "NonNullType",
            type: {
              kind: "NamedType",
              name: { kind: "Name", value: "String" },
            },
          },
        },
        {
          kind: "VariableDefinition",
          variable: {
            kind: "Variable",
            name: { kind: "Name", value: "password" },
          },
          type: {
            kind: "NonNullType",
            type: {
              kind: "NamedType",
              name: { kind: "Name", value: "String" },
            },
          },
        },
      ],
      selectionSet: {
        kind: "SelectionSet",
        selections: [
          {
            kind: "Field",
            name: { kind: "Name", value: "login" },
            arguments: [
              {
                kind: "Argument",
                name: { kind: "Name", value: "username" },
                value: {
                  kind: "Variable",
                  name: { kind: "Name", value: "id" },
                },
              },
              {
                kind: "Argument",
                name: { kind: "Name", value: "password" },
                value: {
                  kind: "Variable",
                  name: { kind: "Name", value: "password" },
                },
              },
            ],
          },
        ],
      },
    },
  ],
} as unknown as DocumentNode<
  PerformLoginMutation,
  PerformLoginMutationVariables
>;
export const GetIsSetupUserFinishedDocument = {
  kind: "Document",
  definitions: [
    {
      kind: "OperationDefinition",
      operation: "query",
      name: { kind: "Name", value: "getIsSetupUserFinished" },
      selectionSet: {
        kind: "SelectionSet",
        selections: [
          {
            kind: "Field",
            name: { kind: "Name", value: "users" },
            selectionSet: {
              kind: "SelectionSet",
              selections: [
                {
                  kind: "Field",
                  name: { kind: "Name", value: "me" },
                  selectionSet: {
                    kind: "SelectionSet",
                    selections: [
                      {
                        kind: "Field",
                        name: { kind: "Name", value: "setupFinished" },
                      },
                    ],
                  },
                },
              ],
            },
          },
        ],
      },
    },
  ],
} as unknown as DocumentNode<
  GetIsSetupUserFinishedQuery,
  GetIsSetupUserFinishedQueryVariables
>;
export const CreatePasskeyChallengeDocument = {
  kind: "Document",
  definitions: [
    {
      kind: "OperationDefinition",
      operation: "mutation",
      name: { kind: "Name", value: "createPasskeyChallenge" },
      variableDefinitions: [
        {
          kind: "VariableDefinition",
          variable: {
            kind: "Variable",
            name: { kind: "Name", value: "username" },
          },
          type: {
            kind: "NonNullType",
            type: {
              kind: "NamedType",
              name: { kind: "Name", value: "String" },
            },
          },
        },
      ],
      selectionSet: {
        kind: "SelectionSet",
        selections: [
          {
            kind: "Field",
            name: { kind: "Name", value: "createChallenge" },
            arguments: [
              {
                kind: "Argument",
                name: { kind: "Name", value: "username" },
                value: {
                  kind: "Variable",
                  name: { kind: "Name", value: "username" },
                },
              },
            ],
          },
        ],
      },
    },
  ],
} as unknown as DocumentNode<
  CreatePasskeyChallengeMutation,
  CreatePasskeyChallengeMutationVariables
>;
export const PasskeyLoginDocument = {
  kind: "Document",
  definitions: [
    {
      kind: "OperationDefinition",
      operation: "mutation",
      name: { kind: "Name", value: "passkeyLogin" },
      variableDefinitions: [
        {
          kind: "VariableDefinition",
          variable: {
            kind: "Variable",
            name: { kind: "Name", value: "keyId" },
          },
          type: {
            kind: "NonNullType",
            type: {
              kind: "NamedType",
              name: { kind: "Name", value: "String" },
            },
          },
        },
        {
          kind: "VariableDefinition",
          variable: {
            kind: "Variable",
            name: { kind: "Name", value: "response" },
          },
          type: {
            kind: "NonNullType",
            type: {
              kind: "NamedType",
              name: { kind: "Name", value: "String" },
            },
          },
        },
        {
          kind: "VariableDefinition",
          variable: {
            kind: "Variable",
            name: { kind: "Name", value: "authenticatorData" },
          },
          type: {
            kind: "NonNullType",
            type: {
              kind: "NamedType",
              name: { kind: "Name", value: "String" },
            },
          },
        },
        {
          kind: "VariableDefinition",
          variable: {
            kind: "Variable",
            name: { kind: "Name", value: "clientDataJSON" },
          },
          type: {
            kind: "NonNullType",
            type: {
              kind: "NamedType",
              name: { kind: "Name", value: "String" },
            },
          },
        },
      ],
      selectionSet: {
        kind: "SelectionSet",
        selections: [
          {
            kind: "Field",
            name: { kind: "Name", value: "loginWithPasskey" },
            arguments: [
              {
                kind: "Argument",
                name: { kind: "Name", value: "keyId" },
                value: {
                  kind: "Variable",
                  name: { kind: "Name", value: "keyId" },
                },
              },
              {
                kind: "Argument",
                name: { kind: "Name", value: "signature" },
                value: {
                  kind: "Variable",
                  name: { kind: "Name", value: "response" },
                },
              },
              {
                kind: "Argument",
                name: { kind: "Name", value: "authenticatorData" },
                value: {
                  kind: "Variable",
                  name: { kind: "Name", value: "authenticatorData" },
                },
              },
              {
                kind: "Argument",
                name: { kind: "Name", value: "clientDataJson" },
                value: {
                  kind: "Variable",
                  name: { kind: "Name", value: "clientDataJSON" },
                },
              },
            ],
          },
        ],
      },
    },
  ],
} as unknown as DocumentNode<
  PasskeyLoginMutation,
  PasskeyLoginMutationVariables
>;
export const GetAppDocument = {
  kind: "Document",
  definitions: [
    {
      kind: "OperationDefinition",
      operation: "query",
      name: { kind: "Name", value: "getApp" },
      variableDefinitions: [
        {
          kind: "VariableDefinition",
          variable: { kind: "Variable", name: { kind: "Name", value: "id" } },
          type: {
            kind: "NonNullType",
            type: {
              kind: "NamedType",
              name: { kind: "Name", value: "String" },
            },
          },
        },
      ],
      selectionSet: {
        kind: "SelectionSet",
        selections: [
          {
            kind: "Field",
            name: { kind: "Name", value: "users" },
            selectionSet: {
              kind: "SelectionSet",
              selections: [
                {
                  kind: "Field",
                  name: { kind: "Name", value: "me" },
                  selectionSet: {
                    kind: "SelectionSet",
                    selections: [
                      {
                        kind: "Field",
                        name: { kind: "Name", value: "app" },
                        arguments: [
                          {
                            kind: "Argument",
                            name: { kind: "Name", value: "appId" },
                            value: {
                              kind: "Variable",
                              name: { kind: "Name", value: "id" },
                            },
                          },
                        ],
                        selectionSet: {
                          kind: "SelectionSet",
                          selections: [
                            {
                              kind: "Field",
                              name: { kind: "Name", value: "domains" },
                              selectionSet: {
                                kind: "SelectionSet",
                                selections: [
                                  {
                                    kind: "Field",
                                    name: { kind: "Name", value: "name" },
                                  },
                                ],
                              },
                            },
                            {
                              kind: "Field",
                              name: { kind: "Name", value: "installed" },
                            },
                            {
                              kind: "Field",
                              name: { kind: "Name", value: "name" },
                            },
                            {
                              kind: "Field",
                              name: { kind: "Name", value: "icon" },
                            },
                          ],
                        },
                      },
                    ],
                  },
                },
              ],
            },
          },
        ],
      },
    },
  ],
} as unknown as DocumentNode<GetAppQuery, GetAppQueryVariables>;
export const GetPluginJwtDocument = {
  kind: "Document",
  definitions: [
    {
      kind: "OperationDefinition",
      operation: "query",
      name: { kind: "Name", value: "getPluginJwt" },
      variableDefinitions: [
        {
          kind: "VariableDefinition",
          variable: {
            kind: "Variable",
            name: { kind: "Name", value: "audience" },
          },
          type: {
            kind: "NonNullType",
            type: {
              kind: "NamedType",
              name: { kind: "Name", value: "String" },
            },
          },
        },
      ],
      selectionSet: {
        kind: "SelectionSet",
        selections: [
          {
            kind: "Field",
            name: { kind: "Name", value: "users" },
            selectionSet: {
              kind: "SelectionSet",
              selections: [
                {
                  kind: "Field",
                  name: { kind: "Name", value: "me" },
                  selectionSet: {
                    kind: "SelectionSet",
                    selections: [
                      {
                        kind: "Field",
                        name: { kind: "Name", value: "jwt" },
                        arguments: [
                          {
                            kind: "Argument",
                            name: { kind: "Name", value: "audience" },
                            value: {
                              kind: "ListValue",
                              values: [
                                {
                                  kind: "Variable",
                                  name: { kind: "Name", value: "audience" },
                                },
                              ],
                            },
                          },
                        ],
                      },
                    ],
                  },
                },
              ],
            },
          },
        ],
      },
    },
  ],
} as unknown as DocumentNode<GetPluginJwtQuery, GetPluginJwtQueryVariables>;
export const ExistingPasskeysDocument = {
  kind: "Document",
  definitions: [
    {
      kind: "OperationDefinition",
      operation: "query",
      name: { kind: "Name", value: "existingPasskeys" },
      selectionSet: {
        kind: "SelectionSet",
        selections: [
          {
            kind: "Field",
            name: { kind: "Name", value: "users" },
            selectionSet: {
              kind: "SelectionSet",
              selections: [
                {
                  kind: "Field",
                  name: { kind: "Name", value: "me" },
                  selectionSet: {
                    kind: "SelectionSet",
                    selections: [
                      {
                        kind: "Field",
                        name: { kind: "Name", value: "passkeys" },
                        selectionSet: {
                          kind: "SelectionSet",
                          selections: [
                            {
                              kind: "Field",
                              name: { kind: "Name", value: "displayName" },
                            },
                            {
                              kind: "Field",
                              name: { kind: "Name", value: "rpId" },
                            },
                            {
                              kind: "Field",
                              name: { kind: "Name", value: "createdAt" },
                            },
                            {
                              kind: "Field",
                              name: { kind: "Name", value: "keyIdBase64" },
                            },
                          ],
                        },
                      },
                    ],
                  },
                },
              ],
            },
          },
        ],
      },
    },
  ],
} as unknown as DocumentNode<
  ExistingPasskeysQuery,
  ExistingPasskeysQueryVariables
>;
export const DeletePasskeyDocument = {
  kind: "Document",
  definitions: [
    {
      kind: "OperationDefinition",
      operation: "mutation",
      name: { kind: "Name", value: "deletePasskey" },
      variableDefinitions: [
        {
          kind: "VariableDefinition",
          variable: { kind: "Variable", name: { kind: "Name", value: "id" } },
          type: {
            kind: "NonNullType",
            type: {
              kind: "NamedType",
              name: { kind: "Name", value: "String" },
            },
          },
        },
      ],
      selectionSet: {
        kind: "SelectionSet",
        selections: [
          {
            kind: "Field",
            name: { kind: "Name", value: "removePubkey" },
            arguments: [
              {
                kind: "Argument",
                name: { kind: "Name", value: "keyId" },
                value: {
                  kind: "Variable",
                  name: { kind: "Name", value: "id" },
                },
              },
            ],
          },
        ],
      },
    },
  ],
} as unknown as DocumentNode<
  DeletePasskeyMutation,
  DeletePasskeyMutationVariables
>;
export const UserCustomDomainInfoDocument = {
  kind: "Document",
  definitions: [
    {
      kind: "OperationDefinition",
      operation: "query",
      name: { kind: "Name", value: "userCustomDomainInfo" },
      selectionSet: {
        kind: "SelectionSet",
        selections: [
          {
            kind: "Field",
            name: { kind: "Name", value: "users" },
            selectionSet: {
              kind: "SelectionSet",
              selections: [
                {
                  kind: "Field",
                  name: { kind: "Name", value: "me" },
                  selectionSet: {
                    kind: "SelectionSet",
                    selections: [
                      {
                        kind: "Field",
                        name: { kind: "Name", value: "acmeProviders" },
                      },
                      {
                        kind: "Field",
                        name: { kind: "Name", value: "permissions" },
                      },
                    ],
                  },
                },
              ],
            },
          },
          {
            kind: "Field",
            name: { kind: "Name", value: "network" },
            selectionSet: {
              kind: "SelectionSet",
              selections: [
                { kind: "Field", name: { kind: "Name", value: "ip" } },
              ],
            },
          },
        ],
      },
    },
  ],
} as unknown as DocumentNode<
  UserCustomDomainInfoQuery,
  UserCustomDomainInfoQueryVariables
>;
export const GetDomainVerificationCodeDocument = {
  kind: "Document",
  definitions: [
    {
      kind: "OperationDefinition",
      operation: "query",
      name: { kind: "Name", value: "getDomainVerificationCode" },
      variableDefinitions: [
        {
          kind: "VariableDefinition",
          variable: {
            kind: "Variable",
            name: { kind: "Name", value: "domain" },
          },
          type: {
            kind: "NonNullType",
            type: {
              kind: "NamedType",
              name: { kind: "Name", value: "String" },
            },
          },
        },
      ],
      selectionSet: {
        kind: "SelectionSet",
        selections: [
          {
            kind: "Field",
            name: { kind: "Name", value: "users" },
            selectionSet: {
              kind: "SelectionSet",
              selections: [
                {
                  kind: "Field",
                  name: { kind: "Name", value: "me" },
                  selectionSet: {
                    kind: "SelectionSet",
                    selections: [
                      {
                        kind: "Field",
                        name: { kind: "Name", value: "domainVerificationCode" },
                        arguments: [
                          {
                            kind: "Argument",
                            name: { kind: "Name", value: "domain" },
                            value: {
                              kind: "Variable",
                              name: { kind: "Name", value: "domain" },
                            },
                          },
                        ],
                      },
                    ],
                  },
                },
              ],
            },
          },
        ],
      },
    },
  ],
} as unknown as DocumentNode<
  GetDomainVerificationCodeQuery,
  GetDomainVerificationCodeQueryVariables
>;
export const EnsureCodeExistsDocument = {
  kind: "Document",
  definitions: [
    {
      kind: "OperationDefinition",
      operation: "query",
      name: { kind: "Name", value: "ensureCodeExists" },
      variableDefinitions: [
        {
          kind: "VariableDefinition",
          variable: {
            kind: "Variable",
            name: { kind: "Name", value: "domain" },
          },
          type: {
            kind: "NonNullType",
            type: {
              kind: "NamedType",
              name: { kind: "Name", value: "String" },
            },
          },
        },
      ],
      selectionSet: {
        kind: "SelectionSet",
        selections: [
          {
            kind: "Field",
            name: { kind: "Name", value: "users" },
            selectionSet: {
              kind: "SelectionSet",
              selections: [
                {
                  kind: "Field",
                  name: { kind: "Name", value: "me" },
                  selectionSet: {
                    kind: "SelectionSet",
                    selections: [
                      {
                        kind: "Field",
                        name: { kind: "Name", value: "verifyTxt" },
                        arguments: [
                          {
                            kind: "Argument",
                            name: { kind: "Name", value: "domain" },
                            value: {
                              kind: "Variable",
                              name: { kind: "Name", value: "domain" },
                            },
                          },
                        ],
                      },
                    ],
                  },
                },
              ],
            },
          },
        ],
      },
    },
  ],
} as unknown as DocumentNode<
  EnsureCodeExistsQuery,
  EnsureCodeExistsQueryVariables
>;
export const AddVerificationCodeDocument = {
  kind: "Document",
  definitions: [
    {
      kind: "OperationDefinition",
      operation: "mutation",
      name: { kind: "Name", value: "addVerificationCode" },
      variableDefinitions: [
        {
          kind: "VariableDefinition",
          variable: {
            kind: "Variable",
            name: { kind: "Name", value: "domain" },
          },
          type: {
            kind: "NonNullType",
            type: {
              kind: "NamedType",
              name: { kind: "Name", value: "String" },
            },
          },
        },
        {
          kind: "VariableDefinition",
          variable: {
            kind: "Variable",
            name: { kind: "Name", value: "dnsProvider" },
          },
          type: {
            kind: "NonNullType",
            type: {
              kind: "NamedType",
              name: { kind: "Name", value: "DnsProvider" },
            },
          },
        },
        {
          kind: "VariableDefinition",
          variable: { kind: "Variable", name: { kind: "Name", value: "auth" } },
          type: {
            kind: "NonNullType",
            type: {
              kind: "NamedType",
              name: { kind: "Name", value: "String" },
            },
          },
        },
      ],
      selectionSet: {
        kind: "SelectionSet",
        selections: [
          {
            kind: "Field",
            name: { kind: "Name", value: "addVerificationTxt" },
            arguments: [
              {
                kind: "Argument",
                name: { kind: "Name", value: "domain" },
                value: {
                  kind: "Variable",
                  name: { kind: "Name", value: "domain" },
                },
              },
              {
                kind: "Argument",
                name: { kind: "Name", value: "dnsAuth" },
                value: {
                  kind: "Variable",
                  name: { kind: "Name", value: "auth" },
                },
              },
              {
                kind: "Argument",
                name: { kind: "Name", value: "dnsProvider" },
                value: {
                  kind: "Variable",
                  name: { kind: "Name", value: "dnsProvider" },
                },
              },
            ],
          },
        ],
      },
    },
  ],
} as unknown as DocumentNode<
  AddVerificationCodeMutation,
  AddVerificationCodeMutationVariables
>;
export const GetBasicNetworkInfoDocument = {
  kind: "Document",
  definitions: [
    {
      kind: "OperationDefinition",
      operation: "query",
      name: { kind: "Name", value: "getBasicNetworkInfo" },
      selectionSet: {
        kind: "SelectionSet",
        selections: [
          {
            kind: "Field",
            name: { kind: "Name", value: "network" },
            selectionSet: {
              kind: "SelectionSet",
              selections: [
                { kind: "Field", name: { kind: "Name", value: "ip" } },
              ],
            },
          },
        ],
      },
    },
  ],
} as unknown as DocumentNode<
  GetBasicNetworkInfoQuery,
  GetBasicNetworkInfoQueryVariables
>;
export const GetUserNirvatiMeInfoDocument = {
  kind: "Document",
  definitions: [
    {
      kind: "OperationDefinition",
      operation: "query",
      name: { kind: "Name", value: "getUserNirvatiMeInfo" },
      selectionSet: {
        kind: "SelectionSet",
        selections: [
          {
            kind: "Field",
            name: { kind: "Name", value: "network" },
            selectionSet: {
              kind: "SelectionSet",
              selections: [
                { kind: "Field", name: { kind: "Name", value: "ip" } },
              ],
            },
          },
          {
            kind: "Field",
            name: { kind: "Name", value: "users" },
            selectionSet: {
              kind: "SelectionSet",
              selections: [
                {
                  kind: "Field",
                  name: { kind: "Name", value: "me" },
                  selectionSet: {
                    kind: "SelectionSet",
                    selections: [
                      {
                        kind: "Field",
                        alias: { kind: "Name", value: "nirvatiMeUsername" },
                        name: { kind: "Name", value: "entropy" },
                        arguments: [
                          {
                            kind: "Argument",
                            name: { kind: "Name", value: "identifier" },
                            value: {
                              kind: "StringValue",
                              value: "nirvati.me-username",
                              block: false,
                            },
                          },
                        ],
                      },
                      {
                        kind: "Field",
                        alias: { kind: "Name", value: "nirvatiMePassword" },
                        name: { kind: "Name", value: "entropy" },
                        arguments: [
                          {
                            kind: "Argument",
                            name: { kind: "Name", value: "identifier" },
                            value: {
                              kind: "StringValue",
                              value: "nirvati.me-password",
                              block: false,
                            },
                          },
                        ],
                      },
                    ],
                  },
                },
              ],
            },
          },
        ],
      },
    },
  ],
} as unknown as DocumentNode<
  GetUserNirvatiMeInfoQuery,
  GetUserNirvatiMeInfoQueryVariables
>;
export const GetNirvatiMeAccountDocument = {
  kind: "Document",
  definitions: [
    {
      kind: "OperationDefinition",
      operation: "query",
      name: { kind: "Name", value: "getNirvatiMeAccount" },
      selectionSet: {
        kind: "SelectionSet",
        selections: [
          {
            kind: "Field",
            name: { kind: "Name", value: "users" },
            selectionSet: {
              kind: "SelectionSet",
              selections: [
                {
                  kind: "Field",
                  name: { kind: "Name", value: "me" },
                  selectionSet: {
                    kind: "SelectionSet",
                    selections: [
                      {
                        kind: "Field",
                        name: { kind: "Name", value: "nirvatiMe" },
                        selectionSet: {
                          kind: "SelectionSet",
                          selections: [
                            {
                              kind: "Field",
                              name: { kind: "Name", value: "name" },
                            },
                          ],
                        },
                      },
                      {
                        kind: "Field",
                        name: { kind: "Name", value: "acmeProviders" },
                      },
                    ],
                  },
                },
              ],
            },
          },
        ],
      },
    },
  ],
} as unknown as DocumentNode<
  GetNirvatiMeAccountQuery,
  GetNirvatiMeAccountQueryVariables
>;
export const FinishSetupDocument = {
  kind: "Document",
  definitions: [
    {
      kind: "OperationDefinition",
      operation: "mutation",
      name: { kind: "Name", value: "finishSetup" },
      selectionSet: {
        kind: "SelectionSet",
        selections: [
          { kind: "Field", name: { kind: "Name", value: "finishSetup" } },
        ],
      },
    },
  ],
} as unknown as DocumentNode<FinishSetupMutation, FinishSetupMutationVariables>;
export const GetMyInitialDetailsDocument = {
  kind: "Document",
  definitions: [
    {
      kind: "OperationDefinition",
      operation: "query",
      name: { kind: "Name", value: "getMyInitialDetails" },
      selectionSet: {
        kind: "SelectionSet",
        selections: [
          {
            kind: "Field",
            name: { kind: "Name", value: "users" },
            selectionSet: {
              kind: "SelectionSet",
              selections: [
                {
                  kind: "Field",
                  name: { kind: "Name", value: "me" },
                  selectionSet: {
                    kind: "SelectionSet",
                    selections: [
                      {
                        kind: "Field",
                        name: { kind: "Name", value: "username" },
                      },
                      { kind: "Field", name: { kind: "Name", value: "name" } },
                      { kind: "Field", name: { kind: "Name", value: "email" } },
                    ],
                  },
                },
              ],
            },
          },
        ],
      },
    },
  ],
} as unknown as DocumentNode<
  GetMyInitialDetailsQuery,
  GetMyInitialDetailsQueryVariables
>;
export const UpdateMyAccountDocument = {
  kind: "Document",
  definitions: [
    {
      kind: "OperationDefinition",
      operation: "mutation",
      name: { kind: "Name", value: "updateMyAccount" },
      variableDefinitions: [
        {
          kind: "VariableDefinition",
          variable: {
            kind: "Variable",
            name: { kind: "Name", value: "username" },
          },
          type: {
            kind: "NonNullType",
            type: {
              kind: "NamedType",
              name: { kind: "Name", value: "String" },
            },
          },
        },
        {
          kind: "VariableDefinition",
          variable: { kind: "Variable", name: { kind: "Name", value: "name" } },
          type: {
            kind: "NonNullType",
            type: {
              kind: "NamedType",
              name: { kind: "Name", value: "String" },
            },
          },
        },
        {
          kind: "VariableDefinition",
          variable: {
            kind: "Variable",
            name: { kind: "Name", value: "password" },
          },
          type: {
            kind: "NonNullType",
            type: {
              kind: "NamedType",
              name: { kind: "Name", value: "String" },
            },
          },
        },
        {
          kind: "VariableDefinition",
          variable: {
            kind: "Variable",
            name: { kind: "Name", value: "email" },
          },
          type: { kind: "NamedType", name: { kind: "Name", value: "String" } },
        },
        {
          kind: "VariableDefinition",
          variable: {
            kind: "Variable",
            name: { kind: "Name", value: "isPublicName" },
          },
          type: { kind: "NamedType", name: { kind: "Name", value: "Boolean" } },
        },
      ],
      selectionSet: {
        kind: "SelectionSet",
        selections: [
          {
            kind: "Field",
            name: { kind: "Name", value: "updateUser" },
            arguments: [
              {
                kind: "Argument",
                name: { kind: "Name", value: "username" },
                value: {
                  kind: "Variable",
                  name: { kind: "Name", value: "username" },
                },
              },
              {
                kind: "Argument",
                name: { kind: "Name", value: "name" },
                value: {
                  kind: "Variable",
                  name: { kind: "Name", value: "name" },
                },
              },
              {
                kind: "Argument",
                name: { kind: "Name", value: "password" },
                value: {
                  kind: "Variable",
                  name: { kind: "Name", value: "password" },
                },
              },
              {
                kind: "Argument",
                name: { kind: "Name", value: "email" },
                value: {
                  kind: "Variable",
                  name: { kind: "Name", value: "email" },
                },
              },
              {
                kind: "Argument",
                name: { kind: "Name", value: "showOnLoginScreen" },
                value: {
                  kind: "Variable",
                  name: { kind: "Name", value: "isPublicName" },
                },
              },
            ],
          },
        ],
      },
    },
  ],
} as unknown as DocumentNode<
  UpdateMyAccountMutation,
  UpdateMyAccountMutationVariables
>;
export const DoSetupDocument = {
  kind: "Document",
  definitions: [
    {
      kind: "OperationDefinition",
      operation: "mutation",
      name: { kind: "Name", value: "doSetup" },
      variableDefinitions: [
        {
          kind: "VariableDefinition",
          variable: {
            kind: "Variable",
            name: { kind: "Name", value: "username" },
          },
          type: {
            kind: "NonNullType",
            type: {
              kind: "NamedType",
              name: { kind: "Name", value: "String" },
            },
          },
        },
        {
          kind: "VariableDefinition",
          variable: { kind: "Variable", name: { kind: "Name", value: "name" } },
          type: {
            kind: "NonNullType",
            type: {
              kind: "NamedType",
              name: { kind: "Name", value: "String" },
            },
          },
        },
        {
          kind: "VariableDefinition",
          variable: {
            kind: "Variable",
            name: { kind: "Name", value: "password" },
          },
          type: {
            kind: "NonNullType",
            type: {
              kind: "NamedType",
              name: { kind: "Name", value: "String" },
            },
          },
        },
        {
          kind: "VariableDefinition",
          variable: {
            kind: "Variable",
            name: { kind: "Name", value: "email" },
          },
          type: { kind: "NamedType", name: { kind: "Name", value: "String" } },
        },
        {
          kind: "VariableDefinition",
          variable: {
            kind: "Variable",
            name: { kind: "Name", value: "isPublicName" },
          },
          type: {
            kind: "NonNullType",
            type: {
              kind: "NamedType",
              name: { kind: "Name", value: "Boolean" },
            },
          },
        },
      ],
      selectionSet: {
        kind: "SelectionSet",
        selections: [
          {
            kind: "Field",
            name: { kind: "Name", value: "createInitialUser" },
            arguments: [
              {
                kind: "Argument",
                name: { kind: "Name", value: "username" },
                value: {
                  kind: "Variable",
                  name: { kind: "Name", value: "username" },
                },
              },
              {
                kind: "Argument",
                name: { kind: "Name", value: "name" },
                value: {
                  kind: "Variable",
                  name: { kind: "Name", value: "name" },
                },
              },
              {
                kind: "Argument",
                name: { kind: "Name", value: "password" },
                value: {
                  kind: "Variable",
                  name: { kind: "Name", value: "password" },
                },
              },
              {
                kind: "Argument",
                name: { kind: "Name", value: "email" },
                value: {
                  kind: "Variable",
                  name: { kind: "Name", value: "email" },
                },
              },
              {
                kind: "Argument",
                name: { kind: "Name", value: "isPublicName" },
                value: {
                  kind: "Variable",
                  name: { kind: "Name", value: "isPublicName" },
                },
              },
            ],
          },
        ],
      },
    },
  ],
} as unknown as DocumentNode<DoSetupMutation, DoSetupMutationVariables>;
export const FinalizeSetupDocument = {
  kind: "Document",
  definitions: [
    {
      kind: "OperationDefinition",
      operation: "mutation",
      name: { kind: "Name", value: "finalizeSetup" },
      selectionSet: {
        kind: "SelectionSet",
        selections: [
          {
            kind: "Field",
            name: { kind: "Name", value: "installRequiredSystemApps" },
          },
        ],
      },
    },
  ],
} as unknown as DocumentNode<
  FinalizeSetupMutation,
  FinalizeSetupMutationVariables
>;
export const CustomDomainInfoDocument = {
  kind: "Document",
  definitions: [
    {
      kind: "OperationDefinition",
      operation: "query",
      name: { kind: "Name", value: "customDomainInfo" },
      selectionSet: {
        kind: "SelectionSet",
        selections: [
          {
            kind: "Field",
            name: { kind: "Name", value: "users" },
            selectionSet: {
              kind: "SelectionSet",
              selections: [
                {
                  kind: "Field",
                  name: { kind: "Name", value: "me" },
                  selectionSet: {
                    kind: "SelectionSet",
                    selections: [
                      {
                        kind: "Field",
                        name: { kind: "Name", value: "acmeProviders" },
                      },
                    ],
                  },
                },
              ],
            },
          },
          {
            kind: "Field",
            name: { kind: "Name", value: "network" },
            selectionSet: {
              kind: "SelectionSet",
              selections: [
                { kind: "Field", name: { kind: "Name", value: "ip" } },
                {
                  kind: "Field",
                  name: { kind: "Name", value: "tailscaleInfo" },
                  selectionSet: {
                    kind: "SelectionSet",
                    selections: [
                      { kind: "Field", name: { kind: "Name", value: "ip" } },
                    ],
                  },
                },
              ],
            },
          },
        ],
      },
    },
  ],
} as unknown as DocumentNode<
  CustomDomainInfoQuery,
  CustomDomainInfoQueryVariables
>;
export const GetNetworkInfoDocument = {
  kind: "Document",
  definitions: [
    {
      kind: "OperationDefinition",
      operation: "query",
      name: { kind: "Name", value: "getNetworkInfo" },
      selectionSet: {
        kind: "SelectionSet",
        selections: [
          {
            kind: "Field",
            name: { kind: "Name", value: "network" },
            selectionSet: {
              kind: "SelectionSet",
              selections: [
                { kind: "Field", name: { kind: "Name", value: "ip" } },
                {
                  kind: "Field",
                  name: { kind: "Name", value: "tailscaleInfo" },
                  selectionSet: {
                    kind: "SelectionSet",
                    selections: [
                      { kind: "Field", name: { kind: "Name", value: "ip" } },
                    ],
                  },
                },
              ],
            },
          },
        ],
      },
    },
  ],
} as unknown as DocumentNode<GetNetworkInfoQuery, GetNetworkInfoQueryVariables>;
export const GetNirvatiMeInfoDocument = {
  kind: "Document",
  definitions: [
    {
      kind: "OperationDefinition",
      operation: "query",
      name: { kind: "Name", value: "getNirvatiMeInfo" },
      selectionSet: {
        kind: "SelectionSet",
        selections: [
          {
            kind: "Field",
            name: { kind: "Name", value: "network" },
            selectionSet: {
              kind: "SelectionSet",
              selections: [
                { kind: "Field", name: { kind: "Name", value: "ip" } },
                {
                  kind: "Field",
                  name: { kind: "Name", value: "tailscaleInfo" },
                  selectionSet: {
                    kind: "SelectionSet",
                    selections: [
                      { kind: "Field", name: { kind: "Name", value: "ip" } },
                    ],
                  },
                },
              ],
            },
          },
          {
            kind: "Field",
            name: { kind: "Name", value: "users" },
            selectionSet: {
              kind: "SelectionSet",
              selections: [
                {
                  kind: "Field",
                  name: { kind: "Name", value: "me" },
                  selectionSet: {
                    kind: "SelectionSet",
                    selections: [
                      {
                        kind: "Field",
                        alias: { kind: "Name", value: "nirvatiMeUsername" },
                        name: { kind: "Name", value: "entropy" },
                        arguments: [
                          {
                            kind: "Argument",
                            name: { kind: "Name", value: "identifier" },
                            value: {
                              kind: "StringValue",
                              value: "nirvati.me-username",
                              block: false,
                            },
                          },
                        ],
                      },
                      {
                        kind: "Field",
                        alias: { kind: "Name", value: "nirvatiMePassword" },
                        name: { kind: "Name", value: "entropy" },
                        arguments: [
                          {
                            kind: "Argument",
                            name: { kind: "Name", value: "identifier" },
                            value: {
                              kind: "StringValue",
                              value: "nirvati.me-password",
                              block: false,
                            },
                          },
                        ],
                      },
                    ],
                  },
                },
              ],
            },
          },
        ],
      },
    },
  ],
} as unknown as DocumentNode<
  GetNirvatiMeInfoQuery,
  GetNirvatiMeInfoQueryVariables
>;
export const InstallTelemetryDocument = {
  kind: "Document",
  definitions: [
    {
      kind: "OperationDefinition",
      operation: "mutation",
      name: { kind: "Name", value: "installTelemetry" },
      selectionSet: {
        kind: "SelectionSet",
        selections: [
          {
            kind: "Field",
            name: { kind: "Name", value: "installApp" },
            arguments: [
              {
                kind: "Argument",
                name: { kind: "Name", value: "appId" },
                value: {
                  kind: "StringValue",
                  value: "nirvati-telemetry",
                  block: false,
                },
              },
              {
                kind: "Argument",
                name: { kind: "Name", value: "settings" },
                value: { kind: "ObjectValue", fields: [] },
              },
            ],
          },
        ],
      },
    },
  ],
} as unknown as DocumentNode<
  InstallTelemetryMutation,
  InstallTelemetryMutationVariables
>;
export const GetDomainReadyStateDocument = {
  kind: "Document",
  definitions: [
    {
      kind: "OperationDefinition",
      operation: "query",
      name: { kind: "Name", value: "getDomainReadyState" },
      selectionSet: {
        kind: "SelectionSet",
        selections: [
          {
            kind: "Field",
            name: { kind: "Name", value: "users" },
            selectionSet: {
              kind: "SelectionSet",
              selections: [
                {
                  kind: "Field",
                  name: { kind: "Name", value: "me" },
                  selectionSet: {
                    kind: "SelectionSet",
                    selections: [
                      {
                        kind: "Field",
                        name: { kind: "Name", value: "app" },
                        arguments: [
                          {
                            kind: "Argument",
                            name: { kind: "Name", value: "appId" },
                            value: {
                              kind: "StringValue",
                              value: "nirvati",
                              block: false,
                            },
                          },
                        ],
                        selectionSet: {
                          kind: "SelectionSet",
                          selections: [
                            {
                              kind: "Field",
                              name: { kind: "Name", value: "domains" },
                              selectionSet: {
                                kind: "SelectionSet",
                                selections: [
                                  {
                                    kind: "Field",
                                    name: { kind: "Name", value: "name" },
                                  },
                                  {
                                    kind: "Field",
                                    name: { kind: "Name", value: "ready" },
                                  },
                                ],
                              },
                            },
                          ],
                        },
                      },
                    ],
                  },
                },
              ],
            },
          },
        ],
      },
    },
  ],
} as unknown as DocumentNode<
  GetDomainReadyStateQuery,
  GetDomainReadyStateQueryVariables
>;
export const GetIsCitadelDocument = {
  kind: "Document",
  definitions: [
    {
      kind: "OperationDefinition",
      operation: "query",
      name: { kind: "Name", value: "getIsCitadel" },
      selectionSet: {
        kind: "SelectionSet",
        selections: [
          {
            kind: "Field",
            name: { kind: "Name", value: "info" },
            selectionSet: {
              kind: "SelectionSet",
              selections: [
                {
                  kind: "Field",
                  name: { kind: "Name", value: "isCitadelCompat" },
                },
              ],
            },
          },
        ],
      },
    },
  ],
} as unknown as DocumentNode<GetIsCitadelQuery, GetIsCitadelQueryVariables>;
export const TailscaleUpDocument = {
  kind: "Document",
  definitions: [
    {
      kind: "OperationDefinition",
      operation: "mutation",
      name: { kind: "Name", value: "tailscaleUp" },
      selectionSet: {
        kind: "SelectionSet",
        selections: [
          { kind: "Field", name: { kind: "Name", value: "startTailscale" } },
        ],
      },
    },
  ],
} as unknown as DocumentNode<TailscaleUpMutation, TailscaleUpMutationVariables>;
export const GetTailscalePageInfoDocument = {
  kind: "Document",
  definitions: [
    {
      kind: "OperationDefinition",
      operation: "query",
      name: { kind: "Name", value: "getTailscalePageInfo" },
      selectionSet: {
        kind: "SelectionSet",
        selections: [
          {
            kind: "Field",
            name: { kind: "Name", value: "network" },
            selectionSet: {
              kind: "SelectionSet",
              selections: [
                {
                  kind: "Field",
                  name: { kind: "Name", value: "tailscaleInfo" },
                  selectionSet: {
                    kind: "SelectionSet",
                    selections: [
                      {
                        kind: "Field",
                        name: { kind: "Name", value: "authUrl" },
                      },
                      { kind: "Field", name: { kind: "Name", value: "ip" } },
                    ],
                  },
                },
              ],
            },
          },
        ],
      },
    },
  ],
} as unknown as DocumentNode<
  GetTailscalePageInfoQuery,
  GetTailscalePageInfoQueryVariables
>;
export const StartTailscaleLoginDocument = {
  kind: "Document",
  definitions: [
    {
      kind: "OperationDefinition",
      operation: "mutation",
      name: { kind: "Name", value: "startTailscaleLogin" },
      selectionSet: {
        kind: "SelectionSet",
        selections: [
          {
            kind: "Field",
            name: { kind: "Name", value: "startTailscaleLogin" },
          },
        ],
      },
    },
  ],
} as unknown as DocumentNode<
  StartTailscaleLoginMutation,
  StartTailscaleLoginMutationVariables
>;
export const GetMyDomainsDocument = {
  kind: "Document",
  definitions: [
    {
      kind: "OperationDefinition",
      operation: "query",
      name: { kind: "Name", value: "getMyDomains" },
      selectionSet: {
        kind: "SelectionSet",
        selections: [
          {
            kind: "Field",
            name: { kind: "Name", value: "users" },
            selectionSet: {
              kind: "SelectionSet",
              selections: [
                {
                  kind: "Field",
                  name: { kind: "Name", value: "me" },
                  selectionSet: {
                    kind: "SelectionSet",
                    selections: [
                      {
                        kind: "Field",
                        name: { kind: "Name", value: "app" },
                        arguments: [
                          {
                            kind: "Argument",
                            name: { kind: "Name", value: "appId" },
                            value: {
                              kind: "StringValue",
                              value: "nirvati",
                              block: false,
                            },
                          },
                        ],
                        selectionSet: {
                          kind: "SelectionSet",
                          selections: [
                            {
                              kind: "Field",
                              name: { kind: "Name", value: "domains" },
                              selectionSet: {
                                kind: "SelectionSet",
                                selections: [
                                  {
                                    kind: "Field",
                                    name: { kind: "Name", value: "name" },
                                  },
                                ],
                              },
                            },
                          ],
                        },
                      },
                    ],
                  },
                },
              ],
            },
          },
        ],
      },
    },
  ],
} as unknown as DocumentNode<GetMyDomainsQuery, GetMyDomainsQueryVariables>;
export const StorageInfoDocument = {
  kind: "Document",
  definitions: [
    {
      kind: "OperationDefinition",
      operation: "query",
      name: { kind: "Name", value: "storageInfo" },
      selectionSet: {
        kind: "SelectionSet",
        selections: [
          {
            kind: "Field",
            name: { kind: "Name", value: "users" },
            selectionSet: {
              kind: "SelectionSet",
              selections: [
                {
                  kind: "Field",
                  name: { kind: "Name", value: "me" },
                  selectionSet: {
                    kind: "SelectionSet",
                    selections: [
                      {
                        kind: "Field",
                        name: { kind: "Name", value: "apps" },
                        arguments: [
                          {
                            kind: "Argument",
                            name: { kind: "Name", value: "installedOnly" },
                            value: { kind: "BooleanValue", value: true },
                          },
                        ],
                        selectionSet: {
                          kind: "SelectionSet",
                          selections: [
                            {
                              kind: "Field",
                              name: { kind: "Name", value: "name" },
                            },
                            {
                              kind: "Field",
                              name: { kind: "Name", value: "icon" },
                            },
                            {
                              kind: "Field",
                              name: { kind: "Name", value: "id" },
                            },
                            {
                              kind: "Field",
                              name: { kind: "Name", value: "runningVolumes" },
                              selectionSet: {
                                kind: "SelectionSet",
                                selections: [
                                  {
                                    kind: "Field",
                                    name: { kind: "Name", value: "id" },
                                  },
                                  {
                                    kind: "Field",
                                    name: { kind: "Name", value: "size" },
                                  },
                                  {
                                    kind: "Field",
                                    name: { kind: "Name", value: "actualSize" },
                                  },
                                  {
                                    kind: "Field",
                                    name: {
                                      kind: "Name",
                                      value: "replicaCount",
                                    },
                                  },
                                  {
                                    kind: "Field",
                                    name: {
                                      kind: "Name",
                                      value: "humanReadableId",
                                    },
                                  },
                                  {
                                    kind: "Field",
                                    name: {
                                      kind: "Name",
                                      value: "volumeDefinition",
                                    },
                                    selectionSet: {
                                      kind: "SelectionSet",
                                      selections: [
                                        {
                                          kind: "Field",
                                          name: { kind: "Name", value: "name" },
                                        },
                                        {
                                          kind: "Field",
                                          name: {
                                            kind: "Name",
                                            value: "description",
                                          },
                                        },
                                      ],
                                    },
                                  },
                                ],
                              },
                            },
                          ],
                        },
                      },
                    ],
                  },
                },
              ],
            },
          },
        ],
      },
    },
  ],
} as unknown as DocumentNode<StorageInfoQuery, StorageInfoQueryVariables>;
export const GetAppVolumeDocument = {
  kind: "Document",
  definitions: [
    {
      kind: "OperationDefinition",
      operation: "query",
      name: { kind: "Name", value: "getAppVolume" },
      variableDefinitions: [
        {
          kind: "VariableDefinition",
          variable: {
            kind: "Variable",
            name: { kind: "Name", value: "appId" },
          },
          type: {
            kind: "NonNullType",
            type: {
              kind: "NamedType",
              name: { kind: "Name", value: "String" },
            },
          },
        },
      ],
      selectionSet: {
        kind: "SelectionSet",
        selections: [
          {
            kind: "Field",
            name: { kind: "Name", value: "users" },
            selectionSet: {
              kind: "SelectionSet",
              selections: [
                {
                  kind: "Field",
                  name: { kind: "Name", value: "me" },
                  selectionSet: {
                    kind: "SelectionSet",
                    selections: [
                      {
                        kind: "Field",
                        name: { kind: "Name", value: "app" },
                        arguments: [
                          {
                            kind: "Argument",
                            name: { kind: "Name", value: "appId" },
                            value: {
                              kind: "Variable",
                              name: { kind: "Name", value: "appId" },
                            },
                          },
                        ],
                        selectionSet: {
                          kind: "SelectionSet",
                          selections: [
                            {
                              kind: "Field",
                              name: { kind: "Name", value: "name" },
                            },
                            {
                              kind: "Field",
                              name: { kind: "Name", value: "icon" },
                            },
                            {
                              kind: "Field",
                              name: { kind: "Name", value: "runningVolumes" },
                              selectionSet: {
                                kind: "SelectionSet",
                                selections: [
                                  {
                                    kind: "Field",
                                    name: {
                                      kind: "Name",
                                      value: "replicaCount",
                                    },
                                  },
                                  {
                                    kind: "Field",
                                    name: { kind: "Name", value: "actualSize" },
                                  },
                                  {
                                    kind: "Field",
                                    name: { kind: "Name", value: "size" },
                                  },
                                  {
                                    kind: "Field",
                                    name: {
                                      kind: "Name",
                                      value: "humanReadableId",
                                    },
                                  },
                                  {
                                    kind: "Field",
                                    name: {
                                      kind: "Name",
                                      value: "volumeDefinition",
                                    },
                                    selectionSet: {
                                      kind: "SelectionSet",
                                      selections: [
                                        {
                                          kind: "Field",
                                          name: { kind: "Name", value: "name" },
                                        },
                                        {
                                          kind: "Field",
                                          name: {
                                            kind: "Name",
                                            value: "description",
                                          },
                                        },
                                      ],
                                    },
                                  },
                                ],
                              },
                            },
                          ],
                        },
                      },
                    ],
                  },
                },
              ],
            },
          },
        ],
      },
    },
  ],
} as unknown as DocumentNode<GetAppVolumeQuery, GetAppVolumeQueryVariables>;
export const UpdateReplicaCountDocument = {
  kind: "Document",
  definitions: [
    {
      kind: "OperationDefinition",
      operation: "mutation",
      name: { kind: "Name", value: "updateReplicaCount" },
      variableDefinitions: [
        {
          kind: "VariableDefinition",
          variable: {
            kind: "Variable",
            name: { kind: "Name", value: "appId" },
          },
          type: {
            kind: "NonNullType",
            type: {
              kind: "NamedType",
              name: { kind: "Name", value: "String" },
            },
          },
        },
        {
          kind: "VariableDefinition",
          variable: {
            kind: "Variable",
            name: { kind: "Name", value: "volumeId" },
          },
          type: {
            kind: "NonNullType",
            type: {
              kind: "NamedType",
              name: { kind: "Name", value: "String" },
            },
          },
        },
        {
          kind: "VariableDefinition",
          variable: {
            kind: "Variable",
            name: { kind: "Name", value: "replicaCount" },
          },
          type: {
            kind: "NonNullType",
            type: { kind: "NamedType", name: { kind: "Name", value: "Int" } },
          },
        },
      ],
      selectionSet: {
        kind: "SelectionSet",
        selections: [
          {
            kind: "Field",
            name: { kind: "Name", value: "setReplicaCount" },
            arguments: [
              {
                kind: "Argument",
                name: { kind: "Name", value: "appId" },
                value: {
                  kind: "Variable",
                  name: { kind: "Name", value: "appId" },
                },
              },
              {
                kind: "Argument",
                name: { kind: "Name", value: "volumeId" },
                value: {
                  kind: "Variable",
                  name: { kind: "Name", value: "volumeId" },
                },
              },
              {
                kind: "Argument",
                name: { kind: "Name", value: "replicas" },
                value: {
                  kind: "Variable",
                  name: { kind: "Name", value: "replicaCount" },
                },
              },
            ],
          },
        ],
      },
    },
  ],
} as unknown as DocumentNode<
  UpdateReplicaCountMutation,
  UpdateReplicaCountMutationVariables
>;
export const UpdateSizeDocument = {
  kind: "Document",
  definitions: [
    {
      kind: "OperationDefinition",
      operation: "mutation",
      name: { kind: "Name", value: "updateSize" },
      variableDefinitions: [
        {
          kind: "VariableDefinition",
          variable: {
            kind: "Variable",
            name: { kind: "Name", value: "appId" },
          },
          type: {
            kind: "NonNullType",
            type: {
              kind: "NamedType",
              name: { kind: "Name", value: "String" },
            },
          },
        },
        {
          kind: "VariableDefinition",
          variable: {
            kind: "Variable",
            name: { kind: "Name", value: "volumeId" },
          },
          type: {
            kind: "NonNullType",
            type: {
              kind: "NamedType",
              name: { kind: "Name", value: "String" },
            },
          },
        },
        {
          kind: "VariableDefinition",
          variable: { kind: "Variable", name: { kind: "Name", value: "size" } },
          type: {
            kind: "NonNullType",
            type: { kind: "NamedType", name: { kind: "Name", value: "Int" } },
          },
        },
      ],
      selectionSet: {
        kind: "SelectionSet",
        selections: [
          {
            kind: "Field",
            name: { kind: "Name", value: "resizeVolume" },
            arguments: [
              {
                kind: "Argument",
                name: { kind: "Name", value: "appId" },
                value: {
                  kind: "Variable",
                  name: { kind: "Name", value: "appId" },
                },
              },
              {
                kind: "Argument",
                name: { kind: "Name", value: "volumeId" },
                value: {
                  kind: "Variable",
                  name: { kind: "Name", value: "volumeId" },
                },
              },
              {
                kind: "Argument",
                name: { kind: "Name", value: "size" },
                value: {
                  kind: "Variable",
                  name: { kind: "Name", value: "size" },
                },
              },
            ],
          },
        ],
      },
    },
  ],
} as unknown as DocumentNode<UpdateSizeMutation, UpdateSizeMutationVariables>;
export const GetAvailableUpdatesDocument = {
  kind: "Document",
  definitions: [
    {
      kind: "OperationDefinition",
      operation: "query",
      name: { kind: "Name", value: "getAvailableUpdates" },
      selectionSet: {
        kind: "SelectionSet",
        selections: [
          {
            kind: "Field",
            name: { kind: "Name", value: "users" },
            selectionSet: {
              kind: "SelectionSet",
              selections: [
                {
                  kind: "Field",
                  name: { kind: "Name", value: "me" },
                  selectionSet: {
                    kind: "SelectionSet",
                    selections: [
                      {
                        kind: "Field",
                        name: { kind: "Name", value: "permissions" },
                      },
                      {
                        kind: "Field",
                        name: { kind: "Name", value: "apps" },
                        arguments: [
                          {
                            kind: "Argument",
                            name: { kind: "Name", value: "installedOnly" },
                            value: { kind: "BooleanValue", value: true },
                          },
                        ],
                        selectionSet: {
                          kind: "SelectionSet",
                          selections: [
                            {
                              kind: "Field",
                              name: { kind: "Name", value: "id" },
                            },
                            {
                              kind: "Field",
                              name: { kind: "Name", value: "name" },
                            },
                            {
                              kind: "Field",
                              name: { kind: "Name", value: "icon" },
                            },
                            {
                              kind: "Field",
                              name: { kind: "Name", value: "version" },
                            },
                            {
                              kind: "Field",
                              name: { kind: "Name", value: "latestVersion" },
                              selectionSet: {
                                kind: "SelectionSet",
                                selections: [
                                  {
                                    kind: "Field",
                                    name: {
                                      kind: "Name",
                                      value: "latestVersion",
                                    },
                                  },
                                ],
                              },
                            },
                          ],
                        },
                      },
                    ],
                  },
                },
              ],
            },
          },
        ],
      },
    },
  ],
} as unknown as DocumentNode<
  GetAvailableUpdatesQuery,
  GetAvailableUpdatesQueryVariables
>;
export const GetKubeVersionDocument = {
  kind: "Document",
  definitions: [
    {
      kind: "OperationDefinition",
      operation: "query",
      name: { kind: "Name", value: "getKubeVersion" },
      selectionSet: {
        kind: "SelectionSet",
        selections: [
          {
            kind: "Field",
            name: { kind: "Name", value: "upgrades" },
            selectionSet: {
              kind: "SelectionSet",
              selections: [
                {
                  kind: "Field",
                  name: { kind: "Name", value: "kube" },
                  selectionSet: {
                    kind: "SelectionSet",
                    selections: [
                      {
                        kind: "Field",
                        name: { kind: "Name", value: "installedVersion" },
                      },
                    ],
                  },
                },
              ],
            },
          },
        ],
      },
    },
  ],
} as unknown as DocumentNode<GetKubeVersionQuery, GetKubeVersionQueryVariables>;
export const GetLatestKubeVersionDocument = {
  kind: "Document",
  definitions: [
    {
      kind: "OperationDefinition",
      operation: "query",
      name: { kind: "Name", value: "getLatestKubeVersion" },
      selectionSet: {
        kind: "SelectionSet",
        selections: [
          {
            kind: "Field",
            name: { kind: "Name", value: "upgrades" },
            selectionSet: {
              kind: "SelectionSet",
              selections: [
                {
                  kind: "Field",
                  name: { kind: "Name", value: "kube" },
                  selectionSet: {
                    kind: "SelectionSet",
                    selections: [
                      {
                        kind: "Field",
                        name: { kind: "Name", value: "latestVersion" },
                      },
                    ],
                  },
                },
              ],
            },
          },
        ],
      },
    },
  ],
} as unknown as DocumentNode<
  GetLatestKubeVersionQuery,
  GetLatestKubeVersionQueryVariables
>;
export const UpdateKubeDocument = {
  kind: "Document",
  definitions: [
    {
      kind: "OperationDefinition",
      operation: "mutation",
      name: { kind: "Name", value: "updateKube" },
      variableDefinitions: [
        {
          kind: "VariableDefinition",
          variable: {
            kind: "Variable",
            name: { kind: "Name", value: "version" },
          },
          type: {
            kind: "NonNullType",
            type: {
              kind: "NamedType",
              name: { kind: "Name", value: "String" },
            },
          },
        },
      ],
      selectionSet: {
        kind: "SelectionSet",
        selections: [
          {
            kind: "Field",
            name: { kind: "Name", value: "upgradeKube" },
            arguments: [
              {
                kind: "Argument",
                name: { kind: "Name", value: "targetVersion" },
                value: {
                  kind: "Variable",
                  name: { kind: "Name", value: "version" },
                },
              },
            ],
          },
        ],
      },
    },
  ],
} as unknown as DocumentNode<UpdateKubeMutation, UpdateKubeMutationVariables>;
export const UpdateAppDocument = {
  kind: "Document",
  definitions: [
    {
      kind: "OperationDefinition",
      operation: "mutation",
      name: { kind: "Name", value: "updateApp" },
      variableDefinitions: [
        {
          kind: "VariableDefinition",
          variable: { kind: "Variable", name: { kind: "Name", value: "id" } },
          type: {
            kind: "NonNullType",
            type: {
              kind: "NamedType",
              name: { kind: "Name", value: "String" },
            },
          },
        },
      ],
      selectionSet: {
        kind: "SelectionSet",
        selections: [
          {
            kind: "Field",
            name: { kind: "Name", value: "updateApp" },
            arguments: [
              {
                kind: "Argument",
                name: { kind: "Name", value: "appId" },
                value: {
                  kind: "Variable",
                  name: { kind: "Name", value: "id" },
                },
              },
            ],
          },
        ],
      },
    },
  ],
} as unknown as DocumentNode<UpdateAppMutation, UpdateAppMutationVariables>;
export const GetUsersDocument = {
  kind: "Document",
  definitions: [
    {
      kind: "OperationDefinition",
      operation: "query",
      name: { kind: "Name", value: "getUsers" },
      selectionSet: {
        kind: "SelectionSet",
        selections: [
          {
            kind: "Field",
            name: { kind: "Name", value: "users" },
            selectionSet: {
              kind: "SelectionSet",
              selections: [
                {
                  kind: "Field",
                  name: { kind: "Name", value: "users" },
                  selectionSet: {
                    kind: "SelectionSet",
                    selections: [
                      { kind: "Field", name: { kind: "Name", value: "name" } },
                      {
                        kind: "Field",
                        name: { kind: "Name", value: "username" },
                      },
                      { kind: "Field", name: { kind: "Name", value: "email" } },
                      {
                        kind: "Field",
                        name: { kind: "Name", value: "createdAt" },
                      },
                    ],
                  },
                },
              ],
            },
          },
        ],
      },
    },
  ],
} as unknown as DocumentNode<GetUsersQuery, GetUsersQueryVariables>;
export const GetJwtDocument = {
  kind: "Document",
  definitions: [
    {
      kind: "OperationDefinition",
      operation: "query",
      name: { kind: "Name", value: "getJwt" },
      selectionSet: {
        kind: "SelectionSet",
        selections: [
          {
            kind: "Field",
            name: { kind: "Name", value: "users" },
            selectionSet: {
              kind: "SelectionSet",
              selections: [
                {
                  kind: "Field",
                  name: { kind: "Name", value: "me" },
                  selectionSet: {
                    kind: "SelectionSet",
                    selections: [
                      { kind: "Field", name: { kind: "Name", value: "jwt" } },
                    ],
                  },
                },
              ],
            },
          },
        ],
      },
    },
  ],
} as unknown as DocumentNode<GetJwtQuery, GetJwtQueryVariables>;
export const AddDomainDocument = {
  kind: "Document",
  definitions: [
    {
      kind: "OperationDefinition",
      operation: "mutation",
      name: { kind: "Name", value: "addDomain" },
      variableDefinitions: [
        {
          kind: "VariableDefinition",
          variable: {
            kind: "Variable",
            name: { kind: "Name", value: "domain" },
          },
          type: {
            kind: "NonNullType",
            type: {
              kind: "NamedType",
              name: { kind: "Name", value: "String" },
            },
          },
        },
        {
          kind: "VariableDefinition",
          variable: {
            kind: "Variable",
            name: { kind: "Name", value: "dnsProvider" },
          },
          type: {
            kind: "NamedType",
            name: { kind: "Name", value: "DnsProvider" },
          },
        },
        {
          kind: "VariableDefinition",
          variable: { kind: "Variable", name: { kind: "Name", value: "auth" } },
          type: { kind: "NamedType", name: { kind: "Name", value: "String" } },
        },
      ],
      selectionSet: {
        kind: "SelectionSet",
        selections: [
          {
            kind: "Field",
            name: { kind: "Name", value: "addDomain" },
            arguments: [
              {
                kind: "Argument",
                name: { kind: "Name", value: "domain" },
                value: {
                  kind: "Variable",
                  name: { kind: "Name", value: "domain" },
                },
              },
              {
                kind: "Argument",
                name: { kind: "Name", value: "dnsProvider" },
                value: {
                  kind: "Variable",
                  name: { kind: "Name", value: "dnsProvider" },
                },
              },
              {
                kind: "Argument",
                name: { kind: "Name", value: "dnsAuth" },
                value: {
                  kind: "Variable",
                  name: { kind: "Name", value: "auth" },
                },
              },
            ],
          },
        ],
      },
    },
  ],
} as unknown as DocumentNode<AddDomainMutation, AddDomainMutationVariables>;
export const RemoveDomainDocument = {
  kind: "Document",
  definitions: [
    {
      kind: "OperationDefinition",
      operation: "mutation",
      name: { kind: "Name", value: "removeDomain" },
      variableDefinitions: [
        {
          kind: "VariableDefinition",
          variable: {
            kind: "Variable",
            name: { kind: "Name", value: "domain" },
          },
          type: {
            kind: "NonNullType",
            type: {
              kind: "NamedType",
              name: { kind: "Name", value: "String" },
            },
          },
        },
      ],
      selectionSet: {
        kind: "SelectionSet",
        selections: [
          {
            kind: "Field",
            name: { kind: "Name", value: "removeDomain" },
            arguments: [
              {
                kind: "Argument",
                name: { kind: "Name", value: "domain" },
                value: {
                  kind: "Variable",
                  name: { kind: "Name", value: "domain" },
                },
              },
            ],
          },
        ],
      },
    },
  ],
} as unknown as DocumentNode<
  RemoveDomainMutation,
  RemoveDomainMutationVariables
>;
export const AddAcmeProviderDocument = {
  kind: "Document",
  definitions: [
    {
      kind: "OperationDefinition",
      operation: "mutation",
      name: { kind: "Name", value: "addAcmeProvider" },
      variableDefinitions: [
        {
          kind: "VariableDefinition",
          variable: {
            kind: "Variable",
            name: { kind: "Name", value: "provider" },
          },
          type: {
            kind: "NonNullType",
            type: {
              kind: "NamedType",
              name: { kind: "Name", value: "AcmeProvider" },
            },
          },
        },
        {
          kind: "VariableDefinition",
          variable: {
            kind: "Variable",
            name: { kind: "Name", value: "shareEmail" },
          },
          type: {
            kind: "NonNullType",
            type: {
              kind: "NamedType",
              name: { kind: "Name", value: "Boolean" },
            },
          },
        },
      ],
      selectionSet: {
        kind: "SelectionSet",
        selections: [
          {
            kind: "Field",
            name: { kind: "Name", value: "createAcmeIssuer" },
            arguments: [
              {
                kind: "Argument",
                name: { kind: "Name", value: "provider" },
                value: {
                  kind: "Variable",
                  name: { kind: "Name", value: "provider" },
                },
              },
              {
                kind: "Argument",
                name: { kind: "Name", value: "shareEmail" },
                value: {
                  kind: "Variable",
                  name: { kind: "Name", value: "shareEmail" },
                },
              },
            ],
          },
        ],
      },
    },
  ],
} as unknown as DocumentNode<
  AddAcmeProviderMutation,
  AddAcmeProviderMutationVariables
>;
export const AddAppDomainDocument = {
  kind: "Document",
  definitions: [
    {
      kind: "OperationDefinition",
      operation: "mutation",
      name: { kind: "Name", value: "addAppDomain" },
      variableDefinitions: [
        {
          kind: "VariableDefinition",
          variable: { kind: "Variable", name: { kind: "Name", value: "app" } },
          type: {
            kind: "NonNullType",
            type: {
              kind: "NamedType",
              name: { kind: "Name", value: "String" },
            },
          },
        },
        {
          kind: "VariableDefinition",
          variable: {
            kind: "Variable",
            name: { kind: "Name", value: "domain" },
          },
          type: {
            kind: "NonNullType",
            type: {
              kind: "NamedType",
              name: { kind: "Name", value: "String" },
            },
          },
        },
        {
          kind: "VariableDefinition",
          variable: {
            kind: "Variable",
            name: { kind: "Name", value: "acmeProvider" },
          },
          type: {
            kind: "NonNullType",
            type: {
              kind: "NamedType",
              name: { kind: "Name", value: "AcmeProvider" },
            },
          },
        },
        {
          kind: "VariableDefinition",
          variable: {
            kind: "Variable",
            name: { kind: "Name", value: "forceHttpOnly" },
          },
          type: {
            kind: "NonNullType",
            type: {
              kind: "NamedType",
              name: { kind: "Name", value: "Boolean" },
            },
          },
        },
      ],
      selectionSet: {
        kind: "SelectionSet",
        selections: [
          {
            kind: "Field",
            name: { kind: "Name", value: "addAppDomain" },
            arguments: [
              {
                kind: "Argument",
                name: { kind: "Name", value: "app" },
                value: {
                  kind: "Variable",
                  name: { kind: "Name", value: "app" },
                },
              },
              {
                kind: "Argument",
                name: { kind: "Name", value: "domain" },
                value: {
                  kind: "Variable",
                  name: { kind: "Name", value: "domain" },
                },
              },
              {
                kind: "Argument",
                name: { kind: "Name", value: "acmeProvider" },
                value: {
                  kind: "Variable",
                  name: { kind: "Name", value: "acmeProvider" },
                },
              },
              {
                kind: "Argument",
                name: { kind: "Name", value: "forceHttpOnly" },
                value: {
                  kind: "Variable",
                  name: { kind: "Name", value: "forceHttpOnly" },
                },
              },
            ],
          },
        ],
      },
    },
  ],
} as unknown as DocumentNode<
  AddAppDomainMutation,
  AddAppDomainMutationVariables
>;
export const PerformRegenDocument = {
  kind: "Document",
  definitions: [
    {
      kind: "OperationDefinition",
      operation: "mutation",
      name: { kind: "Name", value: "performRegen" },
      selectionSet: {
        kind: "SelectionSet",
        selections: [
          { kind: "Field", name: { kind: "Name", value: "generate" } },
        ],
      },
    },
  ],
} as unknown as DocumentNode<
  PerformRegenMutation,
  PerformRegenMutationVariables
>;
export const PerformRefreshDocument = {
  kind: "Document",
  definitions: [
    {
      kind: "OperationDefinition",
      operation: "mutation",
      name: { kind: "Name", value: "performRefresh" },
      selectionSet: {
        kind: "SelectionSet",
        selections: [
          { kind: "Field", name: { kind: "Name", value: "refreshSources" } },
        ],
      },
    },
  ],
} as unknown as DocumentNode<
  PerformRefreshMutation,
  PerformRefreshMutationVariables
>;

import type { UserPermission } from "~/utils/gql/graphql";

export enum DangerLevel {
  None,
  Medium,
  High,
}

export function getDangerLevel(permission: string) {
  if (["kube-api/full", "root", "plugin/custom-resource", "network/raw", "network/host"].includes(permission)) {
    return DangerLevel.High;
  } else if (["network/cluster-egress", "plugin/runtime", "network/load-balancer"].includes(permission)) {
    return DangerLevel.Medium;
  } else {
    return DangerLevel.None;
  }
}

export function translatePerm(perm: UserPermission, t: (key: string) => string) {
  switch (perm) {
    case "MANAGE_APPS":
      return t("users.permissions.manageApps");
    case "MANAGE_DOMAINS":
      return t("users.permissions.manageDomains");
    case "MANAGE_USERS":
      return t("users.permissions.manageUsers");
    case "MANAGE_GROUPS":
      return t("users.permissions.manageGroups");
    case "MANAGE_PROXIES":
      return t("users.permissions.manageProxies");
    case "MANAGE_ISSUERS":
      return t("users.permissions.manageIssuers");
    case "CONFIGURE_NETWORK":
      return t("users.permissions.configureNetwork");
    case "MANAGE_SYS_COMPONENTS":
      return t("users.permissions.manageSysComponents");
    case "MANAGE_HARDWARE":
      return t("users.permissions.manageHardware");
    case "ESCALATE":
      return t("users.permissions.escalate");
    case "USE_NIRVATI_ME":
      return t("users.permissions.useNiratiMe");
    case "MANAGE_APP_STORES":
      return t("users.permissions.manageAppStores");
    case "MANAGE_APP_STORAGE":
      return t("users.permissions.manageAppStorage");
    case "INSTALL_ROOT_APPS":
      return t("users.permissions.installRootApps");
    case "ADD_TRUSTED_DOMAINS":
      return t("users.permissions.addTrustedDomains");
    default:
      return "Unknown permission [INTERNAL ERROR]";  // Optional: to handle unknown permissions
  }
}
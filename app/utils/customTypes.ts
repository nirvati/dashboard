export type MultiLanguageItem = Record<string, string>;

export type StringType = "password" | "email" | "url" | "ip" | "fqdn";

export type Setting =
  | {
  type: "Enum";
  name: MultiLanguageItem;
  description: MultiLanguageItem;
  values: string[];
  default?: string | null;
}
  | {
  type: "String";
  name: MultiLanguageItem;
  description: MultiLanguageItem;
  default?: string | null;
  max_len?: number | null;
  string_type?: StringType | null;
  required: boolean;
  placeholder?: MultiLanguageItem;
}
  | {
  type: "Bool";
  name: MultiLanguageItem;
  description: MultiLanguageItem;
  default: boolean;
}
  | {
  type: "Int";
  name: MultiLanguageItem;
  description: MultiLanguageItem;
  default?: number | null;
  min?: number | null;
  max?: number | null;
  step_size?: number | null;
}
  | {
  type: "Float";
  name: MultiLanguageItem;
  description: MultiLanguageItem;
  default?: number | null;
  min?: number | null;
  max?: number | null;
  step_size?: number | null;
};

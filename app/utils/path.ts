// SPDX-FileCopyrightText: 2024 The Nirvati Developers
//
// SPDX-License-Identifier: AGPL-3.0-or-later

// CHecks if an URL is relative (like /api/thing) and if it is, convert is to an absolute path based on window.location
export function relativeToAbsolute(url: string, proto?: string): string {
  if (url.startsWith("/")) {
    return `${proto || window.location.protocol}//${
      window.location.host
    }${url}`;
  }
  return url;
}

export function activeWsProto(): string {
  return window.location.protocol === "https:" ? "wss:" : "ws:";
}

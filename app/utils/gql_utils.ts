// SPDX-FileCopyrightText: 2024 The Nirvati Developers
//
// SPDX-License-Identifier: AGPL-3.0-or-later

import { DnsProvider } from "~/utils/gql/graphql";

export const addDomainMutation = graphql(`
  mutation addDomain(
    $domain: String!
    $dnsProvider: DnsProvider
    $auth: String
    #$acmeProviders: [AcmeProvider!]!
  ) {
    addDomain(
      domain: $domain
      dnsProvider: $dnsProvider
      dnsAuth: $auth
      #acmeProviders: $acmeProviders
    )
  }
`);

export const removeDomainMutation = graphql(`
  mutation removeDomain($domain: String!) {
    removeDomain(domain: $domain)
  }
`);

export function getDnsProvider(provider: string): DnsProvider | undefined {
  switch (provider) {
    case "Cloudflare™":
      return DnsProvider.Cloudflare;
    default:
      return undefined;
  }
}

export function getDnsProviderString(
  provider: DnsProvider | undefined,
): string {
  switch (provider) {
    case DnsProvider.Cloudflare:
      return "Cloudflare™";
    case DnsProvider.NirvatiMe:
      return "Nirvati.me";
    default:
      return "None";
  }
}

export const addAcmeProviderMutation = graphql(`
  mutation addAcmeProvider($provider: AcmeProvider!, $shareEmail: Boolean!) {
    createAcmeIssuer(provider: $provider, shareEmail: $shareEmail)
  }
`);

export const addAppDomainMutation = graphql(`
  mutation addAppDomain(
    $app: String!
    $domain: String!
    $acmeProvider: AcmeProvider!
    $forceHttpOnly: Boolean!
  ) {
    addAppDomain(app: $app, domain: $domain, acmeProvider: $acmeProvider, forceHttpOnly: $forceHttpOnly)
  }
`);

export const generateAppFilesMutation = graphql(`
  mutation performRegen {
    generate
  }
`);

export const refreshStoresMutation = graphql(`
  mutation performRefresh {
    refreshSources
  }
`);

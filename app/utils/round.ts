// SPDX-FileCopyrightText: 2024 The Nirvati Developers
//
// SPDX-License-Identifier: AGPL-3.0-or-later

export function round(value: number, precision: number) {
  const multiplier = Math.pow(10, precision || 0);
  return precision === 0
    ? Math.round(value * multiplier) / multiplier
    : Number(
        (Math.round(value * multiplier) / multiplier).toPrecision(precision),
      );
}

// SPDX-FileCopyrightText: 2024 The Nirvati Developers
//
// SPDX-License-Identifier: AGPL-3.0-or-later

const freshJwtQuery = graphql(`
  query getJwt {
    users {
      me {
        jwt
      }
    }
  }
`);

type useApolloType = ReturnType<typeof useApollo>;
export async function refreshJwt({ clients, onLogin }: useApolloType) {
  const { data, error } = await clients!.default.query({
    query: freshJwtQuery,
  });
  if (error) {
    throw error;
  }
  if (!data?.users?.me?.jwt) {
    throw new Error("No JWT!");
  }
  await onLogin(data.users.me.jwt);
}

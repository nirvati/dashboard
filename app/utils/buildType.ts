export enum BuildType {
  Standard,
  Beta,
  Citadel,
  Dtv,
  Waldbaur,
}

export const buildType = BuildType.Beta as BuildType;

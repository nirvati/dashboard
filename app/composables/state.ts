// SPDX-FileCopyrightText: 2024 The Nirvati Developers
//
// SPDX-License-Identifier: AGPL-3.0-or-later

import type { BreadcrumbLink } from "#ui/types/breadcrumb";

export const useBreadcrumbPath = () =>
  useState<BreadcrumbLink[]>("breadcrumbPath", () => []);

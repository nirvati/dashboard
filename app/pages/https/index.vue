<!--
SPDX-FileCopyrightText: 2024 The Nirvati Developers

SPDX-License-Identifier: AGPL-3.0-or-later
-->

<template>
  <UCard class="min-h-full">
    <template #header>
      <div class="flex flex-row">
        <div class="flex-col gap-1">
          <h1 class="text-4xl font-bold">
            {{ t("https.title") }}
          </h1>
          <span class="max-w-lg text-center">
            {{ t("https.subtitle") }}
          </span>
        </div>
        <div class="flex flex-col gap-3 ml-auto mr-2 justify-center">
          <NuxtLink
            to="/https/domains"
            class="flex cursor-pointer flex-row items-center text-sm"
          >
            <UIcon name="i-heroicons-link" class="mr-2 h-6 w-6" />
            <span class="text-lg">{{
              t("https.connected-domains.button")
            }}</span>
          </NuxtLink>
          <NuxtLink
            to="/https/proxies"
            class="flex cursor-pointer flex-row items-center text-sm"
          >
            <UIcon name="i-ph-planet" class="mr-2 h-6 w-6" />
            <span class="text-lg">{{ t("proxies.manage-button") }}</span>
          </NuxtLink>
        </div>
      </div>
    </template>
    <span v-if="error" class="text-red-300 dark:text-red-700">{{ error }}</span>
    <div
      v-else
      class="grid h-auto w-full grid-cols-1 md:lg:grid-cols-2 xl:grid-cols-3 gap-4"
    >
      <UCard
        v-for="app in apps"
        :key="app.id"
        :ui="{
          body: 'flex flex-row h-full w-full items-center',
        }"
      >
        <img
          aria-hidden="true"
          :alt="`${getTranslated(app.name)} icon`"
          :src="app.icon!"
          class="mb-2 h-32 w-32 rounded"
        />
        <div class="ml-6 flex flex-col justify-center">
          <h2 class="text-bold text-2xl">
            {{ getTranslated(app.name) }}
          </h2>
          <div v-for="domain in app.domains" :key="domain.name">
            <a
              :href="`https://${domain.name}${app.path || '/'}`"
              class="text-sm text-blue-500"
              >{{ domain.name }}</a
            >
            <button
              v-if="app.domains.length > 1"
              @click="deleteDomain(domain.name)"
              class="cursor-pointer"
            >
              <UIcon name="i-heroicons-minus-circle" class="ml-2 size-4" />
            </button>
          </div>
          <a
            v-if="!app.paused"
            class="mt-1 flex cursor-pointer flex-row items-center text-sm"
            @click.prevent="
              selectedApp = app.id;
              showAppSetupModal = true;
            "
          >
            <UIcon name="i-heroicons-plus-circle" class="mr-2 size-4" />
            <span>{{ t("https.add-new") }}</span></a
          >
          <p v-else class="mt-1 flex flex-row items-center text-sm">
            <UIcon class="mr-2 h-4 w-4" name="i-heroicons-pause" />
            <span>{{ t("https.paused") }}</span>
          </p>
        </div>
      </UCard>
      <UModal
        v-model:open="showAppSetupModal"
        class="text-center"
        :title='t("https.add-new-dialog.title")'
        :ui="{ root: 'flex items-center justify-center py-4' }"
      >
        <template #body>
          <form
            class="flex flex-col items-center justify-center w-full"
            @submit.prevent="addDomain"
          >
            <USelect
              v-model="newDomainOption"
              option-attribute="name"
              class="mb-3 min-w-[50%]"
              :placeholder="t('utils.please-select')"
              :items="domainOptionsForApp(selectedApp)"
            />
            <UInput
              v-if="newDomainOption === 'custom'"
              v-model="customAppDomain"
              :placeholder="`${selectedApp}.example.com`"
              class="mb-2"
            />
            <p v-if="error" class="text-red-500">
              {{ error }}
            </p>
            <UButton type="submit">
              {{ t("https.add-new-dialog.confirm") }}
            </UButton>
            <UAlert
              v-if="newDomainOption === 'custom'"
              class="max-w-[80%] my-4"
              :title="t('https.port-forwarding.title')"
              :description="t('https.port-forwarding.content')"
            />
            <UAlert
              v-if="newDomainOption === 'onion'"
              class="max-w-[80%] my-4"
              :title="t('https.onion-explanation.title')"
              :description="t('https.onion-explanation.description')"
            />
          </form>
        </template>
      </UModal>
    </div>
  </UCard>
</template>

<script setup lang="ts">
import type { AcmeProvider } from "~/utils/gql/graphql";

const { t } = useI18n();
const { getTranslated } = useTranslate();
const showAppSetupModal = ref(false);
const newDomainOption = ref<string>("");
const selectedApp = ref("");
const customAppDomain = ref("");
const selectedDomain = computed(() => {
  if (newDomainOption.value === "custom") {
    return customAppDomain.value;
  } else {
    return newDomainOption.value;
  }
});

const appsQuery = graphql(`
  query getHttpsApps {
    users {
      me {
        apps(installedOnly: true) {
          id
          name
          icon
          path
          supportsIngress
          paused
          domains {
            name
          }
        }
      }
    }
  }
`);

const { data, error, refresh: refreshDomains } = await useAsyncQuery(appsQuery);

const apps = computed(() => {
  const apps = data.value?.users.me.apps || [];
  // Library apps are "internal" apps that do not get a domain
  return apps.filter((app) => app.supportsIngress).sort((a, b) => {
    if (a.id == "nirvati") {
      return -1;
    } else if (b.id == "nirvati") {
      return 1;
    } else {
      return getTranslated(a.name).localeCompare(getTranslated(b.name))
    }
  });
});

const hasTor = computed(() => {
  return !!data.value?.users.me.apps.find((app) => app.id === "tor");
});

const domainsQuery = graphql(`
  query getLinkedDomainNames {
    users {
      me {
        domains {
          domain
        }
        acmeProviders
      }
    }
  }
`);

const { data: linkedDomains } = await useAsyncQuery(domainsQuery);

watch(showAppSetupModal, (newVal) => {
  if(!newVal) {
    newDomainOption.value = '';
  }
});

function randomProvider(acmeProviders: AcmeProvider[]) {
  return acmeProviders[Math.floor(Math.random() * acmeProviders.length)];
}

const addOnionDomainMutation = graphql(`
  mutation addOnionDomain($app: String!) {
    addOnionDomain(appId: $app)
  }
`);

const { mutate: performAddDomainMutation } = useMutation(
  addAppDomainMutation,
  computed(() => ({
    variables: {
      app: selectedApp.value,
      domain: selectedDomain.value,
      acmeProvider: randomProvider(
        linkedDomains.value!.users.me.acmeProviders,
      )!,
      forceHttpOnly: false,
    },
  })),
);

const { mutate: performAddOnionDomainMutation } = useMutation(
  addOnionDomainMutation,
  computed(() => ({
    variables: {
      app: selectedApp.value,
    },
  })),
);

async function addDomain() {
  if (newDomainOption.value === "onion") {
    await performAddOnionDomainMutation();
  } else {
    await performAddDomainMutation();
  }
  if (error.value) {
    return;
  }
  await refreshDomains();
  newDomainOption.value = "custom";
  customAppDomain.value = "";
  showAppSetupModal.value = false;
}

const deleteDomainMutation = graphql(`
  mutation deleteDomain($name: String!) {
    removeAppDomain(domain: $name)
  }
`);

const { mutate: performDeleteDomain } = useMutation(deleteDomainMutation);

async function deleteDomain(name: string) {
  await performDeleteDomain({ name });
  await refreshDomains();
}

function domainOptionsForApp(app: string) {
  const domains =
    linkedDomains.value?.users.me.domains.map((d: { domain: string }) => ({
      label: `${app}.${d.domain}`,
      value: `${app}.${d.domain}`,
    })) || [];
  const hasTorDomain = !!data.value?.users.me.apps
    .find((appData) => appData.id == app)
    ?.domains.find((domain) => domain.name.endsWith(".onion"));
  const showTor = hasTor.value && !hasTorDomain;
  return [
    ...domains,
    ...(showTor
      ? [
          {
            value: "onion",
            label: t("https.onion"),
          },
        ]
      : []),
    {
      value: "custom",
      label: t("https.custom"),
    },
  ];
}

definePageMeta({
  layout: "authenticated",
});
</script>

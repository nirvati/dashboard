<!--
SPDX-FileCopyrightText: 2024 The Nirvati Developers

SPDX-License-Identifier: AGPL-3.0-or-later
-->

<template>
  <UCard
    class="min-h-full"
    :ui="{
      root: 'flex flex-col',
      body: 'h-full flex-grow flex flex-col',
    }"
  >
    <template #header>
      <h1 class="text-4xl font-bold">
        {{ t("proxies.title") }}
      </h1>
      <span class="max-w-lg text-center">
        {{ t("proxies.subtitle") }}
      </span>
    </template>
    <div
      v-if="proxiesInfo!.users.me.proxies.length === 0"
      class="flex items-center justify-center flex-col h-full flex-grow"
    >
      <h2 class="font-bold text-2xl">
        {{ t("proxies.no-proxies.heading") }}
      </h2>
      <p class="mb-4">
        {{ t("proxies.no-proxies.explanation") }}
      </p>
      <UButton size="lg" @click="showCreateProxiesModal = true">
        {{ t("proxies.create") }}
      </UButton>
    </div>
    <div v-else class="grid grid-cols-1 gap-4 md:grid-cols-2 lg:grid-cols-3">
      <UCard v-for="proxy in proxiesInfo!.users.me.proxies" :key="proxy.domain">
        <template #header>
          <div class="flex items-center justify-center">
            <h2 class="font-bold text-2xl">
              {{ proxy.domain }}
            </h2>
            <button class="ml-auto" @click="deleteProxy(proxy.domain)">
              <UIcon name="i-heroicons-trash" />
            </button>
          </div>
        </template>
        <div class="flex flex-col gap-2">
          <div v-for="route in proxy.routes" :key="route.localPath">
            <span class="font-bold text-lg">{{ route.localPath }}</span> ->
            {{ route.protocol.toLowerCase() }}://{{ route.service.upstream
            }}{{ route.targetPath }}
            <button
              v-if="proxy.routes.length > 1"
              @click="deleteRoute(route.id)"
            >
              <UIcon name="i-heroicons-trash" />
            </button>
          </div>
          <button
            class="w-max"
            @click="
              domain = proxy.domain;
              showAddRouteModal = true;
            "
          >
            {{ t("proxies.add-route") }}
          </button>
        </div>
      </UCard>
      <UCard
        :ui="{ body: 'h-full flex items-center justify-center' }"
      >
        <div class="flex items-center justify-center h-full w-full">
          <button @click="showCreateProxiesModal = true">
            <UIcon name="i-heroicons-plus-circle" />
            {{ t("proxies.add-new") }}
          </button>
        </div>
      </UCard>
    </div>
    <UModal v-model:open="showCreateProxiesModal" :title='t("proxies.create-dialog.title")'>
        <template #body>
        <form
          class="flex gap-2 flex-col items-center justify-center"
          @submit.prevent="createProxies"
        >
          <UFormField :label="t('proxies.create-dialog.domain')">
            <UInput v-model="domain" required />
          </UFormField>
          <UFormField :label="t('proxies.create-dialog.target-url')">
            <UInput v-model="targetUrl" required />
            <span
              v-if="isInvalidTarget && targetUrl != ''"
              class="text-red-500"
            >
              {{ t("proxies.create-dialog.invalid-target") }}
            </span>
          </UFormField>
          <UFormField
            v-if="parsedTarget?.protocol === 'https:'"
            :label="t('proxies.create-dialog.target-sni')"
          >
            <UInput v-model="targetSni" />
          </UFormField>
          <UCheckbox
            v-model="enableHttpCompression"
            :label="t('proxies.create-dialog.enable-http-compression')"
          />
          <UCheckbox
            v-model="allowInvalidCertificates"
            :label="t('proxies.create-dialog.allow-invalid-certificates')"
          />
          <UButton type="submit" :enabled="!isInvalidTarget">
            {{ t("proxies.create") }}
          </UButton>
        </form>
        </template>
    </UModal>
    <UModal v-model="showAddRouteModal" :title='t("proxies.add-route-dialog.title")'>
      <template #body>
        <form
          class="flex gap-2 flex-col items-center justify-center"
          @submit.prevent="createRoute"
        >
          <UFormField :label="t('proxies.add-route-dialog.path-prefix')">
            <UInput v-model="pathPrefix" required />
          </UFormField>
          <UFormField :label="t('proxies.add-route-dialog.target-url')">
            <UInput v-model="targetUrl" required />
            <span
              v-if="isInvalidTarget && targetUrl != ''"
              class="text-red-500"
            >
              {{ t("proxies.add-route-dialog.invalid-target") }}
            </span>
          </UFormField>
          <UFormField
            v-if="parsedTarget?.protocol === 'https:'"
            :label="t('proxies.create-dialog.target-sni')"
          >
            <UInput v-model="targetSni" />
          </UFormField>
          <UCheckbox
            v-model="enableHttpCompression"
            :label="t('proxies.add-route-dialog.enable-http-compression')"
          />
          <UCheckbox
            v-model="allowInvalidCertificates"
            :label="t('proxies.add-route-dialog.allow-invalid-certificates')"
          />
          <UButton type="submit" :enabled="!isInvalidTarget">
            {{ t("proxies.create") }}
          </UButton>
        </form>
</template>
    </UModal>
  </UCard>
</template>

<script setup lang="ts">
import { Protocol } from "~/utils/gql/graphql";

const { t } = useI18n();

const showCreateProxiesModal = ref(false);
const showAddRouteModal = ref(false);
const targetUrl = ref("");
const targetSni = ref("");
const domain = ref("");
const pathPrefix = ref("");
const enableHttpCompression = ref(true);
const allowInvalidCertificates = ref(false);
const isInvalidTarget = ref(false);

const proxiesInfoQuery = graphql(`
  query getProxiesInfo {
    users {
      me {
        proxies {
          domain
          routes {
            id
            localPath
            targetPath
            protocol
            service {
              upstream
            }
          }
        }
        services {
          id
          upstream
          tcpPorts
          udpPorts
        }
      }
    }
  }
`);

const { data: proxiesInfo, refresh } = await useAsyncQuery(proxiesInfoQuery);

const ipv4Regex =
  /^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/;

const ipv6Regex =
  /^(([0-9a-fA-F]{1,4}:){7}[0-9a-fA-F]{1,4}|([0-9a-fA-F]{1,4}:){1,7}:|([0-9a-fA-F]{1,4}:){1,6}:[0-9a-fA-F]{1,4}|([0-9a-fA-F]{1,4}:){1,5}(:[0-9a-fA-F]{1,4}){1,2}|([0-9a-fA-F]{1,4}:){1,4}(:[0-9a-fA-F]{1,4}){1,3}|([0-9a-fA-F]{1,4}:){1,3}(:[0-9a-fA-F]{1,4}){1,4}|([0-9a-fA-F]{1,4}:){1,2}(:[0-9a-fA-F]{1,4}){1,5}|[0-9a-fA-F]{1,4}:((:[0-9a-fA-F]{1,4}){1,6})|:((:[0-9a-fA-F]{1,4}){1,7}|:)|fe80:(:[0-9a-fA-F]{0,4}){0,4}%[0-9a-zA-Z]+|::(ffff(:0{1,4})?:)?((25[0-5]|(2[0-4]|1?[0-9])?[0-9])\.){3}(25[0-5]|(2[0-4]|1?[0-9])?[0-9])|([0-9a-fA-F]{1,4}:){1,4}:((25[0-5]|(2[0-4]|1?[0-9])?[0-9])\.){3}(25[0-5]|(2[0-4]|1?[0-9])?[0-9]))$/;

const createServiceMutation = graphql(`
  mutation createService(
    $ip: String
    $dnsName: String
    $tcpPorts: [Int!]!
    $udpPorts: [Int!]!
  ) {
    createService(
      ip: $ip
      dnsName: $dnsName
      tcpPorts: $tcpPorts
      udpPorts: $udpPorts
    )
  }
`);

const addServicePortsMutation = graphql(`
  mutation addServicePorts(
    $id: String!
    $tcpPorts: [Int!]!
    $udpPorts: [Int!]!
  ) {
    addServicePorts(id: $id, tcpPorts: $tcpPorts, udpPorts: $udpPorts)
  }
`);

const createProxyMutation = graphql(`
  mutation createProxy($domain: String!) {
    createProxy(domain: $domain, acmeProvider: LETS_ENCRYPT)
  }
`);

const deleteProxyMutation = graphql(`
  mutation deleteProxy($domain: String!) {
    deleteProxy(domain: $domain)
  }
`);

const createRouteMutation = graphql(`
  mutation createRoute(
    $domain: String!
    $serviceId: String!
    $localPath: String!
    $targetPath: String!
    $targetPort: Int!
    $protocol: Protocol!
    $enableCompression: Boolean!
    $allowInvalidCertificates: Boolean!
    $targetSni: String
  ) {
    createRoute(
      proxyDomain: $domain
      serviceId: $serviceId
      localPath: $localPath
      targetPath: $targetPath
      targetPort: $targetPort
      protocol: $protocol
      enableCompression: $enableCompression
      allowInvalidCert: $allowInvalidCertificates
      targetSni: $targetSni
    )
  }
`);

const deleteRouteMutation = graphql(`
  mutation deleteRoute($id: String!) {
    deleteRoute(routeId: $id)
  }
`);

const { mutate: createService } = useMutation(createServiceMutation);
const { mutate: addServicePorts } = useMutation(addServicePortsMutation);
const { mutate: createProxy } = useMutation(createProxyMutation);
const { mutate: doDeleteProxy } = useMutation(deleteProxyMutation);
const { mutate: doCreateRoute } = useMutation(createRouteMutation);
const { mutate: doDeleteRoute } = useMutation(deleteRouteMutation);

const parsedTarget = computed(() => {
  try {
    return new URL(targetUrl.value);
  } catch {
    return null;
  }
});

const createProxies = async () => {
  const parsedUrl = parsedTarget.value;
  if (!parsedUrl) {
    return;
  }
  const target = parsedUrl.hostname;
  const isIp = ipv4Regex.test(target) || ipv6Regex.test(target);
  const port =
    Number(parsedUrl.port) || (parsedUrl.protocol === "https:" ? 443 : 80);
  const protocol = parsedUrl.protocol;
  const existingService = proxiesInfo.value?.users.me.services.find(
    (service) => service.upstream === target,
  );
  let serviceId = existingService?.id;
  if (!existingService) {
    const result = await createService({
      ip: isIp ? target : undefined,
      dnsName: isIp ? undefined : target,
      tcpPorts: [port],
      // Only HTTP2 uses UDP
      udpPorts: protocol === "https" ? [port] : [],
    });
    serviceId = result?.data?.createService;
  } else if (isIp && !existingService.tcpPorts.includes(port)) {
    await addServicePorts({
      id: existingService.id,
      tcpPorts: [port],
      udpPorts: protocol === "https" ? [port] : [],
    });
  }
  const remotePath = parsedUrl!.pathname;
  await createProxy({ domain: domain.value });
  const localPath = "/";
  await doCreateRoute({
    domain: domain.value,
    serviceId: serviceId!,
    localPath,
    targetPath: remotePath,
    targetPort: port,
    protocol: protocol === "https" ? Protocol.Https : Protocol.Http,
    enableCompression: enableHttpCompression.value,
    allowInvalidCertificates: allowInvalidCertificates.value,
    targetSni: targetSni.value === "" ? undefined : targetSni.value,
  });
  await refresh();
  // Reset state so the modal is clean when opened again
  showCreateProxiesModal.value = false;
  domain.value = "";
  targetUrl.value = "";
  targetSni.value = "";
  enableHttpCompression.value = true;
  allowInvalidCertificates.value = false;
};

const createRoute = async () => {
  const parsedUrl = parsedTarget.value;
  const target = parsedUrl!.hostname;
  const isIp = ipv4Regex.test(target) || ipv6Regex.test(target);
  const port =
    Number(parsedUrl!.port) || (parsedUrl!.protocol === "https:" ? 443 : 80);
  const protocol = parsedUrl!.protocol;
  const existingService = proxiesInfo.value?.users.me.services.find(
    (service) => service.upstream === target,
  );
  let serviceId = existingService?.id;
  if (!existingService) {
    const { data } = (await createService({
      ip: isIp ? target : undefined,
      dnsName: isIp ? undefined : target,
      tcpPorts: [port],
      // Only HTTP2 uses UDP
      udpPorts: protocol === "https" ? [port] : [],
    }))!;
    serviceId = data!.createService;
  } else if (isIp && !existingService.tcpPorts.includes(port)) {
    await addServicePorts({
      id: existingService.id,
      tcpPorts: [port],
      udpPorts: protocol === "https" ? [port] : [],
    });
  }
  const remotePath = parsedUrl!.pathname;
  await doCreateRoute({
    domain: domain.value,
    serviceId: serviceId!,
    localPath: pathPrefix.value,
    targetPath: remotePath,
    targetPort: port,
    protocol: protocol === "https" ? Protocol.Https : Protocol.Http,
    enableCompression: enableHttpCompression.value,
    allowInvalidCertificates: allowInvalidCertificates.value,
    targetSni: targetSni.value === "" ? undefined : targetSni.value,
  });
  await refresh();
  // Reset state so the modal is clean when opened again
  showAddRouteModal.value = false;
  pathPrefix.value = "";
  targetUrl.value = "";
  targetSni.value = "";
  enableHttpCompression.value = true;
  allowInvalidCertificates.value = false;
};

async function deleteProxy(domain: string) {
  await doDeleteProxy({ domain });
  await refresh();
}

async function deleteRoute(id: string) {
  await doDeleteRoute({ id });
  await refresh();
}

definePageMeta({
  layout: "authenticated",
});
</script>

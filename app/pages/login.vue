<!--
SPDX-FileCopyrightText: 2024 The Nirvati Developers

SPDX-License-Identifier: AGPL-3.0-or-later
-->

<template>
  <div
    class="new-bg flex h-dvh flex-col items-center justify-center dark text-white"
  >
    <div
      class="absolute right-3 top-3 flex items-center justify-center dark:text-white-important"
    >
      <LanguageToggle class="mx-1" />
    </div>
    <UCard
      :ui="{
        root: 'bg-zinc-900/40 dark:bg-zinc-900/40 ring-0 shadow-none md:shadow',
      }"
      class="max-w-md"
    >
      <template #header>
        <img
          v-if="buildType === BuildType.Dtv"
          src="~/assets/dtv-logo-black-bg.svg"
          class="h-24 mx-auto"
        />
        <img
          v-else-if="buildType === BuildType.Citadel"
          src="~/assets/citadel-logo.svg"
          class="h-32 mx-auto"
        />
        <img v-else src="~/assets/nirvati-logo-text.svg" class="h-24 mx-auto" />
      </template>
      <h3 class="text-lg text-center mb-4">
        {{ t("login.subtitle") }}
      </h3>
      <form
        class="flex flex-col items-center justify-center w-fit mx-auto gap-4"
        @submit.prevent="login"
      >
        <USelect
          v-if="loginOptions.length > 1"
          v-model="username"
          :items="loginOptions"
          :placeholder="t('login.user')"
          required
          size="xl"
          autocomplete="username"
          class="w-full"
        />
        <UInput
          v-if="username === '__custom' || loginOptions.length === 1"
          v-model="customUsername"
          :placeholder="t('login.username')"
          required
          size="xl"
          autocomplete="username"
          class="w-full"
        />

        <UButton
          :loading="passkeyLoading"
          :disabled="loading || passkeyLoading || selectedUsername.length == 0"
          @click="passkeyLogin"
          aria-label="Login"
          size="lg"
          icon="i-fluent-person-passkey-16-filled"
        >
          {{ t("login.login-with-passkey") }}
        </UButton>

        <div class="flex items-center justify-center w-full">
          <hr class="flex-grow border-gray-300 dark:border-gray-700" />
          <span class="mx-2">{{ t("login.login-or") }}</span>
          <hr class="flex-grow border-gray-300 dark:border-gray-700" />
        </div>
        <PasswordInput
          v-model="password"
          :placeholder="t('login.password')"
          required
          size="xl"
          autocomplete="current-password"
          class="w-full"
        />
        <div class="h-10 flex gap-2 flex-col items-center justify-center mb-2">
          <UButton
            :loading="loading"
            :disabled="loading || passkeyLoading || password.length == 0"
            type="submit"
            aria-label="Login"
            size="lg"
          >
            {{ t("login.login") }}
          </UButton>
        </div>
      </form>
    </UCard>
  </div>
</template>

<script setup lang="ts">
const { onLogin } = useApollo();
const { t } = useI18n();

const username = ref("");
const customUsername = ref("");
const password = ref("");
const loading = ref(false);
const passkeyLoading = ref(false);

const userInfoQuery = graphql(`
  query getLoginUserInfo {
    users {
      loginUsernames {
        username
        name
      }
    }
  }
`);
const { data } = await useAsyncQuery(userInfoQuery);

const loginOptions = computed(() => {
  const found =
    data?.value?.users.loginUsernames?.map((user) => ({
      label: `${user.name} (${user.username})`,
      value: user.username,
    })) || [];
  return [
    ...found,
    {
      value: "__custom",
      label: t("login.enter-custom-username"),
    },
  ];
});

if (data.value?.users?.loginUsernames?.length === 0) {
  username.value = "__custom";
}

const loginMutation = graphql(`
  mutation performLogin($id: String!, $password: String!) {
    login(username: $id, password: $password)
  }
`);

const selectedUsername = computed(() => {
  if (username.value === "__custom") {
    return customUsername.value;
  }
  return username.value;
});

const { mutate: doLogin, onError } = useMutation(
  loginMutation,
  computed(() => ({
    variables: {
      id: selectedUsername.value,
      password: password.value,
    },
  })),
);

onError((e) => {
  loading.value = false;
});

const isFinishedQuery = graphql(`
  query getIsSetupFinished {
    info {
      isSetupFinished
    }
  }
`);
const { data: setupFinishedState } = await useAsyncQuery(isFinishedQuery);
const isMyAccFinishedQuery = graphql(`
  query getIsSetupUserFinished {
    users {
      me {
        setupFinished
      }
    }
  }
`);
const { result, load } = useLazyQuery(isMyAccFinishedQuery);

async function login() {
  loading.value = true;
  const loginResult = await doLogin();
  if (!loginResult?.data?.login) {
    loading.value = false;
    return;
  }
  await onLogin(loginResult.data.login!);
  if (setupFinishedState.value?.info.isSetupFinished === false) {
    if (window.location.protocol === "https:") {
      await navigateTo("/setup/finish");
    } else {
      await navigateTo("/setup/https-ready");
    }
  } else {
    await load();
    await nextTick();
    if (!result.value?.users?.me?.setupFinished) {
      await navigateTo("/setup-user");
      return;
    }
    await navigateTo("/dashboard");
  }
}

const createPasskeyChallengeMutation = graphql(`
  mutation createPasskeyChallenge($username: String!) {
    createChallenge(username: $username)
  }
`);
const { mutate: createPasskeyChallenge } = useMutation(
  createPasskeyChallengeMutation,
);

const passkeyLoginMutation = graphql(`
  mutation passkeyLogin(
    $keyId: String!
    $response: String!
    $authenticatorData: String!
    $clientDataJSON: String!
  ) {
    loginWithPasskey(
      keyId: $keyId
      signature: $response
      authenticatorData: $authenticatorData
      clientDataJson: $clientDataJSON
    )
  }
`);

const { mutate: doLoginWithPasskey } = useMutation(passkeyLoginMutation);

async function passkeyLogin() {
  passkeyLoading.value = true;
  const challenge = await createPasskeyChallenge({
    username: selectedUsername.value,
  });
  if (!challenge?.data?.createChallenge) {
    passkeyLoading.value = false;
    return;
  }
  const challengeString = challenge.data.createChallenge
    .replace(/-/g, "+")
    .replace(/_/g, "/");
  console.log(Uint8Array.from(atob(challengeString), (c) => c.charCodeAt(0)));
  const response = await navigator.credentials
    .get({
      publicKey: {
        challenge: Uint8Array.from(atob(challengeString), (c) =>
          c.charCodeAt(0),
        ),
        rpId: window.location.host,
        userVerification: "required",
        /*allowCredentials: [
          {
            type: "public-key",
          },
        ],*/
        timeout: 60000,
      },
    })
    .catch((e) => {
      console.error(e);
      passkeyLoading.value = false;
      throw e;
    }) as PublicKeyCredential & { readonly response: AuthenticatorAssertionResponse; } | null;
  if (!response || !response.response) {
    passkeyLoading.value = false;
    return;
  }
  const loginResult = await doLoginWithPasskey({
    response: btoa(
      String.fromCharCode.apply(
        null,
        new Uint8Array(response.response.signature),
      ),
    )
      .replace(/\+/g, "-")
      .replace(/\//g, "_"),
    keyId: btoa(String.fromCharCode.apply(null, new Uint8Array(response.rawId)))
      .replace(/\+/g, "-")
      .replace(/\//g, "_"),
    authenticatorData: btoa(
      String.fromCharCode.apply(
        null,
        new Uint8Array(response.response.authenticatorData),
      ),
    )
      .replace(/\+/g, "-")
      .replace(/\//g, "_"),
    clientDataJSON: btoa(
      String.fromCharCode.apply(
        null,
        new Uint8Array(response.response.clientDataJSON),
      ),
    )
      .replace(/\+/g, "-")
      .replace(/\//g, "_"),
  });
  if (loginResult?.data?.loginWithPasskey) {
    await onLogin(loginResult.data.loginWithPasskey!);
    if (setupFinishedState.value?.info.isSetupFinished === false) {
      if (window.location.protocol === "https:") {
        await navigateTo("/setup/finish");
      } else {
        await navigateTo("/setup/https-ready");
      }
    } else {
      await load();
      await nextTick();
      if (!result.value?.users?.me?.setupFinished) {
        await navigateTo("/setup-user");
        return;
      }
      await navigateTo("/dashboard");
    }
  }
}

definePageMeta({
  title: "Login",
});
</script>

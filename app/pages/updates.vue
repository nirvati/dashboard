<!--
SPDX-FileCopyrightText: 2024 The Nirvati Developers

SPDX-License-Identifier: AGPL-3.0-or-later
-->

<template>
  <UCard class="h-full min-h-0 overflow-scroll">
    <div
      v-if="kubeUpdateAvailable || appUpdates.length > 0"
      class="grid h-auto w-full grid-cols-1 md:grid-cols-2 xl:grid-cols-3 gap-4"
    >
      <UCard
        v-if="kubeUpdateAvailable"
        class="justify-center flex-col items-start"
      >
        <div class="flex justify-center items-center">
          <img
            v-if="kubeVersion!.upgrades.kube.installedVersion.includes('k3s')"
            alt="K3s logo"
            src="@/assets/k3s.svg"
            class="mb-2 h-32 w-32 rounded"
          >
          <img
            v-else-if="
              kubeVersion!.upgrades.kube.installedVersion.includes('rke2')
            "
            alt="RKE2 logo"
            src="@/assets/rke2.svg"
            class="mb-2 h-32 w-32 rounded"
          >
          <div class="ml-6 flex flex-col justify-center">
            <h2 class="text-bold text-2xl flex items-center">
              {{
                kubeVersion!.upgrades.kube.installedVersion.includes("k3s")
                  ? "K3s"
                  : "RKE2"
              }}
              <span
                class="text-xs bg-blue-300 dark:bg-blue-700 text-white rounded px-1 h-min ml-2"
                >System base</span
              >
            </h2>

            <p>
              Version: <b>{{ kubeVersion!.upgrades.kube.installedVersion }}</b>
            </p>
            <div>
              <b>{{ latestKubeVersion!.upgrades.kube.latestVersion }}</b> is now
              available.
            </div>
          </div>
        </div>
        <UButton
          class="block w-full"
          :loading="kubeUpdating"
          @click="updateSystemComponent('kube')"
        >
          Update now!
        </UButton>
      </UCard>
      <UCard
        v-for="app in appUpdates"
        :key="app.id"
        class="items-start justify-center flex-col"
      >
        <div class="flex justify-center">
          <img
            aria-hidden="true"
            :alt="`${getTranslated(app.name)} icon`"
            :src="app.icon!"
            class="mb-2 h-32 w-32 rounded"
          >
          <div class="ml-6 flex flex-col justify-center">
            <h2 class="text-bold text-2xl">
              {{ getTranslated(app.name) }}
            </h2>
            <p>Version: {{ app.version! }}</p>
            <div>
              {{ getTranslated(app.name) }} <b>{{ app.latestVersion!.latestVersion }}</b> is
              now available.
            </div>
          </div>
        </div>
        <UButton
          class="block w-full"
          :loading="updatingApps.includes(app.id)"
          @click="updateApp(app.id)"
        >
          Update now!
        </UButton>
      </UCard>
    </div>
    <div
      v-else-if="
        !availableUpdatesPending &&
        !kubeVersionPending &&
        !latestKubeVersionPending
      "
    >
      <div class="flex flex-col items-center justify-center h-full">
        <h1 class="mb-3 text-3xl font-bold">
          {{ t("updates.no-updates.title") }}
        </h1>
        <span class="mb-2 max-w-lg text-center">
          {{ t("updates.no-updates.subtitle") }}
        </span>
      </div>
    </div>
    <div v-else>
      <div class="flex flex-col items-center justify-center h-full">
        <UIcon name="i-heroicons-arrow-path" class="h-20 w-20 mb-4" />
        <h1 class="mb-3 text-3xl font-bold">
          {{ t("updates.loading.title") }}
        </h1>
        <span class="mb-2 max-w-lg text-center">
          {{ t("updates.loading.subtitle") }}
        </span>
      </div>
    </div>
  </UCard>
</template>

<script setup lang="ts">
const availableUpdatesQuery = graphql(`
  query getAvailableUpdates {
    users {
      me {
        permissions
        apps(installedOnly: true) {
          id
          name
          icon
          version
          latestVersion {
            latestVersion
          }
        }
      }
    }
  }
`);

const { t } = useI18n();
const { getTranslated } = useTranslate();

const {
  data: availableUpdates,
  refresh: refreshAvailableUpdates,
  pending: availableUpdatesPending,
} = await useLazyAsyncQuery(availableUpdatesQuery);

const appUpdates = computed(
  () =>
    availableUpdates.value?.users?.me?.apps.filter(
      (app) =>
        !!app.latestVersion && app.version !== app.latestVersion.latestVersion,
    ) || [],
);

const kubeVersionQuery = graphql(`
  query getKubeVersion {
    upgrades {
      kube {
        installedVersion
      }
    }
  }
`);

const latestKubeVersionQuery = graphql(`
  query getLatestKubeVersion {
    upgrades {
      kube {
        latestVersion
      }
    }
  }
`);

const {
  data: kubeVersion,
  refresh: refreshKube,
  pending: kubeVersionPending,
} = await useLazyAsyncQuery(kubeVersionQuery);

const { data: latestKubeVersion, pending: latestKubeVersionPending } =
  await useLazyAsyncQuery(latestKubeVersionQuery);

const kubeUpdateAvailable = computed(
  () =>
    kubeVersion.value?.upgrades.kube.installedVersion !==
      latestKubeVersion.value?.upgrades.kube.latestVersion &&
    kubeVersion.value?.upgrades.kube.installedVersion !== undefined &&
    latestKubeVersion.value?.upgrades.kube.latestVersion !== undefined,
);

const updateKubeMutation = graphql(`
  mutation updateKube($version: String!) {
    upgradeKube(targetVersion: $version)
  }
`);

const { mutate: updateKube } = useMutation(updateKubeMutation);

const kubeUpdating = ref(false);
const updatingApps = ref<string[]>([]);

async function updateSystemComponent(name: string) {
  switch (name) {
    case "kube": {
      kubeUpdating.value = true;
      await updateKube({
        version: latestKubeVersion.value!.upgrades.kube.latestVersion!,
      });
      const interval = setInterval(async () => {
        await refreshKube();
        if (
          kubeVersion.value?.upgrades.kube.installedVersion ===
          latestKubeVersion.value?.upgrades.kube.latestVersion
        ) {
          kubeUpdating.value = false;
          clearInterval(interval);
        }
      }, 1000);
      break;
    }
    default:
      alert(`Unknown system component ${name}`);
      throw new Error(`Unknown system component ${name}`);
  }
}

const updateAppMutation = graphql(`
  mutation updateApp($id: String!) {
    updateApp(appId: $id)
  }
`);

const { mutate: doUpdateApp } = useMutation(updateAppMutation);

async function updateApp(id: string) {
  updatingApps.value.push(id);
  await doUpdateApp({ id });
  const interval = setInterval(async () => {
    await refreshAvailableUpdates();
    if (!appUpdates.value.find((app) => app.id === id)) {
      clearInterval(interval);
      updatingApps.value = updatingApps.value.filter((appId) => appId !== id);
      if (id === "nirvati") {
        window.location.reload();
      }
    }
  }, 1000);
}

definePageMeta({
  layout: "authenticated",
});
</script>

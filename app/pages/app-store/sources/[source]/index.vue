<!--
SPDX-FileCopyrightText: 2024 The Nirvati Developers

SPDX-License-Identifier: AGPL-3.0-or-later
-->

<template>
  <div class="h-full flex flex-col gap-2">
    <UCard>
      <div class="flex mb-2">
        <img :src="queriedApps?.users?.me?.store?.icon!" class="mr-4 size-16 rounded" >
        <div class="flex-1">
          <h2 class="mb-1 text-2xl font-bold">
            {{ getTranslated(queriedApps?.users?.me?.store?.name) }}
          </h2>
          <p class="mb-1 text-lg">
            {{ getTranslated(queriedApps?.users?.me?.store?.tagline) }}
          </p>
        </div>
      </div>
      <p class="block w-full">
        {{ getTranslated(queriedApps?.users?.me?.store?.description) }}
      </p>
    </UCard>
    <UCard
      class="h-full flex flex-col"
      :ui="{
        body: 'min-h-0 h-full flex flex-col',
        header: 'flex flex-row items-center justify-center',
      }"
    >
      <template #header>
        <div class="flex-grow w-full">
          <UInput
            v-model="searchQuery"
            variant="ghost"
            color="neutral"
            icon="i-heroicons-magnifying-glass-20-solid"
            size="xl"
            :trailing="false"
            :placeholder="t('app-store.search')"
            :ui="{ root: 'w-full' }"
          />
        </div>
        <!--<AppStoreAdvanced
          v-model:show-installed="showInstalled"
          :show-store-management="showStoresManagement"
          class="ml-4"
          @refresh="refresh"
        />-->
      </template>

      <div class="overflow-scroll h-full">
        <div
          v-show="Object.keys(appsByCategory).length > 0"
          class="p-2 pr-4 columns-1 md:columns-2 lg:columns-3"
        >
          <UCard
            v-for="category in Object.keys(appsByCategory)"
            :key="category"
            class="flex-1 basis-full md:basis-[45%] lg:basis-[30%] h-fit break-inside-avoid mb-4"
          >
            <template #header>
              <h1 class="text-2xl font-bold">
                {{ category }}
              </h1>
            </template>
            <div class="flex flex-col gap-4">
              <NuxtLink
                v-for="app in appsByCategory[category]"
                :key="app.id"
                :to="`/app-store/sources/${storeId}/${app.id}`"
                class="group flex flex-row items-center"
              >
                <img :src="app.icon!" class="mr-4 h-12 w-12 rounded" />
                <div class="flex-1">
                  <h2 class="mb-1 text-xl font-bold">
                    {{ app.name }}
                  </h2>
                  <p class="mb-1">
                    {{ app.tagline }}
                  </p>
                </div>
                <div
                  class="flex w-8 flex-1 grow-0 items-center justify-end transition"
                >
                  <UIcon
                    name="i-heroicons-chevron-right"
                    class="mr-2 h-6 w-6 custom-transition group-hover:mr-0 group-hover:ml-2"
                  />
                </div>
              </NuxtLink>
            </div>
          </UCard>
        </div>
        <div
          v-if="Object.keys(appsByCategory).length === 0"
          class="flex h-full w-full flex-col items-center justify-center"
        >
          <UIcon
            name="i-emojione-v1-magnifying-glass-tilted-right"
            class="h-36 w-36 mb-4"
          />
          <p>{{ t("app-store.not-found.search") }}</p>
        </div>
      </div>
    </UCard>
  </div>
</template>

<script setup lang="ts">
import Fuse from "fuse.js";
import { UserPermission } from "~/utils/gql/graphql";

const { t } = useI18n();
const searchQuery = ref("");
const showInstalled = ref(false);

definePageMeta({
  layout: "authenticated",
});

const route = useRoute();
// @ts-expect-error Nuxt typedPages breaks this
const storeId = route.params.source as string;

const storeQuery = graphql(`
  query getStoreById($id: String!) {
    users {
      me {
        store(id: $id) {
          name
          tagline
          icon
          description
          license
          developers
          apps {
            id
            name
            tagline
            icon
            category
            description
            installed
            permissions {
              ... on BuiltinPermission {
                permission
              }
            }
          }
        }
      }
    }
  }
`);

const { getTranslated } = useTranslate();

const { data: queriedApps /*, refresh*/ } = await useAsyncQuery(storeQuery, {
  id: storeId,
});

const translatedApps = computed(() =>
  (queriedApps.value?.users.me.store.apps || [])
    .map((app) => ({
      ...app,
      name: getTranslated(app.name),
      tagline: getTranslated(app.tagline),
      category: getTranslated(app.category),
      description: getTranslated(app.description),
    }))
    .filter(({ id, installed, permissions }) => {
      if (
        [
          "nirvati",
          "cert-manager",
          "longhorn",
          "system-upgrade-controller",
          "coredns",
          "metallb",
          "citadel-system-wide",
          "local-path-provisioner",
        ].includes(id)
      ) {
        return false;
      }
      if (
        !queriedApps.value?.users?.me?.permissions?.includes(
          UserPermission.InstallRootApps,
        ) &&
        permissions.some(
          (perm) =>
            perm.__typename == "BuiltinPermission" &&
            getDangerLevel(perm.permission) != DangerLevel.None &&
            !perm.permission.startsWith("plugin/"),
        )
      ) {
        return false;
      }
      if (showInstalled.value) {
        return true;
      } else {
        return !installed;
      }
    }),
);

const fuse = computed(
  () =>
    new Fuse(translatedApps.value || [], {
      keys: [
        {
          name: "name",
          weight: 2,
        },
        {
          name: "tagline",
          weight: 1,
        },
        {
          name: "category",
          weight: 0.2,
        },
        {
          name: "description",
          weight: 0.5,
        },
      ],
    }),
);
const apps = computed(() => {
  if (searchQuery.value) {
    return fuse.value.search(searchQuery.value).map((result) => result.item);
  }
  return translatedApps.value;
});

const appsByCategory = computed(() => {
  const appsByCategory: Record<string, typeof apps.value> = {};
  for (const app of apps.value) {
    if (app.id === "nirvati-telemetry") {
      // This is a choice during install and will be skipped otherwise
      continue;
    }
    if (!appsByCategory[app.category]) {
      appsByCategory[app.category] = [];
    }
    appsByCategory[app.category].push(app);
  }
  return appsByCategory;
});

/*const showStoresManagement = computed(() => {
  return queriedApps.value?.users?.me?.permissions?.includes(
    UserPermission.ManageAppStores,
  );
});*/
</script>

<style scoped>
.custom-transition {
  transition: margin 100ms;
}
</style>

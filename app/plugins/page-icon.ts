// SPDX-FileCopyrightText: 2024 The Nirvati Developers
//
// SPDX-License-Identifier: AGPL-3.0-or-later

import { useBreadcrumbPath } from "~/composables/state";

export default defineNuxtPlugin((nuxtApp) => {
  nuxtApp.hook("page:finish", () => {
    const breadcrumbPath = useBreadcrumbPath();
    breadcrumbPath.value = [];
  });
});

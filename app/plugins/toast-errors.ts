// SPDX-FileCopyrightText: 2024 The Nirvati Developers
//
// SPDX-License-Identifier: AGPL-3.0-or-later

export default defineNuxtPlugin((nuxtApp) => {
  nuxtApp.hook("apollo:error", (error) => {
    const { add: toaster } = useToast();
    const route = useRoute();
    if (
      route.path === "/" ||
      // Tailscale sometimes fails to start, but ignore that
      route.path === "/setup/tailscale"
    ) {
      return;
    }
    if (
      route.path.startsWith("/setup") &&
      (error.graphQLErrors || [])[0]?.message === "Forbidden"
    ) {
      return;
    }
    // const { t } = useI18n();
    for (const graphQLError of error.graphQLErrors || []) {
      if (graphQLError.message === "Variable id is not defined.") {
        console.warn(graphQLError);
        return;
      }
      console.error(graphQLError);
      toaster({
        title: /* t("error.graphql.title") */ "Internal error",
        description: graphQLError.message,
        color: "error",
        icon: "i-heroicons-exclamation-circle",
      });
    }
    if (error.networkError) {
      console.error(error.networkError);
      toaster({
        title: /* t("error.network.title") */ "Network error",
        description: error.networkError.message,
        color: "error",
        icon: "i-heroicons-exclamation-circle",
      });
    }
  });
});

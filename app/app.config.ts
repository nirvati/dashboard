// SPDX-FileCopyrightText: 2024 The Nirvati Developers
//
// SPDX-License-Identifier: AGPL-3.0-or-later

import { buildType, BuildType } from "./utils/buildType";

function buildTypeToColor() {
  switch (buildType as BuildType) {
    case BuildType.Standard:
      return "nirvati";
    case BuildType.Beta:
      return "beta";
    case BuildType.Citadel:
      return "citadel";
    case BuildType.Waldbaur:
      return "waldbaur";
    case BuildType.Dtv:
      return "dtv";
  }
}

export default defineAppConfig({
  ui: {
    colors: {
      primary: buildTypeToColor(),
    },
    toast: {
      slots: {
        root: "z-100"
      },
    },
    card: {
      slots: {
        root:  "bg-white dark:bg-zinc-900",
      },
    },
    input: {
      color: {
        white: {
          outline: "dark:bg-zinc-900",
        },
      },
    },
    navigationMenu: {
      variants: {
        active: {
          true: {
            childLink: "dark:before:bg-neutral-800",
          },
          false: {
            link: "dark:hover:before:bg-neutral-800/50",
          },
        },
      },
      compoundVariants: [
        {
          color: 'primary',
          variant: 'pill',
          active: true,
          class: {
            link: 'dark:text-white dark:before:bg-neutral-800',
            linkLeadingIcon: 'dark:text-white'
          }
        },
        {
          color: 'primary',
          variant: 'link',
          active: true,
          class: {
            link: 'dark:text-white dark:before:bg-neutral-800',
            linkLeadingIcon: 'dark:text-white'
          }
        },
      ]
    },
  },
});

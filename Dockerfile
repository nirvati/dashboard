# SPDX-FileCopyrightText: 2024 The Nirvati Developers
#
# SPDX-License-Identifier: AGPL-3.0-or-later

ARG BUILD_TYPE=Beta

FROM oven/bun:1 AS build

RUN apt update && apt install -y curl

RUN curl -sfLS https://install-node.vercel.app | bash -s -- 22 -y

ARG BUILD_TYPE

WORKDIR /build
COPY . .
RUN sed -i "s/export const buildType = BuildType.Beta/export const buildType = BuildType.$BUILD_TYPE/" app/utils/buildType.ts

RUN bun install

ENV GRAPHQL_ENDPOINT_SERVER http://api.nirvati.svc.cluster.local/v0/graphql
ENV GRAPHQL_ENDPOINT_CLIENT /api/v0/graphql

RUN NITRO_PRESET=node_cluster node node_modules/.bin/nuxi build

RUN rm -rf .output/server/node_modules

RUN cd .output/server && \
    bun install

FROM cgr.dev/chainguard/node:latest

WORKDIR /app
COPY --from=build --chown=node:node /build/.output .

EXPOSE 3000
CMD [ "./server/index.mjs" ]

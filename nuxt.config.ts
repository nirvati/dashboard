// SPDX-FileCopyrightText: 2024 The Nirvati Developers
//
// SPDX-License-Identifier: AGPL-3.0-or-later

// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  compatibilityDate: "2024-07-07",
  future: {
    compatibilityVersion: 4,
  },
  modules: [
    "@nuxtjs/apollo",
    "@nuxt/eslint",
    "@nuxtjs/i18n",
    "@nuxt/ui",
    // "@vite-pwa/nuxt",
    "@vueuse/nuxt",
    // "nuxt-svgo",
    "nuxt-security",
    "nuxt-typed-router",
    "@artmizu/nuxt-prometheus",
  ],

  colorMode: {
    classSuffix: "",
  },

  telemetry: true,

  apollo: {
    clients: {
      default: {
        httpEndpoint:
          process.env.GRAPHQL_ENDPOINT_SERVER
          || "http://127.0.0.1:3001/v0/graphql",
        browserHttpEndpoint:
          process.env.GRAPHQL_ENDPOINT_CLIENT
          || "http://127.0.0.1:3001/v0/graphql",
      },
    },
    cookieAttributes: {
      // For local connections
      secure: false,
    },
  },

  i18n: {
    strategy: "no_prefix",
    detectBrowserLanguage: {
      useCookie: true,
      cookieKey: "i18n",
    },
    lazy: true,
    compilation: {
      strictMessage: false,
    },
    locales: [
      {
        code: "en",
        name: "English",
        file: "en.json",
      },
      {
        code: "de",
        name: "Deutsch",
        file: "de.json",
      },
      {
        code: "it",
        name: "Italiano",
        file: "it.json",
      },
      {
        code: "nl",
        name: "Nederlands",
        file: "nl.json",
      },
      {
        code: "uk",
        name: "українська",
        file: "uk.json",
      },
    ],
  },

  app: {
    layoutTransition: { name: "layout", mode: "out-in" },
    pageTransition: { name: "page", mode: "out-in" },
  },

  devtools: {
    enabled: true,

    timeline: {
      enabled: true,
    },
  },

  security: {
    headers: {
      contentSecurityPolicy: {
        "img-src": ["*", "data:"],
        "upgrade-insecure-requests": false,
      },
      crossOriginEmbedderPolicy: "unsafe-none",
    },
    removeLoggers: false,
    hidePoweredBy: false,
    xssValidator: false,
  },
});
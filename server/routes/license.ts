import spdx from "spdx-license-list/full.js";

export default defineEventHandler((event) => {
  const query = getQuery(event);
  if (typeof query.spdx !== "string") {
    throw createError({
      statusCode: 400,
      statusMessage: 'No spdx identifier provided!',
    });
  }
  const data = spdx[query.spdx] || {
    name: "Unknown",
    osiApproved: false,
  };
  return data;
});
